﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Core.Mysql.Data
{
    public class Istemci : IDisposable
    {
        public string BaglantiTxt { get; set; } = "";
        public MySql.Data.MySqlClient.MySqlConnection MySqlCLient { get; set; }
        public Istemci(string BaglantiTxt)
        {
            this.BaglantiTxt = BaglantiTxt;
            MySqlCLient = new MySql.Data.MySqlClient.MySqlConnection(BaglantiTxt);
        }

        public bool Ac()
        {
            bool Sonuc = false;
            try
            {
                if (MySqlCLient.State != System.Data.ConnectionState.Open)
                {
                    MySqlCLient.Open();
                }
                Sonuc = true;
            }
            catch (Exception Hata)
            {
                Sonuc = false;
            }
            return Sonuc;
        }
        public bool Kapat()
        {
            bool Sonuc = false;
            try
            {
                if (MySqlCLient.State!=System.Data.ConnectionState.Closed)
                {
                    MySqlCLient.Close();
                }
                Sonuc=true;
            }
            catch (Exception Hata)
            {
                Sonuc = false;
            }
            return Sonuc;
        }
        public IslemDurum KomutCalistirWithNonQuery(string Sql, Dictionary<string, object> PrmListesi = null)
        {
            IslemDurum Sonuc=new IslemDurum();
            Ac();
            try
            {
                MySql.Data.MySqlClient.MySqlCommand Komut = new MySql.Data.MySqlClient.MySqlCommand(Sql, this.MySqlCLient);
                if (PrmListesi != null)
                {
                    foreach (var Prm in PrmListesi)
                    {
                        Komut.Parameters.Add(Prm.Key, Prm.Value);
                    }
                }
                Sonuc.Deger=Komut.ExecuteNonQuery();
                Sonuc.Durum=true;
                Sonuc.Mesaj="Başarılı";
            }
            catch (Exception Hata)
            {
                Sonuc.Durum = false;
                Sonuc.Mesaj = "Başarısız";
                Sonuc.Hata = Hata;
            }
            finally
            {
                Kapat();
            }
            return Sonuc;
        }
        public IslemDurum KomutCalistirWithExecuteScaler(string Sql, Dictionary<string, object> PrmListesi = null)
        {
            IslemDurum Sonuc = new IslemDurum();
            Ac();
            try
            {
                MySql.Data.MySqlClient.MySqlCommand Komut = new MySql.Data.MySqlClient.MySqlCommand(Sql, this.MySqlCLient);
                if (PrmListesi != null)
                {
                    foreach (var Prm in PrmListesi)
                    {
                        Komut.Parameters.Add(Prm.Key,Prm.Value);
                    }
                }
                Sonuc.Deger = Komut.ExecuteScalar();
                Sonuc.Durum = true;
                Sonuc.Mesaj = "Başarılı";
            }
            catch (Exception Hata)
            {
                Sonuc.Durum = false;
                Sonuc.Mesaj = "Başarısız";
                Sonuc.Hata = Hata;
            }
            finally
            {
                Kapat();
            }
            return Sonuc;
        }
        public IslemDurum KomutCalistirWithExecuteReader(string Sql,Dictionary<string,object> PrmListesi=null)
        {
            IslemDurum Sonuc = new IslemDurum();
            Ac();
            try
            {
                MySql.Data.MySqlClient.MySqlCommand Komut = new MySql.Data.MySqlClient.MySqlCommand(Sql, this.MySqlCLient);
                if (PrmListesi!=null)
                {
                    foreach (var Prm in PrmListesi)
                    {
                        Komut.Parameters.Add(Prm.Key, Prm.Value);
                    }
                }
                DataTable DtTable = new DataTable();
                MySql.Data.MySqlClient.MySqlDataAdapter MySqlDataAdapter=new MySql.Data.MySqlClient.MySqlDataAdapter(Komut);
                MySqlDataAdapter.Fill(DtTable);
                Sonuc.Deger = DtTable;
                Sonuc.Durum = true;
                Sonuc.Mesaj = "Başarılı";
            }
            catch (Exception Hata)
            {
                Sonuc.Durum = false;
                Sonuc.Mesaj = "Başarısız";
                Sonuc.Hata = Hata;
            }
            finally
            {
                Kapat();
            }
            return Sonuc;
        }
        public void Dispose()
        {
            Kapat();
            MySqlCLient.Dispose();
        }
    }
}
