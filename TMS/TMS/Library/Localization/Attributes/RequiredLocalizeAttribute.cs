﻿using System.ComponentModel.DataAnnotations;

namespace TMS.Localization.Attributes
{
    public class RequiredLocalizedAttribute : RequiredAttribute
    {
        protected string FormatErrorMessage()
        {
            return Localization.Localize("required");
        }
    }
}