﻿using System;

namespace TMS.Core.Lib
{
    public static class DecimalExtentions
    {
        public static string ToParaFormat(this decimal Nesne)
        {
            string Sonuc = "0";
            Sonuc = (Nesne == null ? "0" : string.Format("{0:C}", Nesne));
            return Sonuc;
        }

        public static decimal ToOran(this decimal Nesne, decimal Nesne2)
        {
            decimal Sonuc = 0;
            if (Nesne < 0)
            {
                Nesne = Nesne * -1;
            }
            if (Nesne > 0)
            {
                Sonuc = Math.Round(((Nesne * 100) / Nesne2), 2);
            }
            return Sonuc;
        }
    }
}