﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using TMS.Core.Data;

namespace TMS.Core.Lib
{
    public static class ObjectExtentions
    {
        public static DataTable ToDataTable<TSource>(this IEnumerable<TSource> source)
        {
            var props = typeof(TSource).GetProperties();

            var dt = new DataTable();
            dt.Columns.AddRange(
              props.Select(p => new DataColumn(p.Name, Nullable.GetUnderlyingType(
            p.PropertyType) ?? p.PropertyType)).ToArray()
            );

            source.ToList().ForEach(
              i => dt.Rows.Add(props.Select(p => p.GetValue(i, null)).ToArray())
            );

            return dt;
        }
        public static List<SelectListItem> GunListesiGetir(this object Nesne, bool IlkKayitBos = true, string Deger = "")
        {
            List<SelectListItem> Sonuc = new List<SelectListItem>();
            if (IlkKayitBos)
            {
                Sonuc.Add(new SelectListItem { Text = "Yok", Value = null });
            }
            Sonuc.AddRange(DateTimeFormatInfo
                       .CurrentInfo
                       .DayNames
                       .Select((monthName, index) => new SelectListItem
                       {
                           Value = (index + 1).ToString(),
                           Text = monthName,
                           Selected = ((index + 1).ToString() == Deger)
                       }));
            return Sonuc;
        }

        public static string SistemTanimGetir(this object Nesne, string Tip)
        {
            string Txt = Nesne.ToString();
            List<SelectListItem> Sonuc = new List<SelectListItem>();
            Sonuc.Add(new SelectListItem() { Value = "Sistem", Text = "Sistem ", Group = new SelectListGroup() { Name = "LogTip" } });
            Sonuc.Add(new SelectListItem() { Value = "Giris", Text = "Giriş", Group = new SelectListGroup() { Name = "LogTip" } });
            Sonuc.Add(new SelectListItem() { Value = "Cikis", Text = "Çıkış", Group = new SelectListGroup() { Name = "LogTip" } });
            Sonuc.Add(new SelectListItem() { Value = "SifreDegistirme", Text = "Şifre Değiştirme", Group = new SelectListGroup() { Name = "LogTip" } });
            Sonuc.Add(new SelectListItem() { Value = "ProfilDuzenleme", Text = "Profil Düzenleme", Group = new SelectListGroup() { Name = "LogTip" } });
            Sonuc.Add(new SelectListItem() { Value = "SifreOgrenme", Text = "Şifre Öğrenme", Group = new SelectListGroup() { Name = "LogTip" } });
            Sonuc.Add(new SelectListItem() { Value = "SifreOgrenmeHata", Text = "Şifre Öğrenme Hata", Group = new SelectListGroup() { Name = "LogTip" } });
            Sonuc.Add(new SelectListItem() { Value = "BrowserBilgi", Text = "Browser Bilgi", Group = new SelectListGroup() { Name = "LogTip" } });
            Sonuc.Add(new SelectListItem() { Value = "Kullanici", Text = "Kullanıcı", Group = new SelectListGroup() { Name = "KullaniciTur" } });
            Sonuc.Add(new SelectListItem() { Value = "Admin", Text = "Sistem Yönetici", Group = new SelectListGroup() { Name = "KullaniciTur" } });
            string Deger = Sonuc.FirstOrDefault(x => x.Value == Txt && x.Group.Name == Tip)?.Text;
            return Deger;
        }




        public static List<SelectListItem> AyListesiGetir(this object Nesne, bool IlkKayitBos = true, string Deger = "")
        {
            List<SelectListItem> Sonuc = new List<SelectListItem>();
            if (IlkKayitBos)
            {
                Sonuc.Add(new SelectListItem { Text = "Yok", Value = null });
            }
            Sonuc.AddRange(DateTimeFormatInfo
                       .CurrentInfo
                       .MonthNames
                       .Select((monthName, index) => new SelectListItem
                       {
                           Value = (index + 1).ToString(),
                           Text = monthName,
                           Selected = ((index + 1).ToString() == Deger)
                       }));
            return Sonuc;
        }
    }
}