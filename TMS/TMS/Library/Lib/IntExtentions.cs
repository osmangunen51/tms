﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace TMS.Core.Lib
{
    public static class IntExtentions
    {
        public static Dictionary<string, string> YilListesiGetir(this int Nesne, int Baslangic = 0, int Bitis = 0)
        {
            Dictionary<string, string> Sonuc = new Dictionary<string, string>();
            for (int Don = Baslangic; Don < Bitis; Don++)
            {
                Sonuc.Add(Don.ToString(), Don.ToString());
            }
            return Sonuc;
        }

        public static string ToAyAd(this int Nesne)
        {
            string Sonuc = new DateTime(DateTime.Now.Year, Nesne, 1).ToString("MMMM", CultureInfo.DefaultThreadCurrentCulture);
            return Sonuc;
        }
    }
}