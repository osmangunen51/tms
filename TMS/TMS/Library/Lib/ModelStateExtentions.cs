﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace TMS.Core.Lib
{
    public static class ModelStateExtentions
    {
        public static List<string> HataListesi(this ModelStateDictionary Kaynak)
        {
            List<string> Sonuc = new List<string>();
            foreach (var State in Kaynak)
            {
                if (State.Value.Errors.Count > 0)
                {
                    foreach (var item in State.Value.Errors.Select(error => error.ErrorMessage).ToList())
                    {
                        Sonuc.Add(item);
                    }
                }
            }
            return Sonuc;
        }
    }
}