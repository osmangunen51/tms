﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace TMS.Core.Lib
{
    public static class EnumIslem<T>
    {
        public static List<SelectListItem> GetSelectList()
        {
            return Enum.GetValues(typeof(T))
               .Cast<object>()
               .Select(i => new SelectListItem()
               {
                   Value = ((int)i).ToString()
                             ,
                   Text = i.ToString().Replace("_", " ")
               })
               .ToList();
        }
    }
}