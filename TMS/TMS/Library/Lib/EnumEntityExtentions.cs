﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace TMS.Core.Lib
{
    public static class EnumEntityExtentions
    {
        public static List<SelectListItem> CreateSelectList<T>(this List<T> entities, Func<T, object> funcToGetValue, Func<T, object> funcToGetText, bool IlkKayitBos = true, string BosIfade = "Yok", string DefaultDeger = "")
        {
            List<SelectListItem> Sonuc = new List<SelectListItem>();
            if (IlkKayitBos)
            {
                Sonuc.Add(new SelectListItem { Text = BosIfade, Value = DefaultDeger });
            }
            if (entities != null)
            {
                Sonuc.AddRange(entities
                                    .Select(x => new SelectListItem
                                    {
                                        Value = funcToGetValue(x).ToString(),
                                        Text = funcToGetText(x).ToString(),
                                        Selected = funcToGetValue(x).ToString() == DefaultDeger
                                    }).ToList());
            }
            return Sonuc;
        }

        public static SelectList CreateSelectWithGroupList<T>(this List<T> entities, Func<T, object> funcToGetValue, Func<T, object> funcToGetText, Func<T, object> funcToGetGroupText, bool IlkKayitBos = true, string BosIfade = "Yok")
        {
            SelectList SonucListe;
            List<SelectListItem> Sonuc = new List<SelectListItem>();
            if (IlkKayitBos)
            {
                Sonuc.Add(new SelectListItem() { Text = BosIfade, Value = null });
            }
            if (entities != null)
            {
                Sonuc.AddRange(entities
                                    .Select(x => new SelectListItem
                                    {
                                        Value = funcToGetValue(x).ToString(),
                                        Text = funcToGetText(x).ToString(),
                                        Group = new SelectListGroup()
                                        {
                                            Name = funcToGetGroupText(x).ToString(),
                                            Disabled = true
                                        }
                                    }).ToList());
            }
            SonucListe = new SelectList(Sonuc, "Value", "Text", "Group.Name", null, null);
            return SonucListe;
        }

        public static IEnumerable<TSource> Between<TSource, TResult>
        (
            this IEnumerable<TSource> source, Func<TSource, TResult> selector,
            TResult lowest, TResult highest
        )
            where TResult : IComparable<TResult>
        {
            return source.OrderBy(selector).
                SkipWhile(s => selector.Invoke(s).CompareTo(lowest) < 0).
                TakeWhile(s => selector.Invoke(s).CompareTo(highest) <= 0);
        }
    }
}