﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TMS.Core.Lib
{
    public static class EnumExtentions
    {
        public static string ToNameText<T>(this T source, int Deger)
        {
            string m = Enum.GetName(typeof(T), Deger);
            return m;
        }

        public static string GetDisplayName(this Enum value)
        {
            var type = value.GetType();

            var members = type.GetMember(value.ToString());
            if (members.Length == 0) throw new ArgumentException(String.Format("error '{0}' not found in type '{1}'", value, type.Name));

            var member = members[0];
            var attributes = member.GetCustomAttributes(typeof(DisplayAttribute), false);
            if (attributes.Length == 0) throw new ArgumentException(String.Format("'{0}.{1}' doesn't have DisplayAttribute", type.Name, value));

            var attribute = (DisplayAttribute)attributes[0];
            return attribute.GetName();
        }
    }
}