﻿using System;
using System.Drawing;

namespace TMS.Core.Lib
{
    public static class ColorExtentions
    {
        private static String HexConverter(Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }
    }
}