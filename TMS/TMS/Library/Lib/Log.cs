﻿using System;
using System.IO;

namespace TMS.Core.Lib
{
    public class Log
    {
        public string Dosya { get; }

        public Log(string Dosya)
        {
            this.Dosya = Dosya;
            if (!File.Exists(this.Dosya))
            {
                File.CreateText(Dosya).Dispose();
            }
        }

        public void WriteLog(String Log)
        {
            using (TextWriter txt = File.AppendText(Dosya))
            {
                txt.WriteLine(DateTime.Now.ToString() + " : " + Log);
                txt.Dispose();
            }
        }
    }
}