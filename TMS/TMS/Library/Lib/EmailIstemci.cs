﻿using System;
using System.Data;
using System.Net.Mail;

namespace TMS.Core.Lib
{
    public class EmailIstemci : IDisposable
    {
        #region -Alanlar-

        /// <summary>
        /// Server ServerAdı
        /// </summary>
        private string _ServerAd = "";

        /// <summary>
        /// Server ServerAdı
        /// </summary>
        public string ServerAd
        {
            get
            {
                return _ServerAd;
            }
            set
            {
                _ServerAd = value;
            }
        }

        /// <summary>
        /// Server ServerKullaniciAdı
        /// </summary>
        private string _KullaniciAd = "";

        /// <summary>
        /// Server ServerKullaniciAdı
        /// </summary>
        public string KullaniciAd
        {
            get
            {
                return _KullaniciAd;
            }
            set
            {
                _KullaniciAd = value;
            }
        }

        /// <summary>
        /// Server ServerSifre
        /// </summary>
        private string _Sifre = "";

        /// <summary>
        /// Server ServerSifre
        /// </summary>
        public string Sifre
        {
            get
            {
                return _Sifre;
            }
            set
            {
                _Sifre = value;
            }
        }

        /// <summary>
        /// Server ServerGonderilmeAd
        /// </summary>
        private string _GonderilmeAd = "";

        /// <summary>
        /// Server ServerGonderilmeAd
        /// </summary>
        public string GonderilmeAd
        {
            get
            {
                return _GonderilmeAd;
            }
            set
            {
                _GonderilmeAd = value;
            }
        }

        /// <summary>
        /// Server ServerPort
        /// </summary>
        private Int32 _Port = 0;

        /// <summary>
        /// Server ServerPort
        /// </summary>
        public Int32 Port
        {
            get
            {
                return _Port;
            }
            set
            {
                if ((value == 0) || (value.ToString() == ""))
                {
                    value = 25;
                }
                _Port = value;
            }
        }

        private MailMessage _Mail = new MailMessage();

        public MailMessage Mail
        {
            get
            {
                return _Mail;
            }
            set
            {
                _Mail = value;
            }
        }

        private bool _SslDurum = false;

        public bool SslDurum
        {
            get
            {
                return _SslDurum;
            }
            set
            {
                _SslDurum = value;
            }
        }

        private DataTable _GondermeSonuclari = new DataTable();

        public DataTable GondermeSonuclari
        {
            get
            {
                return _GondermeSonuclari;
            }
            set
            {
                _GondermeSonuclari = value;
            }
        }

        public SmtpClient Gonderici = new SmtpClient();

        #endregion -Alanlar-

        #region -Yapılandırcılar-

        public EmailIstemci()
        {
        }

        public EmailIstemci(string ServerAd, string KullaniciAd, string Sifre, Int32 Port, bool SslDurum)
        {
            this.ServerAd = ServerAd;
            this.KullaniciAd = KullaniciAd;
            this.Sifre = Sifre;
            this.Port = Port;
            this.SslDurum = SslDurum;
        }

        #endregion -Yapılandırcılar-

        #region -Metodlar

        public bool Gonder()
        {
            Gonderici.Credentials = new System.Net.NetworkCredential(KullaniciAd, Sifre);
            Gonderici.Port = Port;
            Gonderici.Host = ServerAd;
            Gonderici.EnableSsl = SslDurum;
            try
            {
                Gonderici.Send(Mail);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IslemDurum MailGonder()
        {
            IslemDurum Sonuc = new IslemDurum() { Durum = false, Mesaj = "" };
            Gonderici = new SmtpClient(ServerAd, Port);
            Gonderici.DeliveryMethod = SmtpDeliveryMethod.Network;
            Gonderici.UseDefaultCredentials = true;
            Gonderici.Credentials = new System.Net.NetworkCredential(KullaniciAd, Sifre);
            Gonderici.EnableSsl = SslDurum;
            try
            {
                Gonderici.Send(Mail);
                Sonuc = new IslemDurum() { Durum = true, Mesaj = "Mesaj Başarıyla Gönderildi." };
            }
            catch (Exception Hata)
            {
                Sonuc = new IslemDurum() { Durum = false, Mesaj = "Mesaj Gönrilirken Hata Oluştu. Ayrıntılar :" + Hata.Message, Hata = Hata };
            }
            return Sonuc;
        }

        #region IDisposable Members

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Members

        #endregion -Metodlar
    }
}