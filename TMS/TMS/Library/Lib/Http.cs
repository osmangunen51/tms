﻿using Microsoft.VisualBasic;
using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace TMS.Core.Lib
{
    public class Http
    {
        public static string responseFromServer(string url, WebHeaderCollection headerCollection, string postData, string requestMethod)
        {
            string functionReturnValue = null;
            functionReturnValue = string.Empty;
            Encoding encoding = Encoding.GetEncoding("ISO-8859-9");
            byte[] data = encoding.GetBytes(postData);

            if (requestMethod == "POST")
            {
                WebRequest oRequest = WebRequest.Create(url);
                oRequest.Headers = headerCollection;
                oRequest.ContentType = "application/x-www-form-urlencoded";
                oRequest.ContentLength = data.Length;
                oRequest.Method = requestMethod;
                ServicePointManager.ServerCertificateValidationCallback = validateRemoteCertificate;

                Stream newStream = oRequest.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Flush();
                newStream.Close();

                try
                {
                    HttpWebResponse oResponse = (HttpWebResponse)oRequest.GetResponse();
                    Stream dataStream = oResponse.GetResponseStream();
                    StreamReader dataReader = new StreamReader(dataStream, encoding);
                    string serverResponse = dataReader.ReadToEnd();

                    if (oResponse.StatusCode == HttpStatusCode.OK)
                    {
                        functionReturnValue = serverResponse;
                    }
                    else
                    {
                        functionReturnValue = string.Format("{0} : {1}", oResponse.StatusCode.ToString(), oResponse.StatusDescription);
                    }

                    dataReader.Close();
                    dataStream.Close();
                    oResponse.Close();
                }
                catch (Exception ex)
                {
                    functionReturnValue = ex.Message;
                }
            }
            else if (requestMethod == "GET")
            {
                WebRequest oRequest = WebRequest.Create(string.Format("{0}?{1}", url, postData));
                oRequest.Headers = headerCollection;
                oRequest.Method = "GET";
                ServicePointManager.ServerCertificateValidationCallback = validateRemoteCertificate;

                try
                {
                    HttpWebResponse oResponse = (HttpWebResponse)oRequest.GetResponse();
                    Stream dataStream = oResponse.GetResponseStream();
                    StreamReader dataReader = new StreamReader(dataStream, encoding);
                    string serverResponse = dataReader.ReadToEnd();

                    if (oResponse.StatusCode == HttpStatusCode.OK)
                    {
                        functionReturnValue = serverResponse;
                    }
                    else
                    {
                        functionReturnValue = string.Format("{0} : {1}", oResponse.StatusCode.ToString(), oResponse.StatusDescription);
                    }
                    dataReader.Close();
                    dataStream.Close();
                    oResponse.Close();
                }
                catch (Exception ex)
                {
                    functionReturnValue = ex.Message;
                }
            }
            return functionReturnValue;
        }

        public static bool validateRemoteCertificate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }

        public static bool isValidJson(string jsonResponse)
        {
            bool functionReturnValue = false;
            functionReturnValue = false;
            if (Strings.Left(jsonResponse, 1) == "{" | Strings.Left(jsonResponse, 1) == "[")
            {
                functionReturnValue = true;
            }
            return functionReturnValue;
        }
    }
}