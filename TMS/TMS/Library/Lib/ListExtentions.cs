﻿using System;
using System.Collections.Generic;

namespace TMS.Core.Lib
{
    public static class ListExtentions
    {
        public static void ForEachWithIndex<T>(this IEnumerable<T> enumerable, Action<T, int> handler)
        {
            int idx = 0;
            foreach (T item in enumerable)
                handler(item, idx++);
        }

        public static string ListToString(this List<string> Kaynak, string Ayirici)
        {
            string Sonuc = "";
            Sonuc = String.Join(Ayirici, Kaynak.ToArray());
            return Sonuc;
        }
    }
}