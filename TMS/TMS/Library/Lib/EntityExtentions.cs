﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Mvc;

namespace TMS.Core.Lib
{
    public static class EntityExtentions
    {

        public static T DeepClone<T>(this T toClone)
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, toClone);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }

        public static int CountB<TSource>(this IEnumerable<TSource> source)
        {
            if (source == null)
            {
            }
            ICollection<TSource> is2 = source as ICollection<TSource>;
            if (is2 != null)
            {
                return is2.Count;
            }
            ICollection is3 = source as ICollection;
            if (is3 != null)
            {
                return is3.Count;
            }
            int num = 0;
            using (IEnumerator<TSource> enumerator = source.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    num++;
                }
            }
            return num;
        }

        public static long LongCount<TSource>(this IEnumerable<TSource> source)
        {
            if (source == null) return 0;
            long count = 0;
            using (IEnumerator<TSource> e = source.GetEnumerator())
            {
                checked
                {
                    while (e.MoveNext()) count++;
                }
            }
            return count;
        }

        public static DataTable ToDataTable<TSource>(this IEnumerable<TSource> source)
        {
            var props = typeof(TSource).GetProperties();

            var dt = new DataTable();
            dt.Columns.AddRange(
              props.Select(p => new DataColumn(p.Name, Nullable.GetUnderlyingType(
            p.PropertyType) ?? p.PropertyType)).ToArray()
            );

            source.ToList().ForEach(
              i => dt.Rows.Add(props.Select(p => p.GetValue(i, null)).ToArray())
            );

            return dt;
        }

        public static List<SelectListItem> CreateSelectList<T>(this IList<T> entities, Func<T, object> funcToGetValue, Func<T, object> funcToGetText, bool IlkKayitBos = true)
        {
            List<SelectListItem> Sonuc = new List<SelectListItem>();
            if (IlkKayitBos)
            {
                Sonuc.Add(new SelectListItem { Text = "Yok", Value = null });
            }
            if (entities != null)
            {
                Sonuc.AddRange(entities
                                    .Select(x => new SelectListItem
                                    {
                                        Value = funcToGetValue(x).ToString(),
                                        Text = funcToGetText(x).ToString()
                                    }).ToList());
            }
            return Sonuc;
        }

        public static IEnumerable<TSource> Between<TSource, TResult>
        (
            this IEnumerable<TSource> source, Func<TSource, TResult> selector,
            TResult lowest, TResult highest
        )
            where TResult : IComparable<TResult>
        {
            return source.OrderBy(selector).
                SkipWhile(s => selector.Invoke(s).CompareTo(lowest) < 0).
                TakeWhile(s => selector.Invoke(s).CompareTo(highest) <= 0);
        }

        public static TValue GetPropertyAttributeValue<T, TOut, TAttribute, TValue>(this T Nesne,
        Expression<Func<T, TOut>> propertyExpression,
        Func<TAttribute, TValue> valueSelector)
        where TAttribute : Attribute
        {
            var expression = (MemberExpression)propertyExpression.Body;
            var propertyInfo = (PropertyInfo)expression.Member;
            var attr = propertyInfo.GetCustomAttributes(typeof(TAttribute), true).FirstOrDefault() as TAttribute;
            return attr != null ? valueSelector(attr) : default(TValue);
        }

        public static string DisplayName<T>(this T entities, Expression<Func<T, object>> Alan)
        {
            var expression = (MemberExpression)Alan.Body;
            var propertyInfo = (PropertyInfo)expression.Member;
            var pInfo = typeof(T).GetProperty(propertyInfo.Name)
                             .GetCustomAttribute<DisplayAttribute>();
            return pInfo.Name;
        }
    }
}