namespace TMS.Core.Service
{
    public class IlceService : ServiceBase<TMS.Core.Data.Ilce>, IIlceService
    {
        public IlceService()
            : base()
        { }

        public override void Update(TMS.Core.Data.Ilce entity)
        {
            base.Update(entity);
        }
    }

    public interface IIlceService : IService<TMS.Core.Data.Ilce>
    {
    }
}