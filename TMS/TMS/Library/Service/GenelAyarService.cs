namespace TMS.Core.Service
{
    public class GenelAyarService : ServiceBase<TMS.Core.Data.GenelAyar>, IGenelAyarService
    {
        public GenelAyarService()
            : base()
        { }

        public override void Update(TMS.Core.Data.GenelAyar entity)
        {
            base.Update(entity);
        }
    }

    public interface IGenelAyarService : IService<TMS.Core.Data.GenelAyar>
    {
    }
}