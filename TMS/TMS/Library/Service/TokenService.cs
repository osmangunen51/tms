using System.Collections.Generic;
using System.Linq;
using TMS.View.Genel;

namespace TMS.Core.Service
{
    public class TokenService : ServiceBase<TMS.Core.Data.Token>, ITokenService
    {
        public TokenService()
            : base()
        { }

        public override void Update(TMS.Core.Data.Token entity)
        {
            base.Update(entity);
        }

        public List<TokenListItem> TokenListesiGetir(int UserNo=0)
        {
            List<TokenListItem> Sonuc = new List<TokenListItem>();
            Sonuc = DbContext.Token.Where(x=>x.UserNo==UserNo | UserNo == 0)
                   .GroupJoin(
                       DbContext.Users,
                       Token => Token.UserNo,
                       Users => Users.UserId,
                       (Token, Users) => new { Token = Token, Users = Users })
                  .SelectMany(Snc => Snc.Users.DefaultIfEmpty(),
                  (x, y) =>
                  new TMS.View.Genel.TokenListItem
                  {
                      Token = x.Token,
                      Users = y
                  }).ToList();
            return Sonuc;
        }


    }

    public interface ITokenService : IService<TMS.Core.Data.Token>
    {
        List<TokenListItem> TokenListesiGetir(int UserNo = 0);
    }
}