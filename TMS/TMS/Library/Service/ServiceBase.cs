﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace TMS.Core.Service
{
    public abstract class ServiceBase<T> where T : class
    {
        #region Properties

        private Data.Entities _Context = null;
        private readonly IDbSet<T> dbSet;

        public Data.Entities DbContext
        {
            get { return _Context; }
        }

        protected ServiceBase()
        {
            _Context = new Data.Entities();
            dbSet = _Context.Set<T>();
        }

        public virtual void Add(T entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Clone(T entity)
        {
            dbSet.Add(entity);
            _Context.Entry(entity).State = EntityState.Added;
        }

        public virtual void Update(T entity)
        {
            dbSet.Attach(entity);
            _Context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            dbSet.Remove(entity);
        }

        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = dbSet.Where<T>(where).AsEnumerable();
            foreach (T obj in objects)
                dbSet.Remove(obj);
        }

        public virtual T GetById(int id)
        {
            return dbSet.Find(id);
        }

        public virtual IEnumerable<T> GetAll()
        {
            //return dbSet.ToList();
            return dbSet;
        }

        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where)
        {
            //return dbSet.Where(where).ToList();
            return dbSet.Where(where);
        }

        public T Get(Expression<Func<T, bool>> where)
        {
            return dbSet.Where(where).FirstOrDefault<T>();
        }

        public virtual void Create(T entity)
        {
            Add(entity);
        }

        public virtual void Commit()
        {
            _Context.SaveChanges();
        }

        #endregion Properties
    }
}