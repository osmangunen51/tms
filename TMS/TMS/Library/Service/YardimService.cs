namespace TMS.Core.Service
{
    public class YardimService : ServiceBase<TMS.Core.Data.Yardim>, IYardimService
    {
        public YardimService() : base()
        {
        }

        public override void Update(TMS.Core.Data.Yardim entity)
        {
            base.Update(entity);
        }
    }

    public interface IYardimService : IService<TMS.Core.Data.Yardim>
    {
    }
}