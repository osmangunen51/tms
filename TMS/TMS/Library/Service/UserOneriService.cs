﻿using System.Collections.Generic;
using System.Linq;
using TMS.Core.Data;
using TMS.View.Genel;

namespace TMS.Core.Service
{
    public class UserOneriService : ServiceBase<TMS.Core.Data.UserOneri>, IUserOneriService
    {
        public UserOneriService()
            : base()
        { }

        public override void Update(TMS.Core.Data.UserOneri entity)
        {
            base.Update(entity);
        }

        public List<UserOneriListItem> UserGelenOneriListesiGetir(int UserNo)
        {
            List<UserOneriListItem> Sonuc = new List<UserOneriListItem>();
            Sonuc = DbContext.UserOneri.Where(x => x.HedefUsersNo == UserNo)
                   .GroupJoin(
                       DbContext.Users,
                       UserOneri => UserOneri.KaynakUsersNo,
                       Users => Users.UserId,
                       (UserOneri, Users) => new { UserOneri = UserOneri, Users = Users })
                  .SelectMany(Snc => Snc.Users.DefaultIfEmpty(),
                  (x, y) =>
                  new TMS.View.Genel.UserOneriListItem
                  {
                      UserOneri = x.UserOneri,
                      Users = y
                  }).ToList();
            return Sonuc;
        }

        public List<UserOneriListItem> UserGonderilenOneriListesiGetir(int UserNo)
        {
            List<UserOneriListItem> Sonuc = new List<UserOneriListItem>();
            Sonuc = DbContext.UserOneri.Where(x => x.KaynakUsersNo == UserNo)
                   .Join(
                       DbContext.Users,
                       UserOneri => UserOneri.HedefUsersNo,
                       Users => Users.UserId,
                       (UserOneri, Users) => new { UserOneri = UserOneri, Users = Users })
                  .Select(Snc => new TMS.View.Genel.UserOneriListItem
                  {
                      UserOneri = Snc.UserOneri,
                      Users = Snc.Users
                  }).ToList();
            return Sonuc;
        }
    }

    public interface IUserOneriService : IService<TMS.Core.Data.UserOneri>
    {
        List<UserOneriListItem> UserGelenOneriListesiGetir(int UserNo);

        List<UserOneriListItem> UserGonderilenOneriListesiGetir(int UserNo);
    }
}