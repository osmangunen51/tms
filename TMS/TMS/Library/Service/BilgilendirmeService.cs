﻿namespace TMS.Core.Service
{
    public class BilgilendirmeService : ServiceBase<TMS.Core.Data.Bilgilendirme>, IBilgilendirmeService
    {
        public BilgilendirmeService()
            : base()
        { }
        public override void Update(TMS.Core.Data.Bilgilendirme entity)
        {
            base.Update(entity);
        }
    }

    public interface IBilgilendirmeService : IService<TMS.Core.Data.Bilgilendirme>
    {
    }
}