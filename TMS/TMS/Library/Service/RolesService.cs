namespace TMS.Core.Service
{
    public class RolesService : ServiceBase<TMS.Core.Data.Roles>, IRolesService
    {
        public RolesService()
            : base()
        { }

        public override void Update(TMS.Core.Data.Roles entity)
        {
            base.Update(entity);
        }
    }

    public interface IRolesService : IService<TMS.Core.Data.Roles>
    {
    }
}