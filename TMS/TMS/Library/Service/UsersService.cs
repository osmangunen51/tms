﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMS.Core.Data;
using TMS.Core.Lib;
using TMS.View.Genel;

namespace TMS.Core.Service
{
    public class UsersService : ServiceBase<TMS.Core.Data.Users>, IUsersService
    {
        public UsersService()
            : base()
        { }

        public override void Update(TMS.Core.Data.Users entity)
        {
            base.Update(entity);
        }

        public List<TMS.Core.Data.Roles> RolListesiGetir(int UserId)
        {
            List<TMS.Core.Data.Roles> Sonuc = new List<Roles>();
            Sonuc = DbContext.UserRoles.Where(x => x.User_UserId == UserId)
                  .Join(
                      DbContext.Roles,
                      UserRoles => UserRoles.Role_RoleId,
                      Roles => Roles.RoleId,
                      (UserRoles, Roles) => new { Avm = UserRoles, Roles = Roles })
                  .Select(Snc => Snc.Roles).ToList();
            return Sonuc;
        }

        public List<TMS.Core.Data.Users> UsersListesiGetir(string RolAdi = "")
        {
            List<TMS.Core.Data.Users> Sonuc = new List<TMS.Core.Data.Users>
                ();
            if (RolAdi == "")
            {
                Sonuc = DbContext.UserRoles
                .Join(
                    DbContext.Roles,
                    UserRoles => UserRoles.Role_RoleId,
                    Roles => Roles.RoleId,
                    (UserRoles, Roles) => new { UserRoles = UserRoles, Roles = Roles })
                .Join(
                    DbContext.Users,
                    UserRolesRoles => UserRolesRoles.UserRoles.User_UserId,
                    User => User.UserId,
                    (UserRolesRoles, User) => new { UserRolesRoles = UserRolesRoles, User = User })
                .Select(Snc => Snc.User).Distinct().ToList();
            }
            else
            {
                Sonuc = DbContext.UserRoles
                  .Join(
                      DbContext.Roles.Where(x => x.RoleName == RolAdi),
                      UserRoles => UserRoles.Role_RoleId,
                      Roles => Roles.RoleId,
                      (UserRoles, Roles) => new { UserRoles = UserRoles, Roles = Roles })
                  .Join(
                      DbContext.Users,
                      UserRolesRoles => UserRolesRoles.UserRoles.User_UserId,
                      User => User.UserId,
                      (UserRolesRoles, User) => new { UserRolesRoles = UserRolesRoles, User = User })
                  .Select(Snc => Snc.User).Distinct().ToList();
            }

            return Sonuc;
        }

        public bool RolAta(string RolAd, Data.Users User)
        {
            bool Sonuc = false;
            try
            {
                var Rol = DbContext.Roles.Where(c => c.RoleName == RolAd).SingleOrDefault();
                if (DbContext.Roles.Where(c => c.RoleName == RolAd).Count() == 0)
                {
                    DbContext.UserRoles.Add(new UserRoles() { Role_RoleId = Rol.RoleId, User_UserId = User.UserId });
                    DbContext.SaveChanges();
                    Sonuc = true;
                }
                else
                {
                    Sonuc = true;
                }
            }
            catch (Exception)
            {
                Sonuc = false;
            }
            return Sonuc;
        }
    }

    public interface IUsersService : IService<TMS.Core.Data.Users>
    {
        List<TMS.Core.Data.Roles> RolListesiGetir(int UserId);

        List<TMS.Core.Data.Users> UsersListesiGetir(string RolAdi = "");
    }
}