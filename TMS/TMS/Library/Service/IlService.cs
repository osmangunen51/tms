namespace TMS.Core.Service
{
    public class IlService : ServiceBase<TMS.Core.Data.Il>, IIlService
    {
        public IlService()
            : base()
        { }

        public override void Update(TMS.Core.Data.Il entity)
        {
            base.Update(entity);
        }
    }

    public interface IIlService : IService<TMS.Core.Data.Il>
    {
    }
}