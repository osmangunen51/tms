using System;
using TMS.View.Genel;

namespace TMS.Core.Service
{
    public class LogsService : ServiceBase<TMS.Core.Data.Logs>, ILogsService
    {
        public LogsService()
            : base()
        { }

        public override void Update(TMS.Core.Data.Logs entity)
        {
            base.Update(entity);
        }

        public bool Ekle(int UserNo, string Ip, LogTip Tip, string Aciklama)
        {
            bool Sonuc = false;
            try
            {
                if (Ip.Length > 255)
                {
                    Ip = Ip.Substring(0, 15);
                }

                if (Aciklama.Length > 255)
                {
                    Aciklama = Aciklama.Substring(0, 255);
                }
                Add(new Data.Logs() { UserNo = UserNo, Ip = Ip, Tip = (int)Tip, Aciklama = Aciklama, Date = DateTime.Now });
                Commit();
                Sonuc = true;
            }
            catch (Exception)
            {
                Sonuc = false;
            }
            return Sonuc;
        }

        public Data.Logs GetLog(int No)
        {
            var Sonuc = DbContext.Logs.Find(No);
            return Sonuc;
        }
    }

    public interface ILogsService : IService<TMS.Core.Data.Logs>
    {
        Data.Logs GetLog(int No);

        bool Ekle(int UserNo, string Ip, LogTip Tip, string Aciklama);
    }
}