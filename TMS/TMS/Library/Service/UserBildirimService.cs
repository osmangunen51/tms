﻿using System.Collections.Generic;
using System.Linq;
using TMS.Core.Data;
using TMS.View.Genel;

namespace TMS.Core.Service
{
    public class UserBildirimService : ServiceBase<TMS.Core.Data.UserBildirim>, IUserBildirimService
    {
        public UserBildirimService()
            : base()
        { }

        public override void Update(TMS.Core.Data.UserBildirim entity)
        {
            base.Update(entity);
        }

        public List<UserBildirimListItem> UserGelenBildirimListesiGetir(int UserNo)
        {
            List<UserBildirimListItem> Sonuc = new List<UserBildirimListItem>();
            Sonuc = DbContext.UserBildirim.Where(x => x.HedefUsersNo == UserNo)
                   .GroupJoin(
                       DbContext.Users,
                       UserBildirim => UserBildirim.KaynakUsersNo,
                       Users => Users.UserId,
                       (UserBildirim, Users) => new { UserBildirim = UserBildirim, Users = Users })
                  .SelectMany(Snc => Snc.Users.DefaultIfEmpty(),
                  (x, y) =>
                  new TMS.View.Genel.UserBildirimListItem
                  {
                      UserBildirim = x.UserBildirim,
                      Users = y
                  }).ToList();
            return Sonuc;
        }

        public List<UserBildirimListItem> UserGonderilenBildirimListesiGetir(int UserNo)
        {
            List<UserBildirimListItem> Sonuc = new List<UserBildirimListItem>();
            Sonuc = DbContext.UserBildirim.Where(x => x.KaynakUsersNo == UserNo)
                   .Join(
                       DbContext.Users,
                       UserBildirim => UserBildirim.HedefUsersNo,
                       Users => Users.UserId,
                       (UserBildirim, Users) => new { UserBildirim = UserBildirim, Users = Users })
                  .Select(Snc => new TMS.View.Genel.UserBildirimListItem
                  {
                      UserBildirim = Snc.UserBildirim,
                      Users = Snc.Users
                  }).ToList();
            return Sonuc;
        }
    }

    public interface IUserBildirimService : IService<TMS.Core.Data.UserBildirim>
    {
        List<UserBildirimListItem> UserGelenBildirimListesiGetir(int UserNo);

        List<UserBildirimListItem> UserGonderilenBildirimListesiGetir(int UserNo);
    }
}