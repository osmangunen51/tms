
using EEYS.View.Genel;
using System.Collections.Generic;
using System.Linq;

namespace EEYS.Core.Service
{
    public class IlacFirmaServiceService : ServiceBase<EEYS.Core.Data.IlacFirmaListItem>, IIlacFirmaServiceService
    {
        public IlacFirmaServiceService()
            : base()
        { }

        public override void Update(EEYS.Core.Data.IlacFirma entity)
        {
            base.Update(entity);
        }
        public List<IlacFirmaListItem> IlacFirmaListesiGetir()
        {
            List<IlacFirmaListItem> Sonuc = new List<IlacFirmaListItem>();
            Sonuc = this.DbContext.IlacFirma
                   .Join(
                       this.DbContext.Ilac,
                       IlacFirmaService => IlacFirmaService.IlacNo,
                       Ilac => Ilac.No,
                       (IlacFirmaService, Ilac) => new { IlacFirmaService = IlacFirmaService, Ilac = Ilac })
                  .Select(Snc => new EEYS.View.Genel.IlacFirmaListItem
                  {
                      IlacFirma= Snc.IlacFirma,
                      Ilac = Snc.Ilac,
                  }).ToList();
            return Sonuc;
        }
    }

    public interface IIlacFirmaServiceService : IService<EEYS.Core.Data.IlacFirmaService>
    {
    }
}