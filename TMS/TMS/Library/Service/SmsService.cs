﻿using System;
using System.IO;
using System.Net;
using System.Text;
using TMS.Core.Lib;

namespace TMS.Core.Service
{
    public class SmsService : IDisposable
    {
        private string KullaniciAd = "";
        private string Sifre = "";
        private string Originator = "";
        private string ApiAdres = "https://api.netgsm.com.tr/sms/send/xml";

        public SmsService()
        {
            KullaniciAd = System.Configuration.ConfigurationManager.AppSettings["Sms:KullaniciAd"].ToString();
            Sifre = System.Configuration.ConfigurationManager.AppSettings["Sms:Sifre"].ToString();
            Originator = System.Configuration.ConfigurationManager.AppSettings["Sms:Originator"].ToString();
            ApiAdres = System.Configuration.ConfigurationManager.AppSettings["Sms:ApiAdres"].ToString();
        }

        public TMS.Core.Lib.IslemDurum Gonder(string Numara, string Mesaj)
        {
            TMS.Core.Lib.IslemDurum Sonuc = new Lib.IslemDurum();
            try
            {
                string xmlDesen = $"<?xml version='1.0' encoding='UTF-8'?> <mainbody> <header> <company dil='TR'>Netgsm</company> <usercode>{KullaniciAd}</usercode> <password>{Sifre}</password> <type>1:n</type> <msgheader>{Originator}</msgheader> </header> <body> <msg> <![CDATA[{Mesaj}]]> </msg> <no>{Numara}</no></body> </mainbody>";
                string ApiAdres = this.ApiAdres;
                WebRequest request = WebRequest.Create(ApiAdres);
                request.Method = "POST";
                byte[] byteArray = Encoding.UTF8.GetBytes(xmlDesen);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                WebResponse response = request.GetResponse();
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                if (responseFromServer.Contains("00"))
                {
                    Sonuc.Durum = true;
                    Sonuc.Mesaj = "Başarıyla Gönderildi";
                    Sonuc.Tip = IslemDurum.IslemMesajTip.Basarili;
                }
                else
                {
                    Sonuc.Durum = false;
                    Sonuc.Mesaj = "Başarıyla Hata Oldu.";
                    Sonuc.Tip = IslemDurum.IslemMesajTip.Hata;
                }
            }
            catch (Exception Hata)
            {
                Sonuc.Durum = false;
                Sonuc.Mesaj = Hata.Source;
                Sonuc.Tip = IslemDurum.IslemMesajTip.Hata;
            }
            return Sonuc;
        }

        public void Dispose()
        {
        }
    }
}