using System;

namespace TMS.Core.Service
{
    public class PerformansService : ServiceBase<TMS.Core.Data.Performans>, IPerformansService
    {
        public PerformansService()
            : base()
        { }

        public override void Update(TMS.Core.Data.Performans entity)
        {
            base.Update(entity);
        }

        public Data.Performans GetPerformans(int No)
        {
            var Sonuc = DbContext.Performans.Find(No);
            return Sonuc;
        }

        public bool Ekle(int Member, int Tip, string Aciklama)
        {
            bool Sonuc = false;
            try
            {
                Add(new Data.Performans() { Member = Member, Tip = Tip, Aciklama = Aciklama, Date = DateTime.Now });
                Commit();
                Sonuc = true;
            }
            catch (Exception)
            {
                Sonuc = false;
            }
            return Sonuc;
        }

        public bool Ekle(int Member, int Tip, string Aciklama, float Deger)
        {
            bool Sonuc = false;
            try
            {
                Add(new Data.Performans() { Member = Member, Tip = Tip, Aciklama = Aciklama, Date = DateTime.Now, Deger = Deger });
                Commit();
                Sonuc = true;
            }
            catch (Exception)
            {
                Sonuc = false;
            }
            return Sonuc;
        }
    }

    public interface IPerformansService : IService<TMS.Core.Data.Performans>
    {
        Data.Performans GetPerformans(int No);

        bool Ekle(int Member, int Tip, string Aciklama);

        bool Ekle(int Member, int Tip, string Aciklama, float Deger);
    }
}