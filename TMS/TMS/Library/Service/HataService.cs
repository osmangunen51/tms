namespace TMS.Core.Service
{
    public class HataService : ServiceBase<TMS.Core.Data.Hata>, HatalService
    {
        public HataService()
            : base()
        { }

        public override void Update(TMS.Core.Data.Hata entity)
        {
            base.Update(entity);
        }
    }

    public interface HatalService : IService<TMS.Core.Data.Hata>
    {
    }
}