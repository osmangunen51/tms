using System.Collections.Generic;
using System.Linq;
using TMS.Core.Data;
using TMS.View.Genel;

namespace TMS.Core.Service
{
    public class SiparisService : ServiceBase<TMS.Core.Data.Siparis>, ISiparisService
    {
        public SiparisService()
            : base()
        {

        }
        public override void Update(TMS.Core.Data.Siparis entity)
        {
            base.Update(entity);
        }



        public List<SiparisListItem> SatisListesiGetir(int UserNo = 0)
        {
            List<SiparisListItem> Sonuc = new List<SiparisListItem>();
            var TokenListesi = DbContext.Token.Where(x => x.UserNo == UserNo);
            var TumTokenListesi = DbContext.Token.ToList();
            List<int> TokenNoListesi = TokenListesi.Select(x => x.No).Distinct().ToList();
            Sonuc=DbContext.Siparis.Where(x => TokenNoListesi.Contains((int)x.KaynakTokenNo)).Select(x=>
                new TMS.View.Genel.SiparisListItem
                {
                    Siparis=x
                }
                ).ToList();
            foreach (var item in Sonuc)
            {
                item.KaynakToken = TumTokenListesi.Where(x => x.No == item.Siparis.KaynakTokenNo).FirstOrDefault();
                if (item.KaynakToken==null)
                {
                    item.KaynakToken = new Token(){};
                }
            }
            return Sonuc;
        }


        public SiparisListItem SatisListesiGetirWithSiparisNo(int SiparisNo = 0)
        {
            SiparisListItem Sonuc = new SiparisListItem();
            var Siparis = DbContext.Siparis.Find(SiparisNo);
            if (Siparis!=null)
            {
                var User = DbContext.Users.Find(Siparis.KaynakTokenNo);
                var Token = DbContext.Token.FirstOrDefault(x => x.No == Siparis.KaynakTokenNo);
                Sonuc = new SiparisListItem()
                {
                    Siparis = Siparis,
                    KaynakToken = Token,
                    User = User
                };
            }
            return Sonuc;
        }


        public List<SiparisListItem> TumSiparisSatisListesiGetir()
        {
            List<SiparisListItem> Sonuc = new List<SiparisListItem>();
            var TumTokenListesi = DbContext.Token.ToList();
            List<int> TokenNoListesi = TumTokenListesi.Select(x => x.No).Distinct().ToList();
            Sonuc = DbContext.Siparis.Where(x => TokenNoListesi.Contains((int)x.KaynakTokenNo)).Select(x =>
                  new TMS.View.Genel.SiparisListItem
                  {
                      Siparis = x
                  }
                ).ToList();
            foreach (var item in Sonuc)
            {
                item.KaynakToken = TumTokenListesi.Where(x => x.No == item.Siparis.KaynakTokenNo).FirstOrDefault();
                if (item.KaynakToken == null)
                {
                    item.KaynakToken = new Token() {No=-1,UserNo=-1};
                }
                item.User = DbContext.Users.FirstOrDefault(x => x.UserId == item.KaynakToken.UserNo);
                if (item.User==null)
                {
                    item.User = new Users() { UserId=1};
                }
            }
            return Sonuc;
        }

    }

    public interface ISiparisService : IService<TMS.Core.Data.Siparis>
    {
        List<SiparisListItem> SatisListesiGetir(int UserNo = 0);
    }
}