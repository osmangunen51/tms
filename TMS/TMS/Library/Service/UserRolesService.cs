﻿namespace TMS.Core.Service
{
    public class UserRolesService : ServiceBase<TMS.Core.Data.UserRoles>, IUserRolesService
    {
        public UserRolesService()
            : base()
        { }

        public override void Update(TMS.Core.Data.UserRoles entity)
        {
            base.Update(entity);
        }
    }

    public interface IUserRolesService : IService<TMS.Core.Data.UserRoles>
    {
    }
}