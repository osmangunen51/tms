using System.Collections.Generic;
using System.Linq;
using TMS.Core.Data;
using TMS.Core.Lib;
using TMS.View.Genel;

namespace TMS.Core.Service
{
    public class KullaniciService : ServiceBase<TMS.Core.Data.Kullanici>, IKullaniciService
    {
        public KullaniciService() : base()
        {
        }

        public override void Update(TMS.Core.Data.Kullanici entity)
        {
            base.Update(entity);
        }


        public List<KullaniciListItem> KullaniciListesiGetir()
        {
            List<KullaniciListItem> Sonuc = new List<KullaniciListItem>();
            Sonuc = DbContext.Kullanici
                   .GroupJoin(
                       DbContext.Users,
                       Kullanici => Kullanici.UserNo,
                       Users => Users.UserId,
                       (Kullanici, Users) => new { Kullanici = Kullanici, Users = Users })
                  .SelectMany(Snc => Snc.Users.DefaultIfEmpty(),
                  (x, y) =>
                  new TMS.View.Genel.KullaniciListItem
                  {
                      Kullanici = x.Kullanici,
                      Users = y
                  }).ToList();
            return Sonuc;
        }

        public bool UserOneriKritikGunGuncelle(int UserId)
        {
            bool Sonuc = false;
            try
            {
                var Kullanici = Get(x => x.UserNo == UserId);
                if (Kullanici != null)
                {
                    int Deger = (int)View.Genel.UserOneriTip.Kritik_G�nler;
                    var KritikUserOneriListesi = DbContext.UserOneri.Where(
                                x => x.Tip == Deger
                            ).ToList();
                    DbContext.UserOneri.RemoveRange(KritikUserOneriListesi);
                    DbContext.SaveChanges();
                }
            }
            catch (System.Exception)
            {
                Sonuc = false;
            }
            return Sonuc;
        }
    }

    public interface IKullaniciService : IService<TMS.Core.Data.Kullanici>
    {
        bool UserOneriKritikGunGuncelle(int KullaniciNo);
    }
}