﻿using System;
using System.ComponentModel.DataAnnotations;
using TMS.Core.Data.MetaData;

namespace TMS.Core.Data
{
    [MetadataType(typeof(IlceMetaData))]
    public partial class Ilce : BaseData
    {
    }

    public class IlceMetaData
    {
        public int No { get; set; }

        [StringLength(maximumLength: 255, MinimumLength = 3, ErrorMessage = "{0} En Az 3-255 Karakter Arasında Olmalıdır.")]
        [Display(Name = "İlçe Adı")]
        public string Ad { get; set; }

        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "İl No")]
        public int IlNo { get; set; }

        [Display(Name = "Sıra")]
        public Nullable<int> Sira { get; set; }

        public virtual Il Il { get; set; }
    }
}