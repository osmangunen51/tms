using System.ComponentModel.DataAnnotations;
using TMS.Core.Data.MetaData;

namespace TMS.Core.Data
{
    [MetadataType(typeof(LogsMetaData))]
    public partial class Logs : BaseData
    {
    }

    public class LogsMetaData
    {
    }
}