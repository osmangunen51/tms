﻿using System;

namespace TMS.Core.Data.MetaData
{
    [Serializable]
    public class BaseData : ICloneable
    {
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}