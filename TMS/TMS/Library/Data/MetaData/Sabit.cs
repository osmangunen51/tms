using System;
using System.ComponentModel.DataAnnotations;
using TMS.Core.Data.MetaData;

namespace TMS.Core.Data
{
    [MetadataType(typeof(SabitMetaData))]
    public partial class Sabit : BaseData
    {
    }

    public class SabitMetaData
    {
        [Display(Name = "Tarih")]
        public Nullable<System.DateTime> Tarih { get; set; }

        [Display(Name = "Durum")]
        public short Durum { get; set; }
    }
}