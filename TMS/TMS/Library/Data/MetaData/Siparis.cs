﻿using System.ComponentModel.DataAnnotations;
using TMS.Core.Data.MetaData;

namespace TMS.Core.Data
{
    [MetadataType(typeof(SiparisMetaData))]
    public partial class Siparis : BaseData
    {
    }

    public class SiparisMetaData
    {
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Kaynak Token")]
        public int KaynakTokenNo { get; set; }

        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Hedef Token")]
        public int HedefTokenNo { get; set; }


        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Miktar Token")]
        public decimal Miktar { get; set; }

        [Display(Name = "Token Aktif/Pasif")]
        public bool Durum { get; set; }
    }
}