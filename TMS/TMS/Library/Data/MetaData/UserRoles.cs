﻿using System.ComponentModel.DataAnnotations;
using TMS.Core.Data.MetaData;

namespace TMS.Core.Data
{
    [MetadataType(typeof(UserRolesMetaData))]
    public partial class UserRoles : BaseData
    {
    }

    public class UserRolesMetaData
    {
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Rol")]
        public int Role_RoleId { get; set; }
    }
}