﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TMS.Core.Data.MetaData
{
    public class BaseMetaData : BaseData
    {
        [Display(Name = "No")]
        public int No { get; set; }

        [Display(Name = "Tarih")]
        public Nullable<System.DateTime> Tarih { get; set; }

        [Display(Name = "Durum")]
        public Nullable<short> Durum { get; set; }
    }
}