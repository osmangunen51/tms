﻿using System.ComponentModel.DataAnnotations;
using TMS.Core.Data.MetaData;

namespace TMS.Core.Data
{
    [MetadataType(typeof(RolesMetaData))]
    public partial class Roles : BaseData
    {
    }

    public class RolesMetaData
    {
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Rol Adı")]
        public string Ad { get; set; }

        [Display(Name = "Tanımlama")]
        public string Description { get; set; }
    }
}