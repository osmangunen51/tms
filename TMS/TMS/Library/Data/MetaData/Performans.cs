using System;
using System.ComponentModel.DataAnnotations;
using TMS.Core.Data.MetaData;

namespace TMS.Core.Data
{
    [MetadataType(typeof(PerformansMetaData))]
    public partial class Performans : BaseData
    {
    }

    public class PerformansMetaData
    {
        [Display(Name = "Tarih")]
        public Nullable<System.DateTime> Tarih { get; set; }

        [Display(Name = "Durum")]
        public short Durum { get; set; }
    }
}