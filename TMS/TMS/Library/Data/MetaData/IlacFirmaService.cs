using EEYS.Core.Data.MetaData;
using System;
using System.ComponentModel.DataAnnotations;

namespace EEYS.Core.Data
{
    [MetadataType(typeof(IlacFirmaServiceMetaData))]
    public partial class IlacFirmaService
    {
    }

    public class IlacFirmaServiceMetaData:Base

    {
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Adı")]
        public string Ad { get; set; }


        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Ilac")]
        public Nullable<int> IlacNo { get; set; }
    }
}