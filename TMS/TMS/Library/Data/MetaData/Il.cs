﻿using System.ComponentModel.DataAnnotations;
using TMS.Core.Data.MetaData;

namespace TMS.Core.Data
{
    [MetadataType(typeof(IlMetaData))]
    public partial class Il : BaseData
    {

    }

    public class IlMetaData
    {
        [StringLength(maximumLength: 255, MinimumLength = 3, ErrorMessage = "{0} En Az 3-255 Karakter Arasında Olmalıdır.")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Il Adı")]
        public string Ad { get; set; }

        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Il Aktif/Pasif")]
        public bool Durum { get; set; }
    }
}