using System;
using System.ComponentModel.DataAnnotations;
using TMS.Core.Data.MetaData;

namespace TMS.Core.Data
{
    [MetadataType(typeof(YardimMetaData))]
    public partial class Yardim : BaseData
    {
    }

    public class YardimMetaData
    {
        [Display(Name = "Tarih")]
        public Nullable<System.DateTime> Tarih { get; set; }

        [Display(Name = "Durum")]
        public short Durum { get; set; }
    }
}