﻿using System.ComponentModel.DataAnnotations;
using TMS.Core.Data.MetaData;

namespace TMS.Core.Data
{
    [MetadataType(typeof(TokenMetaData))]
    public partial class Token : BaseData
    {

    }

    public class TokenMetaData
    {
        [StringLength(maximumLength: 255, MinimumLength = 3, ErrorMessage = "{0} En Az 3-255 Karakter Arasında Olmalıdır.")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Token Adı")]
        public string Ad { get; set; }

        [Display(Name = "Token Açıklama")]
        public string Aciklama { get; set; }


        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Kullanıcı")]
        public int UserNo { get; set; }

        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Miktar Token")]
        public decimal Miktar { get; set; }

        [Display(Name = "Token Aktif/Pasif")]
        public bool Durum { get; set; }
    }
}