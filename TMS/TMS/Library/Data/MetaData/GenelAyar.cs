using System;
using System.ComponentModel.DataAnnotations;
using TMS.Core.Data.MetaData;

namespace TMS.Core.Data
{
    [MetadataType(typeof(GenelAyarMetaData))]
    public partial class GenelAyar : BaseData
    {
    }

    public class GenelAyarMetaData
    {
        [Display(Name = "Tarih")]
        public Nullable<System.DateTime> Tarih { get; set; }

        [Display(Name = "Durum")]
        public short Durum { get; set; }
    }
}