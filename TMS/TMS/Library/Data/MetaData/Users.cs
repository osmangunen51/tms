﻿using System;
using System.ComponentModel.DataAnnotations;
using TMS.Core.Data.MetaData;

namespace TMS.Core.Data
{
    [MetadataType(typeof(UsersMetaData))]
    public partial class Users : BaseData
    {
    }

    public class UsersMetaData
    {
        //[Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Kullanıcı Adı")]
        public string Username { get; set; }

        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Telefon")]
        public string Telefon { get; set; }

        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Şifre")]
        public string Password { get; set; }

        [Display(Name = "Ad")]
        public string FirstName { get; set; }

        [Display(Name = "Soyad")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Kullanıcı Aktif/Pasif")]
        public bool IsActive { get; set; }

        //[Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Display(Name = "Oluşturulma Tarihi")]
        public DateTime CreateDate { get; set; }
    }
}