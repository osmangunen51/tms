using System;
using System.ComponentModel.DataAnnotations;
using TMS.Core.Data.MetaData;

namespace TMS.Core.Data
{
    [MetadataType(typeof(ProgramMetaData))]
    public partial class Program : BaseData
    {
    }

    public class ProgramMetaData
    {
        [Display(Name = "Tarih")]
        public Nullable<System.DateTime> Tarih { get; set; }

        [Display(Name = "Durum")]
        public short Durum { get; set; }
    }
}