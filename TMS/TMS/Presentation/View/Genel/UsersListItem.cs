﻿namespace TMS.View.Genel
{
    public class UsersListItem
    {
        public Core.Data.Users Users { get; set; }

        public UsersListItem()
        {
            Users = new Core.Data.Users();
        }
    }
}