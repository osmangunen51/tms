﻿namespace TMS.View.Genel
{
    /// <summary>
    /// Defines the <see cref="GirisView" />
    /// </summary>
    public class GirisView : BaseView
    {
        /// <summary>
        /// Gets or sets the GirisTur
        /// </summary>
        public bool GirisDurum { get; set; } = false;

        /// <summary>
        /// Gets or sets the LoginView
        /// </summary>
        public TMS.View.Membership.LoginViewModel LoginView { get; set; }

        /// <summary>
        /// Gets or sets the ReturnUrl
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GirisView"/> class.
        /// </summary>
        public GirisView()
        {
            IslemMesaj = new View.Genel.IslemMesaj();
            LoginView = new View.Membership.LoginViewModel();
        }
    }
}