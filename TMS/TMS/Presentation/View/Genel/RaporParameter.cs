﻿using System.Collections.Generic;

namespace TMS.View.Genel
{
    public class RaporParameter
    {
        public string Anahtar { get; set; }
        public string Deger { get; set; }
        public string Baslik { get; set; }
        public Dictionary<string, string> Liste { get; set; }
        public RaporParameterTip Tip { get; set; }

        public bool IlkKayitBos { get; set; } = true;

        public RaporParameter()
        {
            Anahtar = "";
            Deger = "";
            Baslik = "";
        }
    }
}