﻿namespace TMS.View.Genel
{
    public class EkranYonlendirme
    {
        public string Url { get; set; }
        public string Mesaj { get; set; }
        public int Saniye { get; set; }

        public bool LinkDurum { get; set; }

        public EkranYonlendirme()
        {
            Url = "";
            Mesaj = "";
            Saniye = 10;
            LinkDurum = true;
        }
    }
}