﻿using System;
using System.Collections.Generic;

namespace TMS.View.Genel
{
    public class AktiviteTakvimEventItem
    {
        public int start { get; set; } = 0;
        public int end { get; set; } = 100;
        public int width { get; set; } = 100;
        public string title { get; set; } = "";
        public string renk { get; set; } = "";

        public string description { get; set; } = "";

        public AktiviteTakvimEventItem()
        {
        }
    }

    public class AktiviteTakvimGunItem
    {
        public int No { get; set; } = 0;
        public DateTime Tarih { get; set; }

        public AktiviteTakvimGunItem()
        {
        }
    }

    public class AktiviteTakvimBaslikItem
    {
        public int start { get; set; } = 0;
        public int end { get; set; } = 100;
        public string title { get; set; } = "";

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public List<AktiviteTakvimGunItem> GunListesi { get; set; } = new List<AktiviteTakvimGunItem>();

        public AktiviteTakvimBaslikItem()
        {
        }

        public void GunHesapla()
        {
            int EklenecekGunSayisi = end - start;
            EndDate = StartDate.AddDays(EklenecekGunSayisi);
            int Adet = 0;
            for (int i = start; i < end; i++)
            {
                DateTime Gun = StartDate.AddDays(Adet);
                GunListesi.Add(
                 new AktiviteTakvimGunItem()
                 {
                     No = i,
                     Tarih = Gun
                 }
                );
                Adet++;
            }
        }
    }

    public class AktiviteTakvim
    {
        public string timeType { get; set; } = "";
        public bool useTimeSuffix { get; set; } = false;
        public string initTime { get; set; } = "";
        public int startTime { get; set; } = 500;
        public int endTime { get; set; } = 2500;
        public int navigateAmount { get; set; } = 500;
        public int markerIncrement { get; set; } = 50;
        public int SelectedEventIndex { get; set; } = 0;

        public List<AktiviteTakvimBaslikItem> BaslikListesi = new List<AktiviteTakvimBaslikItem>();
        public List<AktiviteTakvimEventItem> OlayListesi = new List<AktiviteTakvimEventItem>();

        public AktiviteTakvim()
        {
        }
    }
}