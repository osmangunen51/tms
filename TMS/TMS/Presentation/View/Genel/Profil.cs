﻿namespace TMS.View.Genel
{
    public class Profil : GenelListView
    {
        public Core.Data.Users AktifKayit { get; set; }

        public Profil()
        {
            AktifKayit = new Core.Data.Users();
        }
    }
}