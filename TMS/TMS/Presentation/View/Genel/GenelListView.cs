﻿using System.Collections.Generic;
using SelectListItem = System.Web.Mvc.SelectListItem;

namespace TMS.View.Genel
{
    public class Pager
    {
        public int Size { get; set; }
        public int Index { get; set; }
        public int Total { get; set; }
        public string IndexTxt { get; set; }
        public string SizeTxt { get; set; }

        public List<SelectListItem> SizeListesi { get; set; }

        public Pager()
        {
            Size = 1000;
            Index = 0;
            IndexTxt = Index.ToString();
            SizeTxt = Size.ToString();
            Total = 0;
            SizeListesi = new List<SelectListItem>()
            {
                new SelectListItem() {Text="10",Value="10"},
                new SelectListItem() {Text="25",Value="25",Selected=true},
                new SelectListItem() {Text="50",Value="50"},
                new SelectListItem() {Text="75",Value="75"},
                new SelectListItem() {Text="100",Value="100"},
            };
        }
    }

    public class Filtreleme
    {
        public string AraTxt { get; set; }
        public string Alan { get; set; }
        public string Tip { get; set; }

        public List<SelectListItem> AlanListesi { get; set; }

        public List<SelectListItem> AramaTipListesi { get; set; }

        public Filtreleme()
        {
            AraTxt = "";
            Alan = "";
            Tip = "";
            AlanListesi = new List<SelectListItem>()
            {
            };
            AramaTipListesi = new List<SelectListItem>()
            {
            };
        }
    }

    public class Sort
    {
        public string Alan { get; set; }

        public string AlanTxt { get; set; }
        public int OrderType { get; set; }

        public Sort()
        {
            Alan = "";
            OrderType = 1;
            AlanTxt = Alan;
        }
    }

    public class SiraDegistirme
    {
        public int EskiIndex { get; set; }
        public int YeniIndex { get; set; }

        public SiraDegistirme()
        {
            EskiIndex = -1;
            YeniIndex = -1;
        }
    }

    public class GenelListView : BaseView
    {
        public Pager Sayfalama { get; set; }
        public Sort Siralama { get; set; }

        public SiraDegistirme SiraDegistirme { get; set; }
        public Filtreleme Filtreleme { get; set; }

        public GenelListView()
        {
            Sayfalama = new Pager();
            Siralama = new Sort();
            Filtreleme = new Filtreleme();
            SiraDegistirme = new SiraDegistirme();
        }
    }
}