﻿namespace TMS.View.Genel
{
    public class TokenListItem
    {
        public TMS.Core.Data.Token Token { get; set; } = new Core.Data.Token();
        public TMS.Core.Data.Users Users { get; set; } = new Core.Data.Users();

        public TokenListItem()
        {
        }
    }
}