﻿using System.ComponentModel.DataAnnotations;

namespace TMS.View.Genel
{
    public class ChangePassword : BaseView
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mevcut Şifre")]
        public string Sifre { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Şifre")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Şifre Onay")]
        [Compare("NewPassword", ErrorMessage = "Şifreler Eşleşmiyor..!")]
        public string ConfirmPassword { get; set; }
    }
}