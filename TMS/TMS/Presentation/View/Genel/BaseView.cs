﻿using System.Collections.Generic;
using SelectListItem = System.Web.Mvc.SelectListItem;

namespace TMS.View.Genel
{
    public class BaseView
    {
        public string Baslik { get; set; }
        public List<SelectListItem> EkmekKirintisi { get; set; }
        public IslemMesaj IslemMesaj { get; set; } = new IslemMesaj();

        public BaseView()
        {
            IslemMesaj = new IslemMesaj();
            EkmekKirintisi = new List<SelectListItem>();
            Baslik = "";
        }
    }
}