﻿namespace TMS.View.Genel
{
    public class Secim
    {
        public string HedefKontrolId { get; set; } = "";
        public int Deger { get; set; } = -1;
        public string Baslik { get; set; } = "";
        public string LabelClass { get; set; } = "";
        public string ControlClass { get; set; } = "";

        public Secim()
        {
        }
    }
}