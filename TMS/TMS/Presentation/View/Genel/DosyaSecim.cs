﻿namespace TMS.View.Genel
{
    public class DosyaSecim
    {
        public string HedefKontrolId { get; set; } = "";
        public string Deger { get; set; } = "";
        public string Baslik { get; set; } = "";
        public string LabelClass { get; set; } = "";
        public string ControlClass { get; set; } = "";

        public string StateKontrolId { get; set; } = "";

        public DosyaSecim()
        {
        }
    }
}