﻿using System.Collections.Generic;
using SelectListItem = System.Web.Mvc.SelectListItem;

namespace TMS.View.Genel
{
    public class AjaxResult : BaseView
    {
        public object Deger { get; set; }
        public int Islem { get; set; } = 1;

        public AjaxResult()
        {
            IslemMesaj = new IslemMesaj();
            EkmekKirintisi = new List<SelectListItem>();
            Baslik = "";
            Deger = "";
        }
    }
}