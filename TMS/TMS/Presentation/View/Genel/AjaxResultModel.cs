﻿namespace TMS.View.Genel
{
    public class AjaxResultModel
    {
        public bool success { get; set; }
        public string type { get; set; }
        public string message { get; set; }
        public string returnUrl { get; set; }
        public object result { get; set; }
        public string paramether1 { get; set; }
        public string paramether2 { get; set; }

        public string targetid { get; set; }
    }
}