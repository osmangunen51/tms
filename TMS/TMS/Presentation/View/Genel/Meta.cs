﻿namespace TMS.View.Genel
{
    public class Meta
    {
        public string Author { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }

        public Meta()
        {
            Author = "";
            Keywords = "";
            Description = "";
        }
    }
}