﻿namespace TMS.View.Genel
{
    public enum IslemMesajTip
    {
        Basarili = 1,
        Hata = 2,
        Uyari = 3,
        Bilgi = 4
    }

    public class IslemMesaj
    {
        public bool Visible { get; set; } = false;
        public string Baslik { get; set; } = "İşlemler";
        public string Mesaj { get; set; } = "Başarılı";

        public int Zaman { get; set; }

        public IslemMesajTip Tip { get; set; }

        public IslemMesaj()
        {
            Visible = false;
            Baslik = "";
            Mesaj = "";
            Tip = IslemMesajTip.Basarili;
            Zaman = 5000;
        }
    }
}