﻿namespace TMS.View.Genel
{
    public class IlIlceSecim
    {
        public string HedefKontrolIlId { get; set; }
        public string HedefKontrolIlceId { get; set; }
        public int IlDeger { get; set; }
        public int IlceDeger { get; set; }
        public string IlBaslik { get; set; }
        public string IlceBaslik { get; set; }
        public string LabelClass { get; set; }
        public string ControlClass { get; set; }

        public IlIlceSecim()
        {
            HedefKontrolIlId = "";
            HedefKontrolIlceId = "";
            IlDeger = -1;
            IlceDeger = -1;
            IlBaslik = "";
            IlceBaslik = "";
            LabelClass = "";
            ControlClass = "";
        }
    }
}