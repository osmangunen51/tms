﻿using System.Collections.Generic;

namespace TMS.View.Genel
{
    public class VeriList
    {
        public System.Data.DataTable Data { get; set; }
        public string Baslik { get; set; }
    }

    public class Liste
    {
        public string Title { get; set; }
        public string GirisUrl { get; set; }
        public string ReportPath { get; set; }
        public string ReportTitle { get; set; }
        public string Name { get; set; }
        public List<VeriList> DataListesi { get; set; }
        public List<RaporParameter> ParametreListesi { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Layout { get; set; }

        public FiltreKonum FiltreKonum { get; set; } = FiltreKonum.Sol;

        public Liste()
        {
            Title = "";
            Layout = "";
            GirisUrl = "~/";
            ReportPath = "";
            ReportTitle = "Liste Baslık";
            ParametreListesi = new List<RaporParameter>();
            Name = "Liste Adı";
            Controller = "";
            Action = "";
            DataListesi = new List<VeriList>();
        }
    }

    public class Rapor
    {
        public string Title { get; set; }
        public string GirisUrl { get; set; }
        public string ReportInstanceName { get; set; }
        public string ReportTitle { get; set; }
        public string Name { get; set; }
        public List<VeriList> DataListesi { get; set; }
        public List<RaporParameter> ParametreListesi { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Layout { get; set; }

        public FiltreKonum FiltreKonum { get; set; } = FiltreKonum.Sol;

        public Rapor()
        {
            Title = "";
            Layout = "";
            GirisUrl = "~/";
            ReportInstanceName = "";
            ReportTitle = "Rapor Baslık";
            ParametreListesi = new List<RaporParameter>();
            Name = "Rapor Adı";
            Controller = "";
            Action = "";
            DataListesi = new List<VeriList>();
        }
    }
}