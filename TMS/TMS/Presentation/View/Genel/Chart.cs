﻿using System.Collections.Generic;

namespace TMS.View.Genel
{
    public class ChartSeri
    {
        public string Baslik { get; set; } = "Seri Başlık";
        public List<string> Liste { get; set; } = new List<string>();
        public string Renk { get; set; } = "";
    }

    public class Chart
    {
        public string Baslik { get; set; } = "Grafik Başlık";
        public string Aciklama { get; set; } = "Grafik Açıklama";
        public List<ChartSeri> SeriListesi { get; set; } = new List<ChartSeri>();
    }

    public class ChartPasta
    {
        public string Baslik { get; set; } = "Grafik Başlık";
        public string Aciklama { get; set; } = "Grafik Açıklama";
        public Dictionary<string, string> SeriListesi { get; set; } = new Dictionary<string, string>();
    }

    public class MorisChart
    {
        public string Baslik { get; set; } = "Grafik Başlık";
        public string XEkseni { get; set; } = "Yatay Eksen";
        public List<ChartSeri> SeriListesi { get; set; } = new List<ChartSeri>();
    }
}