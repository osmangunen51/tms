﻿namespace TMS.View.Genel
{


    public enum PanoTur
    {
        Genel = 0
    }

    public enum UserOneriTip
    {
        Genel_Süreç = 1,
        Kritik_Günler = 2,
        Yaklaşan_Önemli_Günler = 3
    }

    public enum BilgilendirmeTip
    {
        Sms = 1,
        Mail = 2
    }

   
    public enum Durum
    {
        Pasif = 0,
        Aktif = 1
    }

   

    public enum ErisimDurum
    {
        Kapalı = 0,
        Açık = 1
    }

    

    public enum OnayDurum
    {
        Hayir = 0,
        Evet = 1
    }

    public enum TokenOnayDurum
    {
        OnayAsamasinda = -1,
        Hayir = 0,
        Evet = 1
    }

   
    public enum LogTip
    {
        Sistem = 0,
        Giris = 1,
        Cikis = 2,
        SifreDegistirme = 3,
        ProfilDuzenleme = 4,
        SifreOgrenme = 5,
        SifreOgrenmeHata = 6,
        BrowserBilgi = 7,
        TokenOlusturma = 8,
        TokenDuzenlemeIcinAcilma = 9,
        TokenGuncelleme = 10,
        TokenSilme = 11
    }

    public enum FiltreKonum
    {
        Sol = 1,
        Sag = 2,
        Ust = 3,
        Alt = 4
    }

    public enum RaporParameterTip
    {
        Yok = 0,
        Liste = 1,
        Text = 2,
        Tarih = 3,
        SecimListe = 4
    }

    public enum BildirimTip
    {
        Yok = 0,
        Onay = 1,
        Red = 2,
        Sistem = 3,
        OgretimElemani = 4,
        Ogretmen = 5,
        OgretmenAdayi = 6,
        Alan_Uzmani = 7
    }
}