﻿using System.Collections.Generic;

namespace TMS.View.Genel
{
    public class UserBildirimListItem : BaseView
    {
        public Core.Data.UserBildirim UserBildirim { get; set; }
        public Core.Data.Users Users { get; set; }
        public int Tip { get; set; }

        public UserBildirimListItem()
        {
            UserBildirim = new Core.Data.UserBildirim();
            Users = new Core.Data.Users();
            Tip = 0;
        }
    }

    public class UserBildirim : GenelListView
    {
        public List<UserBildirimListItem> UserBildirimListesi { get; set; } = new List<UserBildirimListItem>();

        public Core.Data.UserBildirim AktifKayit { get; set; } = new Core.Data.UserBildirim();

        public int Islem { get; set; } = 1;

        public UserBildirim()
        {
        }
    }
}