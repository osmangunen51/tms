﻿namespace TMS.View.Genel
{
    public class SiparisListItem
    {
        public TMS.Core.Data.Siparis Siparis { get; set; } = new Core.Data.Siparis();
        public TMS.Core.Data.Token KaynakToken { get; set; } = new Core.Data.Token();
        public TMS.Core.Data.Users User { get; set; } = new Core.Data.Users();

        public SiparisListItem()
        {

        }
    }
}