﻿using System.Collections.Generic;

namespace TMS.View.Genel
{
    public class UserOneriListItem : BaseView
    {
        public Core.Data.UserOneri UserOneri { get; set; }
        public Core.Data.Users Users { get; set; }
        public UserOneriTip Tip { get; set; } = UserOneriTip.Genel_Süreç;

        public UserOneriListItem()
        {
            UserOneri = new Core.Data.UserOneri();
            Users = new Core.Data.Users();
        }
    }

    public class UserOneri : GenelListView
    {
        public List<UserOneriListItem> UserOneriListesi { get; set; } = new List<UserOneriListItem>();

        public Core.Data.UserOneri AktifKayit { get; set; } = new Core.Data.UserOneri();

        public int Islem { get; set; } = 1;

        public UserOneri()
        {
        }
    }
}