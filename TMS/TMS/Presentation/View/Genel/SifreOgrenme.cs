﻿using System.ComponentModel.DataAnnotations;

namespace TMS.View.Genel
{
    /// <summary>
    /// Defines the <see cref="SifreOgrenme" />
    /// </summary>
    public class SifreOgrenme : BaseView
    {
        /// <summary>
        /// Gets or sets the Ad
        /// </summary>
        [Required]
        [Display(Name = "Adınız")]
        public string Ad { get; set; }

        /// <summary>
        /// Gets or sets the Soyad
        /// </summary>
        [Required]
        [Display(Name = "Soyadınız")]
        public string Soyad { get; set; }

        /// <summary>
        /// Gets or sets the Telefon
        /// </summary>
        [Display(Name = "Cep Telefonu")]
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string Telefon { get; set; }

        /// <summary>
        /// Gets or sets the Email
        /// </summary>
        [Display(Name = "Emailiniz")]
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SifreOgrenme"/> class.
        /// </summary>
        public SifreOgrenme()
        {
            Ad = "";
            Soyad = "";
        }
    }
}