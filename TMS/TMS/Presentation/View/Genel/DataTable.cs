﻿using System;
using System.Collections.Generic;

namespace TMS.View.Genel
{
    public class DataTable
    {
        public string TableName { get; set; } = Guid.NewGuid().ToString();
        public Ajax Ajax { get; set; } = new Ajax();
        public List<Column> SutunListesi { get; set; } = new List<Column>();
        public bool ServerSide { get; set; } = true;
        public bool Processing { get; set; } = true;
        public string LinkBaseUrl { get; set; } = "";
        public int SortSutunIndex { get; set; } = 0;
        public string SortDirection { get; set; } = "asc";
    }

    public class Ajax
    {
        public string Url { get; set; } = "";
        public string Type { get; set; } = "POST";
        public string DataType { get; set; } = "json";
    }

    public class Column
    {
        public string HeaderText { get; set; } = "Sutun";
        public string Data { get; set; } = "Data";
        public string Name { get; set; } = "Name";
        public int Width { get; set; } = 200;
        public bool OtomatikGenislik { get; set; } = true;
        public bool Visible { get; set; } = true;
    }
}