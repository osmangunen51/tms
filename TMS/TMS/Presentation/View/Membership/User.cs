﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TMS.View.Membership
{
    public class User
    {
        public int UserId { get; set; }

        [Required]
        public String Username { get; set; }

        [Required]
        public String Email { get; set; }

        [Required]
        public String Password { get; set; }

        public String FirstName { get; set; }
        public String LastName { get; set; }

        public Boolean IsActive { get; set; }
        public DateTime CreateDate { get; set; }

        [MaxLength(500)]
        public String Image { get; set; }

        [MaxLength(25)]
        public String Telefon { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
    }
}