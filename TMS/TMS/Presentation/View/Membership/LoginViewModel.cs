﻿using System.ComponentModel.DataAnnotations;

namespace TMS.View.Membership
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Kullanıcı Adı")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Şifre")]
        public string Password { get; set; }

        [Display(Name = "Hatırla ?")]
        public bool RememberMe { get; set; }
    }
}