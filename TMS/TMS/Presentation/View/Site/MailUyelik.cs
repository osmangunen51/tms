﻿using TMS.View.Genel;

namespace TMS.View.Site
{
    public class MailUyelik : BaseView
    {
        public Core.Data.Users Users { get; set; } = new Core.Data.Users();
        public string Link { get; set; } = "";

        public MailUyelik()
        {
        }
    }
}