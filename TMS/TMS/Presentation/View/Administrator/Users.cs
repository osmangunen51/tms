﻿using System.Collections.Generic;

namespace TMS.View.Administrator
{
    public class Users : GenelListView
    {
        public List<Core.Data.Users> UsersListesi { get; set; } = new List<Core.Data.Users>();
        public Core.Data.Users AktifKayit { get; set; } = new Core.Data.Users();
        public int[] RolNoListesi { get; set; } = new int[] { };

        public List<Core.Data.Roles> RolListesi { get; set; } = new List<Core.Data.Roles>();

        public Users()
        {
        }
    }
}