﻿using System.Collections.Generic;

namespace TMS.View.Administrator
{
    public class Anasayfa : GenelListView
    {
        public int SurucuSayisi { get; set; } = 0;
        public int AracSayisi { get; set; } = 0;
        public int MusteriSayisi { get; set; } = 0;
        public Core.Data.Users User { get; set; } = new Core.Data.Users();
        public List<TMS.View.Genel.UserBildirimListItem> UserBildirimListesi { get; set; } = new List<Genel.UserBildirimListItem>();
        public List<Core.Data.Logs> UserLogListesi { get; set; } = new List<Core.Data.Logs>();
    }
}