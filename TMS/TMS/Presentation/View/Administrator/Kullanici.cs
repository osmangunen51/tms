﻿using System.Collections.Generic;

namespace TMS.View.Administrator
{
    public class Kullanici : GenelListView
    {
        public TMS.Core.Data.Kullanici AktifKayit { get; set; } = new Core.Data.Kullanici();
        public List<Genel.KullaniciListItem> KullaniciListesi { get; set; } = new List<Genel.KullaniciListItem>();

        public Kullanici()
        {
        }
    }
}