﻿using System.Collections.Generic;

namespace TMS.View.Administrator
{
    public class GenelAyar : BaseView
    {
        public List<Core.Data.GenelAyar> AyarListesi { get; set; } = new List<Core.Data.GenelAyar>();
    }
}