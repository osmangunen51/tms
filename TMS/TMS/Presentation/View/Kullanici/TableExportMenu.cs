﻿namespace TMS.View.Kullanici
{
    public class TableExportMenu
    {
        public string TableName { get; set; }
        public bool BtnJson { get; set; }
        public bool BtnXml { get; set; }
        public bool BtnSql { get; set; }
        public bool BtnCsv { get; set; }
        public bool BtnTxt { get; set; }

        public bool BtnXls { get; set; }

        public bool BtnWord { get; set; }

        public bool BtnPpt { get; set; }

        public bool BtnPdf { get; set; }

        public TableExportMenu()
        {
            BtnJson = true;
            BtnXml = true;
            BtnSql = true;
            BtnCsv = true;
            BtnTxt = true;
            BtnXls = true;
            BtnWord = true;
            BtnPpt = true;
            BtnPdf = true;
            TableName = "";
        }
    }
}