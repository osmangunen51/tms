﻿using System.Collections.Generic;

namespace TMS.View.Kullanici
{
    public class GenelAyar : BaseView
    {
        public List<Core.Data.GenelAyar> AyarListesi { get; set; } = new List<Core.Data.GenelAyar>();
    }
}