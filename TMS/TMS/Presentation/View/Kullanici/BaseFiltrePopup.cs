﻿namespace TMS.View.Kullanici
{
    public class BaseFiltrePopup
    {
        public string HedefKontrolId { get; set; }
        public string Deger { get; set; }

        public string Baslik { get; set; }

        public string AltBaslik { get; set; }

        public bool DugumDurum { get; set; }

        public string ButtonHtml { get; set; }

        public bool ButtonDurum { get; set; }

        public int AvmNo { get; set; }

        public BaseFiltrePopup()
        {
            HedefKontrolId = "";
            Deger = "";
            Deger = "";

            DugumDurum = false;

            ButtonDurum = true;
            AvmNo = -1;
        }
    }
}