﻿using TMS.View.Genel;

namespace TMS.View.Kullanici
{
    public class BaseView
    {
        public IslemMesaj IslemMesaj { get; set; } = new IslemMesaj();

        public BaseView()
        {
        }
    }
}