﻿using System.Collections.Generic;

namespace TMS.View.Kullanici
{
    public class Anasayfa : GenelListView
    {
        public Core.Data.Users User { get; set; } = new Core.Data.Users();
        public List<TMS.View.Genel.UserBildirimListItem> UserBildirimListesi { get; set; } = new List<Genel.UserBildirimListItem>();
        public List<TMS.View.Genel.UserOneriListItem> UserOneriListesi { get; set; } = new List<Genel.UserOneriListItem>();
        public List<Core.Data.Logs> UserLogListesi { get; set; } = new List<Core.Data.Logs>();

        public int TokenSayisi { get; set; } = 0;
        public int SiparisSayisi { get; set; } = 0;
    }
}