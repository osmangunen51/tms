﻿namespace TMS.View.Kullanici
{
    public class Profil : GenelListView
    {
        public TMS.Core.Data.Kullanici AktifKayit { get; set; }

        public Profil()
        {
            AktifKayit = new TMS.Core.Data.Kullanici();
        }
    }
}