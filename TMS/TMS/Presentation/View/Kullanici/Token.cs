﻿using System.Collections.Generic;

namespace TMS.View.Kullanici
{
    public class Token : GenelListView
    {
        public TMS.Core.Data.Token AktifKayit { get; set; } = new Core.Data.Token();
        public List<Genel.TokenListItem> TokenListesi { get; set; } = new List<Genel.TokenListItem>();

        public Token()
        {
        }
    }
}