﻿using System.Collections.Generic;

namespace TMS.View.Kullanici
{
    public class Siparis : GenelListView
    {
        public TMS.Core.Data.Siparis AktifKayit { get; set; } = new Core.Data.Siparis();
        public List<Genel.TokenListItem> KaynakTokenListesi { get; set; } = new List<Genel.TokenListItem>();
        public List<Genel.SiparisListItem> SatisListesi { get; set; } = new List<Genel.SiparisListItem>();

        public Siparis()
        {
        }
    }
}