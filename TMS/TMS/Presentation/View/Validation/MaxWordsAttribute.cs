﻿using System.ComponentModel.DataAnnotations;

namespace TMS.View.Validation
{
    public class MaxWordsAttribute : ValidationAttribute
    {
        public MaxWordsAttribute(int maxWords) : base("{0} alanı çok fazla karakter içeriyor.")
        {
            _maxWords = maxWords;
        }

        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                var deger = value.ToString();
                if (deger.Split(' ').Length > _maxWords)
                {
                    var errorMessage = FormatErrorMessage(
                        validationContext.DisplayName);
                    return new ValidationResult(errorMessage);
                }
            }
            return ValidationResult.Success;
        }

        private readonly int _maxWords;
    }
}