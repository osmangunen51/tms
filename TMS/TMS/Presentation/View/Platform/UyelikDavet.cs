﻿using System.ComponentModel.DataAnnotations;

namespace TMS.View.Platform
{
    public class UyelikDavet : BaseView
    {
        public string PencereAd { get; set; } = "";

        [Display(Name = "Eposta")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [DataType(DataType.EmailAddress, ErrorMessage = "{0} uygun formatta değil")]
        public string Eposta { get; set; } = "";

        public UyelikDavet()
        {
        }
    }
}