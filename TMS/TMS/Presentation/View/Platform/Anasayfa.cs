﻿using System.Collections.Generic;

namespace TMS.View.Platform
{
    public class Anasayfa : GenelListView
    {
        public int GrupSayisi { get; set; } = 0;
        public int GorevSayisi { get; set; } = 0;
        public int KullaniciSayisi { get; set; } = 0;
        public Core.Data.Users User { get; set; } = new Core.Data.Users();
        public List<TMS.View.Genel.UserBildirimListItem> UserBildirimListesi { get; set; } = new List<Genel.UserBildirimListItem>();
        public List<TMS.View.Genel.UserOneriListItem> UserOneriListesi { get; set; } = new List<Genel.UserOneriListItem>();
        public List<Core.Data.Logs> UserLogListesi { get; set; } = new List<Core.Data.Logs>();
    }
}