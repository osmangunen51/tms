﻿using System.ComponentModel.DataAnnotations;

namespace TMS.View.Platform
{
    public class Uyelik : GenelListView
    {
        public string PencereAd { get; set; } = "";


        [Display(Name = "Adınız")]
        [StringLength(maximumLength: 255, MinimumLength = 3, ErrorMessage = "{0} En Az 3-255 Karakter Arasında Olmalıdır.")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        public string Ad { get; set; } = "";

        [Display(Name = "Soyadınız")]
        [StringLength(maximumLength: 255, MinimumLength = 3, ErrorMessage = "{0} En Az 3-255 Karakter Arasında Olmalıdır.")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        public string Soyad { get; set; } = "";

        [Display(Name = "Cinsiyet")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        public int Cinsiyet { get; set; } = -1;

        [Display(Name = "Telefon")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "{0} uygun formatta değil")]
        public string Telefon { get; set; } = "";

        [Display(Name = "Eposta")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [DataType(DataType.EmailAddress, ErrorMessage = "{0} uygun formatta değil")]
        public string Eposta { get; set; } = "";

        [Display(Name = "Üniversite")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        public int Universite { get; set; } = -1;

        [Display(Name = "Fakülte")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        public int Fakulte { get; set; } = -1;

        [Display(Name = "Bölüm")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        public int Bolum { get; set; } = -1;

        [Display(Name = "Şifre")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [DataType(DataType.Password, ErrorMessage = "{0} uygun formatta değil")]
        public string Sifre { get; set; } = "";

        [Display(Name = "Şifre Onay")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        [Compare("Sifre", ErrorMessage = "şifreler eşleşmiyor.")]
        [DataType(DataType.Password, ErrorMessage = "{0} uygun formatta değil")]
        public string SifreOnay { get; set; } = "";

        [Display(Name = "İl")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        public int Il { get; set; } = -1;

        [Display(Name = "Okul")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        public string Okul { get; set; } = "";

        [Display(Name = "Branş")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        public int Brans { get; set; } = -1;

        [Display(Name = "Şifre Onay")]
        [Required(ErrorMessage = "{0} Gerekli Alandır.")]
        public bool SozlesmeOnay { get; set; } = false;

        public Uyelik()
        {
        }
    }
}