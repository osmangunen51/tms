﻿using System.Web.Optimization;

namespace TMS.Panel.Web.Areas.Platform
{
    internal static class BundleConfig
    {
        internal static void RegisterBundles(BundleCollection bundles)
        {
            #region Css Bundle

            bundles.Add(new StyleBundle("~/Platform.Vendor/css").
                        Include
                        (
                            "~/Areas/Platform/Theme/Default/vendor/bootstrap/css/bootstrap.css",
                            "~/Areas/Platform/Theme/Default/vendor/font-awesome/css/font-awesome.css",
                            "~/Areas/Platform/Theme/Default/vendor/font-awesome5/css/all.css",
                            "~/Areas/Platform/Theme/Default/vendor/magnific-popup/magnific-popup.css",
                            "~/Areas/Platform/Theme/Default/vendor/bootstrap-datepicker/css/datepicker3.css",
                            "~/Areas/Platform/Theme/Default/vendor/sweetalert/sweetalert.css",
                            "~/Areas/Platform/Theme/Default/vendor/jstree/themes/default/style.css"
                        )
                );
            bundles.Add(new StyleBundle("~/Platform.CustomVendor/css").
                        Include
                        (
                            "~/Areas/Platform/Theme/Default/vendor/select2/select2.css",
                            "~/Areas/Platform/Theme/Default/vendor/bootstrap-multiselect/bootstrap-multiselect.css",
                            "~/Areas/Platform/Theme/Default/vendor/jquery-datatables-bs3/assets/css/datatables.css",
                            "~/Areas/Platform/Theme/Default/vendor/morris/morris.css",
                            "~/Areas/Platform/Theme/Default/vendor/summernote/summernote.css",
                            "~/Areas/Platform/Theme/Default/vendor/summernote/summernote-bs4.css",
                            "~/Areas/Platform/Theme/Default/vendor/hover-css/hover.css",
                            "~/Areas/Platform/Theme/Default/vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css",
                            "~/Areas/Platform/Theme/Default/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css"
                        )
                );
            bundles.Add(new StyleBundle("~/Platform.Theme/css").
                        Include
                        (
                            "~/Areas/Platform/Theme/Default/css/theme.css"
                        )
            );
            bundles.Add(new StyleBundle("~/Platform.CustomTheme/css").
                                    Include
                                    (
                                        "~/Areas/Platform/Theme/Default/css/theme-custom.css",
                                        "~/Areas/Platform/Theme/Default/css/theme-custom.css"
                                    )
                            );

            #endregion Css Bundle

            #region Javascript Bundle

            bundles.Add(
                            new ScriptBundle("~/Platform.modernizr/js").
                                Include(
                                        "~/Areas/Platform/Theme/Default/vendor/modernizr/modernizr.js"
                                        )
                        );

            bundles.Add(
                           new ScriptBundle("~/Platform.jquery/js").
                               Include(
                               "~/Areas/Platform/Theme/Default/vendor/jquery/v1.12.0/jquery.js",
                               "~/Areas/Platform/Theme/Default/vendor/jquery-ui/js/v1.11.4/jquery-ui.js",
                               "~/Areas/Platform/Theme/Default/vendor/jquery-browser-mobile/jquery.browser.mobile.js",
                               "~/Areas/Platform/Theme/Default/vendor/jquery-cookie/jquery.cookie.js"
                               ));

            bundles.Add(
                new ScriptBundle("~/Platform.Vendor/js").
                    Include(
                    "~/Areas/Platform/Theme/Default/vendor/jquery-ui/v1.10.4/js/jquery-ui-1.10.4.js",
                    //"~/Areas/Platform/Theme/Default/vendor/style-switcher/style.switcher.js",
                    "~/Areas/Platform/Theme/Default/vendor/bootstrap/js/bootstrap.js",
                    "~/Areas/Platform/Theme/Default/vendor/nanoscroller/nanoscroller.js",
                    "~/Areas/Platform/Theme/Default/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js",
                    "~/Areas/Platform/Theme/Default/vendor/magnific-popup/magnific-popup.js",
                    "~/Areas/Platform/Theme/Default/vendor/jquery-datatables/media/js/jquery.dataTables.js",
                    "~/Areas/Platform/Theme/Default/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js",
                    "~/Areas/Platform/Theme/Default/vendor/jquery-datatables-bs3/assets/js/datatables.js",
                    "~/Areas/Platform/Theme/Default/vendor/jquery-placeholder/jquery.placeholder.js",
                    "~/Areas/Platform/Theme/Default/vendor/summernote/summernote.js",
                    "~/Areas/Platform/Theme/Default/vendor/TableExport/tableExport.js",
                    "~/Areas/Platform/Theme/Default/vendor/TableExport/jquery.base64.js",
                    "~/Areas/Platform/Theme/Default/vendor/TableExport/html2canvas.js",
                    "~/Areas/Platform/Theme/Default/vendor/TableExport/jspdf/libs/sprintf.js",
                    "~/Areas/Platform/Theme/Default/vendor/TableExport/jspdf/jspdf.js",
                    "~/Areas/Platform/Theme/Default/vendor/TableExport/jspdf/libs/base64.js",
                    "~/Areas/Platform/Theme/Default/vendor/rowsorter/rowsorter.js",
                    "~/Areas/Platform/Theme/Default/vendor/sweetalert/sweetalert.js",
                    "~/Areas/Platform/Theme/Default/vendor/jquery-countdown/jquery.countdown.js",
                    "~/Areas/Platform/Theme/Default/vendor/jstree/jstree.js",
                    "~/Areas/Platform/Theme/Default/vendor/clipboard/clipboard.min.js",
                    "~/Areas/Platform/Theme/Default/vendor/store-js/store.js",
                    "~/Areas/Platform/Theme/Default/vendor/fullclip.js"

                    )
            );
            bundles.Add(
                new ScriptBundle("~/Platform.CustomVendor/js").
                    Include(
                                "~/Areas/Platform/Theme/Default/vendor/jquery-ui/v1.10.4/js/jquery-ui-1.10.4.js",
                                "~/Areas/Platform/Theme/Default/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js",
                                "~/Areas/Platform/Theme/Default/vendor/jquery-appear/jquery.appear.js",
                                "~/Areas/Platform/Theme/Default/vendor/select2/select2.js",
                                "~/Areas/Platform/Theme/Default/vendor/bootstrap-multiselect/bootstrap-multiselect.js",
                                "~/Areas/Platform/Theme/Default/vendor/jquery-easypiechart/jquery.easypiechart.js",
                                "~/Areas/Platform/Theme/Default/vendor/flot/jquery.flot.js",
                                "~/Areas/Platform/Theme/Default/vendor/flot-tooltip/jquery.flot.tooltip.js",
                                "~/Areas/Platform/Theme/Default/vendor/flot/jquery.flot.pie.js",
                                "~/Areas/Platform/Theme/Default/vendor/flot/jquery.flot.categories.js",
                                "~/Areas/Platform/Theme/Default/vendor/flot/jquery.flot.resize.js",
                                "~/Areas/Platform/Theme/Default/vendor/jquery-sparkline/jquery.sparkline.js",
                                "~/Areas/Platform/Theme/Default/vendor/raphael/raphael.js",
                                "~/Areas/Platform/Theme/Default/vendor/morris/morris.js",
                                "~/Areas/Platform/Theme/Default/vendor/gauge/gauge.js",
                                "~/Areas/Platform/Theme/Default/vendor/snap-svg/snap.svg.js",
                                "~/Areas/Platform/Theme/Default/vendor/liquid-meter/liquid.meter.js",
                                "~/Areas/Platform/Theme/Default/vendor/jqvmap/jquery.vmap.js",
                                "~/Areas/Platform/Theme/Default/vendor/jqvmap/data/jquery.vmap.sampledata.js",
                                "~/Areas/Platform/Theme/Default/vendor/jqvmap/maps/jquery.vmap.world.js",
                                "~/Areas/Platform/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.africa.js",
                                "~/Areas/Platform/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.asia.js",
                                "~/Areas/Platform/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.australia.js",
                                "~/Areas/Platform/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.europe.js",
                                "~/Areas/Platform/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js",
                                "~/Areas/Platform/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js",
                                "~/Areas/Platform/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js",
                                "~/Areas/Platform/Theme/Default/vendor/bootstrap-datetimepicker/js/moment.min.js",
                                "~/Areas/Platform/Theme/Default/vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js",
                                "~/Areas/Platform/Theme/Default/vendor/ios7-switch/ios7-switch.js",
                            "~/Areas/Platform/Theme/Default/js/ui-elements/examples.modals.js",
                            "~/Areas/Platform/Theme/Default/vendor/bootstrap-filestyle-1.3.0/bootstrap-filestyle.js"
                            )
            );
            bundles.Add(
                new ScriptBundle("~/Platform.theme/js").
                    Include(
                            "~/Areas/Platform/Theme/Default/js/theme.js"
                            )
            );
            bundles.Add(
               new ScriptBundle("~/Platform.customtheme/js").
                   Include(
                           "~/Areas/Platform/Theme/Default/js/theme.custom.js"
                           )
           );
            bundles.Add(
               new ScriptBundle("~/Platform.theme_init/js").
                   Include(
                           "~/Areas/Platform/Theme/Default/js/theme.init.js"
                           )
            );
            bundles.Add(
                new ScriptBundle("~/Platform.SpecificPage/js").
                    Include(

                            )
            );

            bundles.Add(
                new ScriptBundle("~/Platform.GoogleMap/js", "https://maps.googleapis.com/maps/api/js?key=AIzaSyDDFkrvHfVe77pUg0E-xpUc3zrXk7J-oks").
                    Include(
                    "~/Areas/Platform/Theme/Default/js/gmaps.js"
                    )
            );

            #endregion Javascript Bundle

            bundles.Add(new ScriptBundle("~/Platform.elfinder/js").Include(
                 //"~/Areas/Platform/Theme/Default/vendor/elfinder/jquery-1.9.1.js",
                 //"~/Areas/Platform/Theme/Default/vendor/elfinder/jquery-ui-1.10.2.js",
                 "~/Areas/Platform/Theme/Default/vendor/elfinder/js/elfinder.full.js"
                 ));
            bundles.Add(new StyleBundle("~/Platform.elfinder/css").Include(
                            "~/Areas/Platform/Theme/Default/vendor/elfinder/css/elfinder.full.css",
                            "~/Areas/Platform/Theme/Default/vendor/elfinder/css/theme.css"));
            //bundles.Add(
            //   new ScriptBundle("~/Platform.jquery.migrate/js").
            //       Include(
            //               "~/Areas/Platform/Theme/Default/vendor/jquery/jquery-migrate-1.2.1.js"
            //               )
            //);
            BundleTable.EnableOptimizations = false;
        }
    }
}