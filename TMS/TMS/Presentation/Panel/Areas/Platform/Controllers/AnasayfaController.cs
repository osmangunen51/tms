﻿using System.Linq;
using System.Web.Mvc;
using TMS.Core.Service;
using TMS.Panel.Web.Code;

namespace TMS.Panel.Web.Areas.Platform.Controllers
{
    public class AnasayfaController : PlatformBaseController
    {
        private UsersService UsersService = new UsersService();

        private UserBildirimService UserBildirimService = new UserBildirimService();
        private UserOneriService UserOneriService = new UserOneriService();
        // GET: Anasayfa/Anasayfa
        [Compress]
        public ActionResult Index()
        {

            SifreDegistirmeLayoutAyarla("~/Areas/Platform/Views/Shared/_Layout.cshtml");
            TMS.View.Platform.Anasayfa Model = new TMS.View.Platform.Anasayfa();
            Model.User = UsersService.Get(x => x.UserId == User.UserId);
            // Model.GrupSayisi = GrupService.GetMany(x=>x.UserNo==this.User.UserId).Count();
            //Model.KullaniciSayisi = KullaniciService.GetMany(x => x.UserNo == this.User.UserId).Count();
            // Model.GorevSayisi = GorevService.GetMany(x => x.UserNo == this.User.UserId).Count();
            Model.UserBildirimListesi = UserBildirimService.UserGelenBildirimListesiGetir(User.UserId);
            Model.UserOneriListesi = UserOneriService.UserGelenOneriListesiGetir(User.UserId);
            Model.UserLogListesi = LogService.GetMany(x => x.UserNo == User.UserId).OrderByDescending(x => x.Date).Take(20).ToList();
            return View(Model);
        }

        [Compress]
        public ActionResult YapimAsamasinda()
        {
            return View();
        }
    }
}