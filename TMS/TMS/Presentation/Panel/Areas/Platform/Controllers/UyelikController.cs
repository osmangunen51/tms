﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TMS.Panel.Web.Code.Security;

namespace TMS.Panel.Web.Areas.Platform.Controllers
{
    public class UyelikController : BaseController
    {
        TMS.Core.Service.UsersService UsersService = new Core.Service.UsersService();
        TMS.Core.Service.KullaniciService KullaniciService = new Core.Service.KullaniciService();
        TMS.Core.Service.RolesService RolesService = new Core.Service.RolesService();
        TMS.Core.Service.UserRolesService UserRolesService = new Core.Service.UserRolesService();
        TMS.Core.Service.GenelAyarService GenelAyarService = new Core.Service.GenelAyarService();


        // GET: Platform/Uyelik



        public ActionResult SifremiUnuttum()
        {
            TMS.View.Platform.SifremiUnuttum Model = new View.Platform.SifremiUnuttum()
            {
                PencereAd = "Üyeliğimi Şifremi Unuttum"
            };
            return View(Model);
        }


        [HttpPost]
        public ActionResult SifremiUnuttum(TMS.View.Platform.SifremiUnuttum Model)
        {
            if (ModelState.IsValid)
            {
                var User = UsersService.Get(x => x.Email == Model.Eposta);
                if (User != null)
                {
                    TMS.View.Genel.IslemDurum MailGonderimIslemi = SifremiUnuttumMailiGonder(User);
                    if (MailGonderimIslemi.Durum)
                    {

                        Model.IslemMesaj.Baslik = "Üyelik İşlemleri";
                        Model.IslemMesaj.Mesaj = "Başarıyla Tamamlandı";
                        Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Basarili;
                        Model.IslemMesaj.Visible = true;
                    }
                    else

                    {
                        Model.IslemMesaj.Baslik = "Üyelik İşlemleri";
                        Model.IslemMesaj.Mesaj = "Hata Oluştu Lütfen Tekrar Deneyiniz.";
                        Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Hata;
                        Model.IslemMesaj.Visible = true;
                    }
                }
                else
                {
                    Model.IslemMesaj.Baslik = "Üyelik İşlemleri";
                    Model.IslemMesaj.Mesaj = "Hesap Bulunamadı. Belki de Üye Olabilirsiniz.";
                    Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Hata;
                    Model.IslemMesaj.Visible = true;
                }
            }
            return View(Model);
        }

        public ActionResult Davet()
        {
            TMS.View.Platform.UyelikDavet Model = new View.Platform.UyelikDavet()
            {
                PencereAd = "Üyeliğe Davet Et"
            };
            return View(Model);
        }


        [HttpPost]
        public ActionResult Davet(TMS.View.Platform.UyelikDavet Model)
        {
            if (ModelState.IsValid)
            {
                TMS.View.Genel.IslemDurum MailGonderimIslemi = DavetMailiGonder(Model.Eposta);
                if (MailGonderimIslemi.Durum)
                {
                    Model.IslemMesaj.Baslik = "Üyelik İşlemleri";
                    Model.IslemMesaj.Mesaj = "Başarıyla Tamamlandı";
                    Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Basarili;
                    Model.IslemMesaj.Visible = true;
                }
                else
                {
                    Model.IslemMesaj.Baslik = "Üyelik İşlemleri";
                    Model.IslemMesaj.Mesaj = "Hata Oluştu Lütfen Tekrar Deneyiniz.";
                    Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Hata;
                    Model.IslemMesaj.Visible = true;
                }
            }
            return View(Model);
        }


        public ActionResult AktifEt()
        {
            TMS.View.Platform.UyelikAktifEt Model = new View.Platform.UyelikAktifEt()
            {
                PencereAd = "Üyeliğimi Aktif Et"
            };
            return View(Model);
        }


        [HttpPost]
        public ActionResult AktifEt(TMS.View.Platform.UyelikAktifEt Model)
        {
            if (ModelState.IsValid)
            {
                var User = UsersService.Get(x => x.Email == Model.Eposta);
                if (User != null)
                {
                    TMS.View.Genel.IslemDurum MailGonderimIslemi = ActivasyonMailiGonder(User);
                    if (MailGonderimIslemi.Durum)
                    {
                        Model.IslemMesaj.Baslik = "Üyelik İşlemleri";
                        Model.IslemMesaj.Mesaj = "Başarıyla Tamamlandı";
                        Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Basarili;
                        Model.IslemMesaj.Visible = true;
                    }
                    else
                    {
                        Model.IslemMesaj.Baslik = "Üyelik İşlemleri";
                        Model.IslemMesaj.Mesaj = "Hata Oluştu Lütfen Tekrar Deneyiniz.";
                        Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Hata;
                        Model.IslemMesaj.Visible = true;
                    }
                }
                else
                {
                    Model.IslemMesaj.Baslik = "Üyelik İşlemleri";
                    Model.IslemMesaj.Mesaj = "Hesap Bulunamadı. Belki de Üye Olabilirsiniz.";
                    Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Hata;
                    Model.IslemMesaj.Visible = true;
                }
            }
            return View(Model);
        }

        public ActionResult Sozlesme()
        {
            string SystemAd = "TMSKullanıciSozlezmesi";
            Core.Data.GenelAyar Model = GenelAyarService.Get(x => x.SystemAd == SystemAd);
            return View(Model);
        }

        public ActionResult Kullanici()
        {
            TMS.View.Platform.Uyelik Model = new View.Platform.Uyelik()
            {
                PencereAd = "Kullanıcı"
            };
            return View("Index", Model);
        }
      

        [HttpPost]
        public ActionResult Index(TMS.View.Platform.Uyelik Model)
        {
            if (ModelState.IsValid)
            {
                if (Model.SozlesmeOnay)
                {
                    var User = UsersService.Get(x => x.Email == Model.Eposta);
                    if (User == null)
                    {
                        using (System.Transactions.TransactionScope Transaction = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, TimeSpan.FromSeconds(30)))
                        {
                            try
                            {
                                Core.Data.Kullanici Kullanici = new Core.Data.Kullanici()
                                {
                                    Ad = Model.Ad,
                                    Soyad = Model.Soyad,
                                    Cinsiyet = (short)Model.Cinsiyet,
                                    Durum = (short)TMS.View.Genel.Durum.Aktif,
                                    Tarih = DateTime.Now,
                                    Mail =Model.Eposta,
                                    IlNo = Model.Il
                                };

                                KullaniciService.Add(Kullanici);
                                KullaniciService.Commit();

                                Core.Data.Users Users = new Core.Data.Users()
                                {
                                    FirstName = Model.Ad,
                                    LastName = Model.Soyad,
                                    Username = Model.Eposta,
                                    Email = Model.Eposta,
                                    IsActive = false,
                                    CreateDate = DateTime.Now,
                                    Image = "",
                                    Ozgecmis = "",
                                    Password = Model.Sifre,
                                    Telefon = Model.Telefon,
                                };
                                UsersService.Add(Users);
                                UsersService.Commit();



                                int RolId = RolesService.Get(x => x.RoleName == "Kullanici").RoleId;
                                bool RolVarmi = UserRolesService.GetMany(x => x.User_UserId == Users.UserId && x.Role_RoleId == RolId) == null;
                                if (!RolVarmi)
                                {
                                    UserRolesService.Add(new Core.Data.UserRoles() { User_UserId = Users.UserId, Role_RoleId = RolId });
                                    UserRolesService.Commit();
                                }

                                Kullanici.UserNo = Users.UserId;
                                KullaniciService.Update(Kullanici);
                                KullaniciService.Commit();

                                TMS.View.Genel.IslemDurum MailGonderimIslemi = ActivasyonMailiGonder(Users);
                                if (MailGonderimIslemi.Durum)
                                {
                                    Model.IslemMesaj.Baslik = "Üyelik İşlemleri";
                                    Model.IslemMesaj.Mesaj = "Başarıyla Tamamlandı";
                                    Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Basarili;
                                    Model.IslemMesaj.Visible = true;
                                    Transaction.Complete();
                                }
                                else
                                {
                                    Model.IslemMesaj.Baslik = "Üyelik İşlemleri";
                                    Model.IslemMesaj.Mesaj = "Hata Oluştu Lütfen Tekrar Deneyiniz.";
                                    Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Hata;
                                    Model.IslemMesaj.Visible = true;
                                }

                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                    else
                    {
                        Model.IslemMesaj.Visible = true;
                        Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Hata;
                        Model.IslemMesaj.Baslik = "Üyelik İşlemleri";
                        Model.IslemMesaj.Mesaj = "Bu mail adresi ile kayıtlı bir üyemiz bulunmaktadır.<br>Lütfen farklı bir mail adresi deneyiniz.";
                    }
                }
                else
                {
                    Model.IslemMesaj.Visible = true;
                    Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Hata;
                    Model.IslemMesaj.Baslik = "Üyelik İşlemleri";
                    Model.IslemMesaj.Mesaj = "Sözleşmeyi onaylamanız gerekmektedir.";
                }
            }
            return View(Model);
        }


        public ActionResult Aktivasyon(string Id)
        {
            TMS.View.Platform.Aktivasyon Model = new View.Platform.Aktivasyon();
            string Key = HttpUtility.UrlDecode(Id);
            if (!string.IsNullOrEmpty(Key))
            {
                string KeyUserNo = "";
                using (Core.Lib.SifreOlusturucu Sifreci = new TMS.Core.Lib.SifreOlusturucu())
                {
                    string Veri = Sifreci.Coz(Key, true);
                    List<string> VeriListesi = Veri.Split('é').ToList();
                    if (VeriListesi.Count > 0)
                    {
                        KeyUserNo = VeriListesi[0].ToString();
                    }
                }
                if (!string.IsNullOrWhiteSpace(KeyUserNo))
                {
                    int UserNo = Convert.ToInt32(KeyUserNo);
                    var user = UsersService.GetById(UserNo);
                    if (user != null)
                    {
                        user.IsActive = true;
                        UsersService.Update(user);
                        UsersService.Commit();
                        CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
                        serializeModel.UserId = user.UserId;
                        serializeModel.FirstName = user.FirstName;
                        serializeModel.LastName = user.LastName;
                        serializeModel.Image = user.Image;
                        List<Core.Data.Roles> RolListesi = UsersService.RolListesiGetir(user.UserId);
                        serializeModel.roles = RolListesi.Select(x => x.RoleName).ToArray();
                        string userData = JsonConvert.SerializeObject(serializeModel);
                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                                 1,
                                 user.Email,
                                 DateTime.Now,
                                 DateTime.Now.AddMinutes(15),
                                 false,
                                 userData);
                        string encTicket = FormsAuthentication.Encrypt(authTicket);
                        HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                        Response.Cookies.Add(faCookie);
                        string Link = "";
                        Link = "~/";
                        if (RolListesi.Select(x => x.RoleName).Contains("Admin"))
                        {
                            Link = "~/Administrator/Anasayfa/Index";
                        }
                        else if (RolListesi.Select(x => x.RoleName).Contains("Kullanici"))
                        {
                            Link = "~/Kullanici/Anasayfa/Index";
                        }
                        else
                        {
                            Link = TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl("~/Giris");
                        }
                        Response.Redirect(Link, true);
                    }
                    else
                    {
                        Response.Redirect(TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl("~/Giris"), true);
                    }
                }
                else
                {
                    Model.IslemMesaj.Baslik = "Üyelik İşlemleri";
                    Model.IslemMesaj.Visible = true;
                    Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Uyari;
                    Model.IslemMesaj.Mesaj = "Üyelik Bulunamadı.";
                }
            }
            else
            {
                Model.IslemMesaj.Baslik = "Üyelik İşlemleri";
                Model.IslemMesaj.Visible = true;
                Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Uyari;
                Model.IslemMesaj.Mesaj = "Üyelik Bulunamadı.";
            }
            return View();
        }
    }
}