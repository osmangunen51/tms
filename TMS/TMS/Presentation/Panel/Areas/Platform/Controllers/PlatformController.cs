﻿using System.Web.Mvc;
using TMS.Panel.Web.Areas.Platform.Code;
using TMS.Panel.Web.Code;

namespace TMS.Panel.Web.Areas.Platform.Controllers
{
    public class PlatformController : PlatformBaseController
    {
        [Compress]
        public ActionResult Index()
        {
            GenelView Model = new GenelView();
            return View("Index", Model);
        }
    }
}