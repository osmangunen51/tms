﻿using TMS.Panel.Web.Code.Security;

namespace TMS.Panel.Web.Areas.Platform.Controllers
{
    [CustomAuthorize(Roles = "Platform")]
    public class PlatformBaseController : BaseController
    {
    }
}