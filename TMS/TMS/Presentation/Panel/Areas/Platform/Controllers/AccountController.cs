﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TMS.Core.Data;
using TMS.Core.Service;
using TMS.Panel.Web.Code.Security;
using TMS.View.Genel;


namespace TMS.Panel.Web.Areas.Platform.Controllers
{
    /// <summary>
    /// Defines the <see cref="AccountController" />
    /// </summary>
    public class AccountController : BaseController
    {
        /// <summary>
        /// Defines the RolesService
        /// </summary>
        private RolesService RolesService = new RolesService();

        /// <summary>
        /// Defines the UsersService
        /// </summary>
        private UsersService UsersService = new UsersService();

        /// <summary>
        /// Defines the AvmService
        /// </summary>


        /// <summary>
        /// Defines the IlService
        /// </summary>
        private IlService IlService = new IlService();
        private KullaniciService KullaniciService = new KullaniciService();

        // GET: /Account/
        /// <summary>
        /// The Index
        /// </summary>
        /// <returns>The <see cref="ActionResult"/></returns>
        public ActionResult Index()
        {
            GirisView Model = new GirisView();
            Model.GirisDurum = false;
            HttpCookie TMSUserName = Request.Cookies["TMSUserName"];
            if (TMSUserName != null)
            {
                Model.LoginView.Username = TMSUserName.Value;
            }
            HttpCookie TMSSifre = Request.Cookies["TMSSifre"];
            if (TMSSifre != null)
            {
                Model.LoginView.Password = TMSSifre.Value;
            }
            Model.IslemMesaj.Visible = false;
            return View(Model);
        }

        /// <summary>
        /// The Index
        /// </summary>
        /// <param name="Model">The <see cref="GirisView"/></param>
        /// <returns>The <see cref="ActionResult"/></returns>
        [HttpPost]
        public ActionResult Index(GirisView Model)
        {
            Model.GirisDurum = false;
            if ((!String.IsNullOrEmpty(Model.LoginView.Username) && (!String.IsNullOrEmpty(Model.LoginView.Password))))
            {
                var user = UsersService.Get(u => u.Username == Model.LoginView.Username && u.Password == Model.LoginView.Password);
                if (user != null)
                {
                    if (user.IsActive)
                    {
                        List<TMS.Core.Data.Roles> RolListesi = UsersService.RolListesiGetir(user.UserId);
                        CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
                        serializeModel.UserId = user.UserId;
                        serializeModel.FirstName = user.FirstName;
                        serializeModel.LastName = user.LastName;
                        serializeModel.roles = RolListesi.Select(x => x.RoleName).ToArray();
                        serializeModel.Image = user.Image;
                        serializeModel.Telefon = user.Telefon;
                        serializeModel.UserName = user.Username;
                        if (RolListesi.Select(x => x.RoleName).Contains("Admin"))
                        {
                            serializeModel.Folder = "Content/Anasayfa";
                            serializeModel.SubFolder = "";
                        }
                        else if (RolListesi.Select(x => x.RoleName).Contains("Kullanici"))
                        {
                            string UserUrlPath = TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl($"~/Content/Kullanici/User/{user.Username}");
                            serializeModel.Folder = UserUrlPath.Replace("~/", "");
                            serializeModel.SubFolder = "";
                        }
                        else
                        {

                        }
                        string Dizin = Server.MapPath(string.Format("~/{0}/{1}", serializeModel.Folder, serializeModel.SubFolder));
                        System.IO.DirectoryInfo DizinBilgi = new System.IO.DirectoryInfo(Dizin);
                        if (!DizinBilgi.Exists)
                        {
                            DizinBilgi.Create();
                        }
                        string userData = JsonConvert.SerializeObject(serializeModel);
                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                                 1,
                                 user.Email,
                                 DateTime.Now,
                                 DateTime.Now.AddMinutes(15),
                                 false,
                                 userData);
                        string encTicket = FormsAuthentication.Encrypt(authTicket);
                        HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                        Response.Cookies.Add(faCookie);
                        string Link = "";
                        Link = "~/";
                        if (RolListesi.Select(x => x.RoleName).Contains("Admin"))
                        {
                            Link = "~/Administrator/Anasayfa/Index";
                        }
                        else if (RolListesi.Select(x => x.RoleName).Contains("Kullanici"))
                        {
                            Link = "~/Kullanici/Anasayfa/Index";
                            KullaniciService.UserOneriKritikGunGuncelle((int)user.UserId);
                        }
                        else
                        {
                            Model.IslemMesaj.Visible = true;
                            Model.IslemMesaj.Baslik = "Giriş";
                            Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Hata;
                            Model.IslemMesaj.Mesaj = string.Format("Kullanıcı rolü belirlenmemiş.");
                        }
                        Model.ReturnUrl = Link;
                        if (Model.ReturnUrl == "~/")
                        {
                        }
                        else
                        {
                            Model.GirisDurum = true;
                            LogService.Ekle(user.UserId, Ip, LogTip.Giris, string.Format("{0} ({1} {2}) Kullanıcısı Giriş Yaptı", user.Username, user.FirstName, user.LastName));
                            Response.Redirect(TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl(Model.ReturnUrl));
                        }
                    }
                    else
                    {
                        Session["UyelikAktivasyonMail"] = user.Email;
                        Response.Redirect("~/Platform/Uyelik/AktifEt", true);
                    }
                }
                else
                {
                    Model.IslemMesaj.Visible = true;
                    Model.IslemMesaj.Baslik = "Giriş";
                    Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Hata;
                    Model.IslemMesaj.Mesaj = string.Format("Kullanıcı adı veya şifre yanlış.");
                }
            }
            else
            {
                Model.IslemMesaj.Visible = true;
                Model.IslemMesaj.Baslik = "Giriş";
                Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Hata;
                Model.IslemMesaj.Mesaj = string.Format("Kullanıcı adı veya şifre yanlış.");
            }
            return View("Index", Model);
        }

        /// <summary>
        /// The ChangePassword
        /// </summary>
        /// <returns>The <see cref="ActionResult"/></returns>
        [CustomAuthorize(Roles = "Admin")]
        public ActionResult ChangePassword()
        {
            TMS.View.Genel.ChangePassword Model = new TMS.View.Genel.ChangePassword();
            return View(Model);
        }

        /// <summary>
        /// The ChangePassword
        /// </summary>
        /// <param name="model">The <see cref="TMS.View.Membership.ChangePassword"/></param>
        /// <param name="returnUrl">The <see cref="string"/></param>
        /// <returns>The <see cref="ActionResult"/></returns>
        [HttpPost]
        [CustomAuthorize(Roles = "Admin")]
        public ActionResult ChangePassword(TMS.View.Genel.ChangePassword Model, string returnUrl = "")
        {
            CustomPrincipal currentUser = HttpContext.User as CustomPrincipal;
            using (Entities VeriTabani = new Entities())
            {
                if (ModelState.IsValid)
                {
                    var user = VeriTabani.Users.Find(currentUser.UserId);
                    if (user != null)
                    {
                        if (user.Password == Model.Sifre)
                        {
                            user.Password = Model.NewPassword;
                            VeriTabani.SaveChanges();
                            Response.RedirectPermanent(Url.Content("~/Anasayfa/Index"));
                            LogService.Ekle(user.UserId, Ip, LogTip.SifreDegistirme, string.Format("{0} ({1} {1}) Kullanıcısı Şifresini Başarıyla Değiştirdi.", user.Username, user.FirstName, user.LastName));
                        }
                        else
                        {
                            Model.IslemMesaj.Visible = true;
                            Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                            Model.IslemMesaj.Baslik = "Şifre Değiştirme İşlemleri";
                            Model.IslemMesaj.Mesaj = "Eski Şifre Yanlış";
                            LogService.Ekle(user.UserId, Ip, LogTip.SifreDegistirme, string.Format("{0} ({1} {1}) Şifresini Değiştiremedi Daha Önceki Şifresi Yanlış Girildi.", user.Username, user.FirstName, user.LastName));
                        }
                    }
                }
                else
                {
                    Model.IslemMesaj.Visible = true;
                    Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                    Model.IslemMesaj.Baslik = "Şifre Değiştirme İşlemleri";
                    Model.IslemMesaj.Mesaj = "Bilgilerde yanlışlık var kontrol ederek tekrar deneyiniz.";
                }
            }
            return View(Model);
        }

        /// <summary>
        /// The KullaniciBilgi
        /// </summary>
        /// <returns>The <see cref="ActionResult"/></returns>
        [CustomAuthorize(Roles = "Admin,User")]
        public ActionResult KullaniciBilgi()
        {
            return View();
        }

        /// <summary>
        /// The Cikis
        /// </summary>
        /// <returns>The <see cref="ActionResult"/></returns>
        [AllowAnonymous]
        public ActionResult Cikis()
        {
            FormsAuthentication.SignOut();
            LogService.Ekle(User.UserId, Ip, LogTip.Cikis, string.Format("{0} kullanıcısı Çıkış Yaptı", User.UserName));
            Session.Abandon();
            Session.Clear();
            return RedirectToAction("Index", new { @controller = "Account" });
        }
    }
}