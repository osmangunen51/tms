﻿using RazorEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMS.Panel.Web.Code.Security;
using TMS.View.Genel;

namespace TMS.Panel.Web.Areas.Platform.Controllers
{
    public class BaseController : TMS.Panel.Web.Controllers.BaseController
    {

        public Core.Service.IlService IlService { get; set; } = new Core.Service.IlService();

        protected virtual new CustomPrincipal User
        {
            get { return HttpContext.User as CustomPrincipal; }
        }


        public JsonResult IlEkle(string id)
        {
            Select2Item Sonuc = new Select2Item();
            if (!string.IsNullOrEmpty(id))
            {
                var Nesne = IlService.Get(x => x.Ad == id);
                if (Nesne == null)
                {
                    Nesne = new Core.Data.Il()
                    {
                        Ad = id
                    };
                    IlService.Create(Nesne);
                    IlService.Commit();
                }
                Sonuc.id = Nesne.No;
                Sonuc.text = Nesne.Ad;
            }
            return Json(new { Sonuc }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IlDegerGetir(string id)
        {
            Select2Item Sonuc = new Select2Item();
            if (!string.IsNullOrEmpty(id))
            {
                int No = System.Convert.ToInt32(id);
                var Nesne = IlService.Get(x => x.No == No);
                if (Nesne != null)
                {
                    Sonuc.id = Nesne.No;
                    Sonuc.text = Nesne.Ad;
                }
            }
            return Json(new { Sonuc }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult IlListesiGetir(string term)
        {
            List<Select2Item> Sonuc = IlService.GetMany(m => (term == "" || m.Ad.ToLower().Contains(term.ToLower()))).Select(Snc =>
                    new Select2Item()
                    {
                        id = Snc.No,
                        text = Snc.Ad
                    }
                ).ToList();
            return Json(new { Sonuc }, JsonRequestBehavior.AllowGet);
        }

        public IslemDurum UyelikMailiGonder(Core.Data.Users Users)
        {

            IslemDurum Sonuc = new IslemDurum();
            TMS.View.Site.MailUyelik ModelMail = new TMS.View.Site.MailUyelik() { Users = Users };


            string Key = "";
            using (Core.Lib.SifreOlusturucu Sifreci = new Core.Lib.SifreOlusturucu())
            {
                Key = Sifreci.Sifrele(string.Format("{0}é{1}", Users.UserId.ToString(), DateTime.Now.ToString()), true);
                Key = HttpUtility.UrlEncode(Key);
            }
            ModelMail.Link = Araclar.HtmlHelpers.ResolveServerUrl($"~/Platform/Uyelik/Aktivasyon/{Key}", false);
            var template = System.IO.File.ReadAllText(Server.MapPath("~/Areas/Platform/Views/Templates/Forms/_Uyelik.html"));
            var Mesaj = Razor.Parse(template, ModelMail);
            var Snc = SistemMailGonder(new List<string>()
                                        {
                                            Users.Email
                                        },
            "Üyelik | Hoş geldiniz",
            Mesaj
            );
            if (Snc.Durum)
            {
                Sonuc.Durum = true;
                Sonuc.Baslik = "Üyelik İşlemleri";
                Sonuc.Mesaj = "Mail başarıyla gönderildi.";
                Sonuc.Tip = IslemDurum.IslemMesajTip.Basarili;
                Sonuc.Deger = "";
            }
            else
            {

                Sonuc.Durum = Snc.Durum;
                Sonuc.Baslik = Snc.Baslik;
                Sonuc.Mesaj = Snc.Mesaj;
                Sonuc.Tip = IslemDurum.IslemMesajTip.Hata;
                Sonuc.Deger = "";
            }


            return Sonuc;

        }

        public IslemDurum ActivasyonMailiGonder(Core.Data.Users Users)
        {

            IslemDurum Sonuc = new IslemDurum();
            TMS.View.Site.MailUyelik ModelMail = new TMS.View.Site.MailUyelik() { Users = Users };


            string Key = "";
            using (Core.Lib.SifreOlusturucu Sifreci = new Core.Lib.SifreOlusturucu())
            {
                Key = Sifreci.Sifrele(string.Format("{0}é{1}", Users.UserId.ToString(), DateTime.Now.ToString()), true);
                Key = HttpUtility.UrlEncode(Key);
            }
            ModelMail.Link = Araclar.HtmlHelpers.ResolveServerUrl($"~/Platform/Uyelik/Aktivasyon/{Key}", false);
            var template = System.IO.File.ReadAllText(Server.MapPath("~/Areas/Platform/Views/Templates/Forms/_Aktivasyon.html"));
            var Mesaj = Razor.Parse(template, ModelMail);
            var Snc = SistemMailGonder(new List<string>()
                                        {
                                            Users.Email
                                        },
            "Üyelik | Aktivasyon",
            Mesaj
            );
            if (Snc.Durum)
            {
                Sonuc.Durum = true;
                Sonuc.Baslik = "Üyelik İşlemleri";
                Sonuc.Mesaj = "Mail başarıyla gönderildi.";
                Sonuc.Tip = IslemDurum.IslemMesajTip.Basarili;
                Sonuc.Deger = "";
            }
            else
            {

                Sonuc.Durum = Snc.Durum;
                Sonuc.Baslik = Snc.Baslik;
                Sonuc.Mesaj = Snc.Mesaj;
                Sonuc.Tip = IslemDurum.IslemMesajTip.Hata;
                Sonuc.Deger = "";
            }


            return Sonuc;

        }

        public IslemDurum SifremiUnuttumMailiGonder(Core.Data.Users Users)
        {

            IslemDurum Sonuc = new IslemDurum();
            TMS.View.Site.MailSifremiUnuttum ModelMail = new TMS.View.Site.MailSifremiUnuttum() { Users = Users };
            var template = System.IO.File.ReadAllText(Server.MapPath("~/Areas/Platform/Views/Templates/Forms/_SifremiUnuttum.html"));
            var Mesaj = Razor.Parse(template, ModelMail);
            var Snc = SistemMailGonder(new List<string>()
                                        {
                                            Users.Email
                                        },
            "Üyelik | Şifremi Unuttum",
            Mesaj
            );
            if (Snc.Durum)
            {
                Sonuc.Durum = true;
                Sonuc.Baslik = "Üyelik İşlemleri";
                Sonuc.Mesaj = "Mail başarıyla gönderildi.";
                Sonuc.Tip = IslemDurum.IslemMesajTip.Basarili;
                Sonuc.Deger = "";
            }
            else
            {

                Sonuc.Durum = Snc.Durum;
                Sonuc.Baslik = Snc.Baslik;
                Sonuc.Mesaj = Snc.Mesaj;
                Sonuc.Tip = IslemDurum.IslemMesajTip.Hata;
                Sonuc.Deger = "";
            }


            return Sonuc;

        }


        public IslemDurum DavetMailiGonder(string MailAdresi)
        {

            IslemDurum Sonuc = new IslemDurum();
            var template = System.IO.File.ReadAllText(Server.MapPath("~/Areas/Platform/Views/Templates/Forms/_Davet.html"));
            var Mesaj = Razor.Parse(template);
            var Snc = SistemMailGonder(new List<string>()
                                        {
                                            MailAdresi
                                        },
            "Üyelik | Davet ediyoruz.",
            Mesaj
            );
            if (Snc.Durum)
            {
                Sonuc.Durum = true;
                Sonuc.Baslik = "Üyelik İşlemleri";
                Sonuc.Mesaj = "Mail başarıyla gönderildi.";
                Sonuc.Tip = IslemDurum.IslemMesajTip.Basarili;
                Sonuc.Deger = "";
            }
            else
            {

                Sonuc.Durum = Snc.Durum;
                Sonuc.Baslik = Snc.Baslik;
                Sonuc.Mesaj = Snc.Mesaj;
                Sonuc.Tip = IslemDurum.IslemMesajTip.Hata;
                Sonuc.Deger = "";
            }


            return Sonuc;

        }


    }
}