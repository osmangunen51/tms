﻿using TMS.View.Genel;

namespace TMS.Panel.Web.Areas.Platform.Code
{
    public class GenelView : BaseView
    {
        public GenelView()
        {
            IslemMesaj = new View.Genel.IslemMesaj();
        }
    }
}