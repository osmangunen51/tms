﻿(function (theme, $) {

    'use strict';

    if (typeof (window.localStorage) === "undefined") return false;

    var closed = localStorage.getItem('sidebar-left-closed'),
        $window = $(window);

    $window.on('sidebar-left-toggle', function (e, opt) {
        localStorage.setItem('sidebar-left-closed', opt.added);
    });

    if (closed === 'true') {
        $('html').addClass('sidebar-left-collapsed').removeClass('sidebar-left-closed');
    }

}).apply(this, [window.theme, jQuery]);

var ResimEkle = function (context) {
    var ui = $.summernote.ui;
    var button = ui.button({
        contents: '<i class="fa fa-picture-o"/> Resim Ekle',
        //tooltip: 'Resim Ekle',
        click: function (e) {
            EditorAcResimEkle(context);
        }
    });
    return button.render();
}
var LinkEkle = function (context) {
    var ui = $.summernote.ui;
    var button = ui.button({
        contents: '<i class="fa fa-link"/> Link Ekle',
        //tooltip: 'Link Ekle',
        click: function (e) {
            EditorAcLinkEkle(context);
        }
    });
    return button.render();
}
var VideoEkle = function (context) {
    var ui = $.summernote.ui;
    var button = ui.button({
        contents: '<i class="fa fa-youtube-play"/> Video Ekle',
        //tooltip: 'Video Ekle',
        click: function (e) {
            EditorAcVideoEkle(context);
        }
    });
    return button.render();
}
var AuidoEkle = function (context) {
    var ui = $.summernote.ui;
    var button = ui.button({
        contents: '<i class="fa fa-file-audio-o"/> Ses Ekle',
        //tooltip: 'Ses Ekle',
        click: function (e) {
            EditorAcAudioEkle(context);
        }
    });
    return button.render();
}
function EditorAcAudioEkle(editor) {
    var Folder = editor.data("userelfinderfolder");
    var subFolder = editor.data("userelfindersubfolder");
    if (Folder === null) {
        Folder = "Content";
    }
    if (subFolder === null) {
        subFolder = "Bos";
    }
    $('<div />').dialogelfinder({
        url: '/connector',
        customData: { folder: Folder, subFolder: subFolder },
        rememberLastDir: false,
        lang: 'tr',
        commandsOptions: {
            getfile: {
                oncomplete: 'destroy' // destroy elFinder after file selection
            }
        },
        getFileCallback: function (File) {
            var url = File.path.toString();
            url = url.replace('Files', File.baseUrl);
            var Html = '<p><audio controls><source src="' + url + '" type="audio/mpeg"></audio></p><p></p>'
            editor.summernote('pasteHTML', Html);
        }
    });
}
function EditorAcResimEkle(editor) {
    var Folder = editor.data("userelfinderfolder");
    var subFolder = editor.data("userelfindersubfolder");
    console.log(Folder);
    console.log(subFolder);
    if (Folder === null) {
        Folder = "Content";
    }
    if (subFolder === null) {
        subFolder = "Bos";
    }
    $('<div />').dialogelfinder({
        url: '/connector',
        customData: { folder: Folder, subFolder: subFolder },
        rememberLastDir: false,
        lang: 'tr',
        commandsOptions: {
            getfile: {
                oncomplete: 'destroy' // destroy elFinder after file selection
            }
        },
        getFileCallback: function (File) {
            var url = File.path.toString();
            url = url.replace('Files', File.baseUrl);
            console.log(url);
            editor.summernote('editor.insertImage', url, function ($image) {
                $image.css('width', $image.width());
                $image.attr('class', "img-responsive");
            });
        }
    });
}
function EditorAcVideoEkle(editor) {
    var Folder = editor.data("userelfinderfolder");
    var subFolder = editor.data("userelfindersubfolder");
    if (Folder === null) {
        Folder = "Content";
    }
    if (subFolder === null) {
        subFolder = "Bos";
    }
    $('<div />').dialogelfinder({
        url: '/connector',
        customData: { folder: Folder, subFolder: subFolder },
        rememberLastDir: false,
        lang: 'tr',
        commandsOptions: {
            getfile: {
                oncomplete: 'destroy' // destroy elFinder after file selection
            }
        },
        getFileCallback: function (File) {
            var url = File.path.toString();
            url = url.replace('Files', File.baseUrl);
            var node = document.createElement('div');
            var html = '<video width="100%" height="400" controls="controls"><source src="' + url + '" type= "video/mp4" autostart= "true"></video>';
            editor.summernote.invoke('editor.insertText', html);
        }
    });
}
function EditorAcLinkEkle(editor) {
    var Folder = editor.data("userelfinderfolder");
    var subFolder = editor.data("userelfindersubfolder");
    if (Folder === null) {
        Folder = "Content";
    }
    if (subFolder === null) {
        subFolder = "Bos";
    }
    $('<div />').dialogelfinder({
        url: '/connector',
        customData: { folder: Folder, subFolder: subFolder },
        rememberLastDir: false,
        lang: 'tr',
        commandsOptions: {
            getfile: {
                oncomplete: 'destroy' // destroy elFinder after file selection
            }
        },
        getFileCallback: function (File) {
            var url = File.path.toString();
            url = url.replace('Files', File.baseUrl);
            editor.summernote('createLink',
                {
                    text: File.name,
                    url: url,
                    newWindow: true
                }
            );
        }
    });
}
$(".summernote").each(function (Index) {
    var Editor = $(this);
    Editor.summernote({
        height: 400,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            //['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']],
            ['ResimEkle', ['ResimEkle']],
            ['LinkEkle', ['LinkEkle']],
            ['VideoEkle', ['VideoEkle']],
            ['AuidoEkle', ['AuidoEkle']]
        ],
        buttons: {
            ResimEkle: ResimEkle(Editor),
            LinkEkle: LinkEkle(Editor),
            VideoEkle: VideoEkle(Editor),
            AuidoEkle: AuidoEkle(Editor)
        }
    });
});
(function ($) {
    var GetUrlBilgi = function (href) {
        var l = document.createElement("a");
        l.href = href;
        return l;
    };
    $(".BtnResimYukle").click(function () {
        var NesneId = $(this).attr("data-hedef");
        var Folder = $(this).attr("data-userelfinderfolder");
        var subFolder = $(this).attr("data-userelfindersubfolder");
        $('<div />').dialogelfinder({
            url: '/connector',
            customData: { folder: Folder, subFolder: subFolder },
            rememberLastDir: false,
            lang: 'tr',
            commandsOptions: { getfile: { oncomplete: 'destroy' } },
            getFileCallback: function (File) {
                console.log(File);
                var url = File.path.toString();
                url = url.replace('Files', File.baseUrl);
                var urlRsm = url;
                var Lnk = GetUrlBilgi(url);
                url = '~' + Lnk.pathname;
                var HedefNesne = $('#' + NesneId);
                console.log(HedefNesne);
                if (HedefNesne != null) {
                    url = url.replace("~C", "~/C");
                    url = url.replace("~c", "~/c");
                    HedefNesne.val(url);
                    console.log(url);
                    $('#SpnRsm' + NesneId).attr("style", 'background-image:url(' + urlRsm + '?h=70&amp;w=150&amp;mode=max&amp;scale=both);background-repeat:no-repeat;background-size:100%;width:150px;height:70px;');
                }
            }
        });
    });
    $(".BtnResimTemizle").click(function () {
        var NesneId = $(this).attr("data-hedef");
        var HedefNesne = $('#' + NesneId);
        if (HedefNesne != null) {
            HedefNesne.val('');
            $('#SpnRsm' + NesneId).attr("style", "width:150px;height:70px;");
        }
    });

    if (typeof GeriSayimZaman === "undefined") {
        var Tmp = 1;
    }
    else {
        $('#GeriSayim').countdown(GeriSayimZaman)
            .on('update.countdown', function (event) {
                var $this = $(this);
                $(this).html(event.strftime(''
                    + '<span class="btn btn-info"><strong>%M</strong></span> Dk. '
                    + '<span class="btn btn-success btn-sm"><strong>%S</strong></span> Sn.'));
            }
            ).on('finish.countdown', function (event) {
                window.open(GeriSayimUrl, "_self")
            }
            );
    }


}).apply(this, [jQuery]);
$(window).load(function () {
    $(".se-pre-con").fadeOut("slow");
});
// Tooltip
(function ($) {
    $('.jstreeListe').jstree({
        'core': {
            'themes': {
                'name': 'proton',
                'responsive': true
            }
        }
    });
    $(".jstreeListe").bind(
        "select_node.jstree", function (evt, data) {
            var NesneId = data.node.li_attr["data-nesne"];
            var HedefNesne = $('#' + NesneId);
            console.log(HedefNesne);
            if (HedefNesne != null) {
                var Deger = data.node.li_attr["data-deger"];
                HedefNesne.val(Deger);
            }
        }
    )
        .on('changed.jstree', function (e, data) {
            var path = data.instance.get_path(data.node, '/');
            console.log('Selected: ' + path);
        });
}).apply(this, [jQuery]);

$('.dropdown-menu ul li').click(function (e) {
});
$('.OpenDropDownMenu').on({
    "click": function (e) {
        e.stopPropagation();
    }
});
// Tooltip
(function ($) {
    $('.datatabkel').jstree({
        'core': {
            'themes': {
                'name': 'proton',
                'responsive': true
            }
        }
    });
    $(".jstreeListe").bind(
        "select_node.jstree", function (evt, data) {
            var NesneId = data.node.li_attr["data-nesne"];
            var HedefNesne = $('#' + NesneId);
            console.log(HedefNesne);
            if (HedefNesne != null) {
                var Deger = data.node.li_attr["data-deger"];
                HedefNesne.val(Deger);
            }
        }
    )
        .on('changed.jstree', function (e, data) {
            var path = data.instance.get_path(data.node, '/');
            console.log('Selected: ' + path);
        });
}).apply(this, [jQuery]);

function OzelArama(filterVal, columnVal) {
    var found;
    if (columnVal === '') {
        return true;
    }
    switch (filterVal) {
        case 'happy':
            found = columnVal.search(/:-\]|:\)|Happy|JOY|:D/g);
            break;
        case 'sad':
            found = columnVal.search(/:\(|Sad|:'\(/g);
            break;
        case 'angry':
            found = columnVal.search(/!!!|Arr\.\.\./g);
            break;
        case 'lucky':
            found = columnVal.search(/777|Bingo/g);
            break;
        case 'january':
            found = columnVal.search(/01|Jan/g);
            break;
        default:
            found = 1;
            break;
    }

    if (found !== -1) {
        return true;
    }
    return false;
}


function TabloToDataTable(Tablo, DisplaySize) {
    if (typeof (DisplaySize) === 'undefined') a = 50;
    $(Tablo).dataTable(
        {
            "stateSave": true,
            "search": {
                "caseInsensitive": true
            },
            "iDisplayLength": DisplaySize,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Turkish.json",
                "sSearch": "Ara"
            },
            "lengthMenu": [[50, 100, 250, 500, -1], [50, 100, 250, 500, "Tümü"]]
        }
    );
}


function TabloToDataFiltreTable(Tablo, DisplaySize) {
    if (typeof (DisplaySize) === 'undefined') a = 50;
    var table = $(Tablo).dataTable(
        {
            "stateSave": true,
            "search": {
                "caseInsensitive": true
            },
            "iDisplayLength": DisplaySize,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Turkish.json",
                "sSearch": "Ara"
            },
            "lengthMenu": [[50, 100, 250, 500, -1], [50, 100, 250, 500, "Tümü"]]
        }
    );
    $('#' + $(Tablo).attr("id") + ' .arm').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text"');
    });



}


function AreaTextSummerNote(Tablo) {
    var Editor = $(Tablo);
    Editor.summernote({
        height: 400,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            //['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']],
            ['ResimEkle', ['ResimEkle']],
            ['LinkEkle', ['LinkEkle']],
            ['VideoEkle', ['VideoEkle']],
            ['AuidoEkle', ['AuidoEkle']]
        ],
        buttons: {
            ResimEkle: ResimEkle(Editor),
            LinkEkle: LinkEkle(Editor),
            VideoEkle: VideoEkle(Editor),
            AuidoEkle: AuidoEkle(Editor)
        }
    });
}
function KontrolRender() {
    AreaTextSummerNote('.summernote');
    // Tooltip
    (function ($) {
        $('.jstreeListe').jstree({
            'core': {
                'themes': {
                    'name': 'proton',
                    'responsive': true
                }
            }
        });
        $(".jstreeListe").bind(
            "select_node.jstree", function (evt, data) {
                var NesneId = data.node.li_attr["data-nesne"];
                var HedefNesne = $('#' + NesneId);
                console.log(HedefNesne);
                if (HedefNesne != null) {
                    var Deger = data.node.li_attr["data-deger"];
                    HedefNesne.val(Deger);
                }
            }
        )
            .on('changed.jstree', function (e, data) {
                var path = data.instance.get_path(data.node, '/');
                console.log('Selected: ' + path);
            });
    }).apply(this, [jQuery]);
    $('[data-plugin-selectTwo]').each(function () {
        var $this = $(this),
            opts = {};
        var pluginOptions = $this.data('plugin-options');
        if (pluginOptions)
            opts = pluginOptions;
        $this.themePluginSelect2(opts);
    });
    $('[data-plugin-ios-switch]').each(function () {
        var $this = $(this);
        $this.themePluginIOS7Switch();
    });
}
(function ($) {
    var clipboardcopy = new Clipboard('.kopyalakes');
    clipboardcopy.on('success', function (e) {
        e.clearSelection();
    });
}).apply(this, [jQuery]);


// Tooltip
(function ($) {
    TabloToDataTable('.DtTable', 50);
}).apply(this, [jQuery]);

(function ($) {
    TabloToDataFiltreTable('.DtTableFiltre', 50);
}).apply(this, [jQuery]);

(function ($) {
    $('.IlSecim').change(function () {
        var IlNo = $(this).val();
        var Hedef = $(this).data("hedef");
        var Url = $(this).data("url");
        console.log(IlNo);
        if (IlNo != null && IlNo != '') {
            $.ajax({
                type: "post",
                url: Url,
                data: { IlNo: IlNo },
                success: function (Sonuclar) {
                    console.log(Sonuclar);
                    $('#' + Hedef).empty();
                    $.each(Sonuclar, function (index, Ilce) {
                        $('#' + Hedef).append($('<option/>', {
                            value: Ilce.No,
                            text: Ilce.Ad
                        }));
                    });
                },
                error: function () {
                    alert("Hata");
                },
                beforeSend: function () {
                },
                complete: function () {
                }
            });
        }
    });
}).apply(this, [jQuery]);

function OnayKutusuOnayAl(title, text, type, confirmButtonText, cancelButtonText, Url, target = "") {
    var sonuc = false;
        swal({
            title: title,
            text: text,
            type: type,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: confirmButtonText,
            cancelButtonText: cancelButtonText
        },
        function (isConfirm) {
            if (isConfirm) {
                if (target === "") {
                    window.location.replace(Url);
                }
                else {
                    window.open(Url, target);
                }
            }
        });
    return sonuc;
}

$(function ($) {
    //----Sol menü - url değişimine göre selector işlemleri------------------------------------
    var url = window.location.href;

    if ((url.split("/")[3] || url.split("/")[4] || url.split("/")[5]) != undefined) {

        var seciliUrl = "/" + url.split("/")[3] + "/" + url.split("/")[4] + "/" + url.split("/")[5];
        var hedef = $('nav#menu a[href="' + seciliUrl + '"]');
        hedef.addClass("text-bold text-primary");
        hedef.parent("li").parent("ul").parent("li").addClass("nav-active nav-expanded");
    }
    else {
        $('nav#menu a[href="/"]').parent("li").addClass("nav-active");
    }
    //-----------------------------------------------------------------------------------------
});

(function ($) {
    $(".DosyaSec").change(function () {
        var Hedef = '#' + $(this).data('hedef');
        var HedefOnIzleme = '#SpnRsm' + $(this).data('hedef');

        if (window.FormData !== undefined) {
            var fileUpload = $(this).get(0);
            var files = fileUpload.files;
            var fileData = new FormData();
            for (var i = 0; i < files.length; i++) {
                fileData.append(files[i].name, files[i]);
            }
            $.ajax({
                url: '/Administrator/DosyaYonetim/DosyaYukle',
                type: "POST",
                contentType: false, // Not to set any content header
                processData: false, // Not to process data
                data: fileData,
                beforeSend: function ()
                {
                    $(".se-pre-con").fadeIn("slow");
                },
                complete: function ()
                {
                    $(".se-pre-con").fadeOut("slow");
                },
                success: function (Sonuc)
                {
                    console.log($(Hedef));
                    $(".se-pre-con").fadeIn("slow");
                    $(Hedef).val(Sonuc.Deger);
                    $(HedefOnIzleme).attr("src", Sonuc.Deger.replace("~/","/"));
                    $(".se-pre-con").fadeOut("slow");
                },
                error: function (Hata)
                {
                    swal({
                        title: "Dosya Yükleme",
                        text: "Hata Oluştu...",
                        type: 'error',
                        showCancelButton: false
                    });
                }
            });
        } else {
            swal({
                title: "Dosya Yükleme",
                text: "Desteklenmeyen Format",
                type: 'error',
                showCancelButton: false
            });
        }
    });

}).apply(this, [jQuery]);

$(function ($) {
    $('.ModalPencere').click(
        function () {
            var height = $(this).attr('data-height');
            var width = $(this).attr('data-width');
            var Html = $(this).attr('data-html');
            var Tip = $(this).attr('data-tip');
            var Url = $(this).attr('data-url');
            var Baslik = $(this).attr('data-baslik');
            var ButtonText = $(this).attr('data-buttontext');
            if (Tip === "1") {
                $('#_ModalPencereBody').load(Url, function () {
                    $('#Baslik').html(Baslik);
                    $('#btnKapat').val(ButtonText);

                });
            }
            if (Tip === "2") {
                if (Url.includes('.jpg') | Url.includes('.png') | Url.includes('.jpeg')) {
                    Html = '<img style="width:100%;height:100%" src="' + Url + '" </img>';
                } else if (Url.includes('.pdf')) {
                    Html = '<object data="' + Url + '" type="application/pdf" width="100%" height="550px"><p>Alternative text - include a link < a href = "' + Url + '"> to the PDF!</a ></p></object>'
                }
                $('#_ModalPencereBody').html(Html);
                $('#Baslik').html(Baslik);
                $('#btnKapat').val(ButtonText);
            }
            $('#_ModalPencere').modal({
                show: true,
                backdrop: 'static',
                keyboard: false
            });
        }
    );
});

$(function ($) {
    $(document).on("click", ".btndelete", function () {
        var link = $(this).data("link");
        var baslik = $(this).data("baslik");
        var mesaj = $(this).data("mesaj");
        return OnayKutusuOnayAl(baslik, mesaj, 'warning', 'Evet', 'Hayır', link);
    });
    $(document).on("click", ".btnclone", function () {
        var link = $(this).data("link");
        var baslik = $(this).data("baslik");
        var mesaj = $(this).data("mesaj");
        return OnayKutusuOnayAl(baslik, mesaj, 'warning', 'Evet', 'Hayır', link);
    });
});



$(function ($) {
    $(".remoteselect").each(function () {

        var data_createurl = $(this).data("createurl");
        console.log(data_createurl);
        var data_selecturl = $(this).data("selecturl");
        console.log(data_selecturl);
        var data_initurl = $(this).data("initurl");
        var data_initvalue = $(this).data("initvalue");
        var minimumInputLength = 0;
        if ($(this).data("minimumInputLength") !== "undefined") {
            minimumInputLength = $(this).data("minimumInputLength");
        }

        var selectislem = $(this).select2({
            minimumInputLength: minimumInputLength,
            ajax: {
                url: data_selecturl,
                type: "POST",
                dataType: 'json',
                data: function (term) {
                    return {
                        term: term,
                    };
                },
                results: function (data, page) {
                    return { results: data.Sonuc };
                },
            },
            initSelection: function (element, callback) {
                var id = data_initvalue;
                if (id !== "") {
                    $.ajax(data_initurl, {
                        data: { id: id },
                        dataType: "json"
                    }).done(function (data) {
                        //console.log(data);
                        callback(data.Sonuc);
                    });
                }
            }
        });

        selectislem.on('select2:select', function (e) {
            var data = e.params.data;
            $.ajax({
                type: 'GET',
                url: data_createurl + e.params.data
            }).then(function (data) {
                var option = new Option(data.text, data.id, true, true);
                selectislem.append(option).trigger('change');
                selectislem.trigger({
                    type: 'select2:select',
                    params: {
                        data: data
                    }
                });
            });
        });
    });

    //if (window.jQuery) {
    //    $(window).load(function () {
    //        var top = parseInt($.cookie("top"));
    //        if (top) {
    //            $(document).scrollTop(top);
    //        }
    //        $(document).scroll(function () {
    //            var top = $(document).scrollTop();
    //            $.cookie("top", top);
    //        });
    //    });
    //}


});

