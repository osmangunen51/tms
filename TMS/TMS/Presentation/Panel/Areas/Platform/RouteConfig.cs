﻿using System.Web.Mvc;
namespace TMS.Panel.Web.Areas.Platform
{
    internal static class RouteConfig
    {
        internal static void RegisterRoutes(AreaRegistrationContext context)
        {
            context.MapRoute(
                             "Platform_default",
                             "Platform/{controller}/{action}/{id}",
                            defaults: new { controller = "Anasayfa", action = "Index", id = UrlParameter.Optional },
                            namespaces: new[] { "TMS.Panel.Web.Areas.Platform.Controllers" }
                         );
        }
    }
}
