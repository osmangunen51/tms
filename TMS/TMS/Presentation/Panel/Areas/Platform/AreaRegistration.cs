﻿using System.Web.Mvc;
using System.Web.Optimization;

namespace TMS.Panel.Web.Areas.Platform
{
    public class PlatformAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Platform";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            RegisterRoutes(context);
            RegisterBundles();
        }

        private void RegisterRoutes(AreaRegistrationContext context)
        {
            Platform.RouteConfig.RegisterRoutes(context);
        }

        private void RegisterBundles()
        {
            Platform.BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}