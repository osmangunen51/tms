﻿using ElFinder;
using System.IO;
using System.Web.Mvc;

namespace TMS.Panel.Web.Areas.Kullanici.Controllers
{
    public partial class FileController : Controller
    {
        public virtual ActionResult Index(string folder, string subFolder)
        {
            FileSystemDriver driver = new FileSystemDriver();
            var root = new Root(
                    new DirectoryInfo(Server.MapPath("~/" + folder)),
                    "http://" + Request.Url.Authority + "/" + folder)
            {
                // Sample using ASP.NET built in Membership functionality...
                // Only the super user can READ (download files) & WRITE (create folders/files/upload files).
                // Other users can only READ (download files)
                // IsReadOnly = !User.IsInRole(AccountController.SuperUser)
                IsReadOnly = false,
                Alias = "Files", // Beautiful name given to the root/home folder
                MaxUploadSizeInKb = 10000 // Limit imposed to user uploaded file <= 500 KB
            };
            // Was a subfolder selected in Home Index page?
            if (!string.IsNullOrEmpty(subFolder))
            {
                root.StartPath = new DirectoryInfo(Server.MapPath("~/" + folder + "/" + subFolder));
            }
            driver.AddRoot(root);
            var connector = new Connector(driver);
            return connector.Process(HttpContext.Request);
        }

        public virtual ActionResult SelectFile(string target)
        {
            FileSystemDriver driver = new FileSystemDriver();
            driver.AddRoot(
                new Root(
                    new DirectoryInfo(Server.MapPath("~/")),
                    "http://" + Request.Url.Authority + "/")
                { IsReadOnly = false });
            var connector = new Connector(driver);
            return Json(connector.GetFileByHash(target).FullName);
        }
    }
}