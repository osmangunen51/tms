﻿using Newtonsoft.Json;
using TMS.Core.Service;
using TMS.Panel.Web.Areas.Kullanici.Code;
using TMS.Panel.Web.Code;
using TMS.Panel.Web.Code.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace TMS.Panel.Web.Areas.Kullanici.Controllers
{
    public class KullaniciController : KullaniciBaseController
    {
        private KullaniciService KullaniciService = new KullaniciService();
        private UsersService UsersService = new UsersService();
        private UserRolesService UserRolesService = new UserRolesService();
        private RolesService RolesService = new RolesService();

        [Compress]
        public ActionResult Index()
        {
            GenelView Model = new GenelView();
            return View("Index", Model);
        }

        public ActionResult Profil()
        {
            View.Kullanici.Profil Model = new View.Kullanici.Profil();
            Model.AktifKayit = KullaniciService.Get(x => x.UserNo == User.UserId);
            return View(Model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [Compress]
        public ActionResult Profil(TMS.View.Kullanici.Profil Model)
        {
            switch (Request.Form["BtnIslem"])
            {
                case "Kaydet":
                    {
                        if (ModelState.IsValid)
                        {
                            Model.IslemMesaj.Visible = true;
                            Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Basarili;
                            Model.IslemMesaj.Baslik = "Ögrenci Profil İşlemleri";
                            try
                            {
                                Core.Data.Kullanici Nesne = KullaniciService.GetById(Model.AktifKayit.No);
                                if (Nesne != null)
                                {
                                    using (System.Transactions.TransactionScope Transaction = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, TimeSpan.FromMinutes(30)))
                                    {
                                        Nesne.Ad = Model.AktifKayit.Ad;
                                        Nesne.Soyad = Model.AktifKayit.Soyad;
                                        Nesne.Telefon = Model.AktifKayit.Telefon;
                                        Nesne.Resim = Model.AktifKayit.Resim;

                                        KullaniciService.Update(Nesne);
                                        KullaniciService.Commit();
                                        Model.IslemMesaj.Mesaj = "Ögrenci Profili Başarıyla Güncellendi.";
                                        Model.AktifKayit = Nesne;

                                        Core.Data.Users Usr = UsersService.Get(x => x.UserId == User.UserId);
                                        Usr.FirstName = Nesne.Ad;
                                        Usr.LastName = Nesne.Soyad;
                                        Usr.Image = Nesne.Resim;
                                        UsersService.Update(Usr);
                                        UsersService.Commit();
                                        List<TMS.Core.Data.Roles> RolListesi = UsersService.RolListesiGetir(Usr.UserId);
                                        CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
                                        serializeModel.UserId = Usr.UserId;
                                        serializeModel.FirstName = Usr.FirstName;
                                        serializeModel.LastName = Usr.LastName;
                                        serializeModel.roles = RolListesi.Select(x => x.RoleName).ToArray();
                                        serializeModel.Image = Usr.Image;
                                        serializeModel.Telefon = Usr.Telefon;
                                        serializeModel.UserName = Usr.Username;
                                        if (RolListesi.Select(x => x.RoleName).Contains("Admin"))
                                        {
                                            serializeModel.Folder = "Content/Anasayfa";
                                            serializeModel.SubFolder = "";
                                        }
                                        else if (RolListesi.Select(x => x.RoleName).Contains("Kullanici"))
                                        {
                                            string UserUrlPath = TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl($"~/Content/Kullanici/User/{Usr.Username}");
                                            serializeModel.Folder = UserUrlPath.Replace("~/", "");
                                            serializeModel.SubFolder = "";
                                        }
                                        else if (RolListesi.Select(x => x.RoleName).Contains("Ogretmen"))
                                        {
                                            string UserUrlPath = TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl($"~/Content/Ogretmen/User/{Usr.Username}");
                                            serializeModel.Folder = UserUrlPath.Replace("~/", "");
                                            serializeModel.SubFolder = "";
                                        }
                                        else if (RolListesi.Select(x => x.RoleName).Contains("OgretimElemani"))
                                        {
                                            string UserUrlPath = TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl($"~/Content/OgretimElemani/User/{Usr.Username}");
                                            serializeModel.Folder = UserUrlPath.Replace("~/", "");
                                            serializeModel.SubFolder = "";
                                        }
                                        else
                                        {

                                        }
                                        string Dizin = Server.MapPath(string.Format("~/{0}/{1}", serializeModel.Folder, serializeModel.SubFolder));
                                        System.IO.DirectoryInfo DizinBilgi = new System.IO.DirectoryInfo(Dizin);
                                        if (!DizinBilgi.Exists)
                                        {
                                            DizinBilgi.Create();
                                        }
                                        string userData = JsonConvert.SerializeObject(serializeModel);
                                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                                                 1,
                                                 Usr.Email,
                                                 DateTime.Now,
                                                 DateTime.Now.AddMinutes(15),
                                                 false,
                                                 userData);
                                        string encTicket = FormsAuthentication.Encrypt(authTicket);
                                        HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                                        Response.Cookies.Add(faCookie);
                                        Transaction.Complete();
                                    }
                                }
                                Response.Redirect(TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl(string.Format("~/Kullanici/Anasayfa/Index")));
                                return View(Model);
                            }
                            catch (Exception ex)
                            {
                                Model.IslemMesaj.Visible = true;
                                Model.IslemMesaj.Tip = TMS.View.Genel.IslemMesajTip.Hata;
                                Model.IslemMesaj.Baslik = "Ögrenci Profil İşlemleri";
                                Model.IslemMesaj.Mesaj = "Hata! " + ex.Message;
                                return View(Model);
                            }
                        }
                        break;
                    }
                case "İptal Et":
                    {
                        Response.Redirect(TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl(string.Format("~/Kullanici/Anasayfa/Index")), true);
                        return View("Index", Model);
                    }
                default:
                    break;
            }
            return View(Model);
        }


    }
}