﻿using System;
using System.Web.Mvc;
using TMS.Core.Lib;
using TMS.Core.Service;
using TMS.View.Genel;
namespace TMS.Panel.Web.Areas.Kullanici.Controllers
{
    public class TokenController : KullaniciBaseController
    {
        private TokenService TokenService = new TokenService();
        public TokenController()
        {
        }
        public ActionResult Index()
        {
            TMS.View.Kullanici.Token Model = new TMS.View.Kullanici.Token();
            Model.AktifKayit = new TMS.Core.Data.Token();
            Model.TokenListesi = TokenService.TokenListesiGetir(this.User.UserId);
            return View(Model);
        }

        [HttpPost]
        public ActionResult Index(TMS.View.Kullanici.Token Model, FormCollection Frm)
        {
            Model.TokenListesi = TokenService.TokenListesiGetir(this.User.UserId);
            return View(Model);
        }

        public ActionResult Delete(int id)
        {
            TMS.View.Kullanici.Token Model = new TMS.View.Kullanici.Token();
            var Aktif = TokenService.GetById(id);

            if (Aktif.UserNo != this.User.UserId)
            {
                Response.Redirect(TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl("~/Kullanici/Token"), true);
            }
            if (Aktif != null)
            {
                try
                {
                    TokenService.Delete(Aktif);
                    TokenService.Commit();
                    Model.IslemMesaj.Visible = true;
                    Model.IslemMesaj.Tip = IslemMesajTip.Basarili;
                    Model.IslemMesaj.Baslik = "Silme İşlemleri";
                    Model.IslemMesaj.Mesaj = "Başarıyla Silindi.";

                }
                catch (Exception)
                {
                    Model.IslemMesaj.Visible = true;
                    Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                    Model.IslemMesaj.Baslik = "Silme İşlemleri";
                    Model.IslemMesaj.Mesaj = "Hata Oluştu.";
                }
            }
            else
            {
                Model.IslemMesaj.Visible = true;
                Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                Model.IslemMesaj.Baslik = "Silme İşlemleri";
                Model.IslemMesaj.Mesaj = "Bulunamadı";
            }
            return RedirectToAction("Index", "Token", Model);
        }
        public ActionResult Edit(int id)
        {
            TMS.View.Kullanici.Token Model = new TMS.View.Kullanici.Token();
            Model.AktifKayit = TokenService.GetById(id);
            if (Model.AktifKayit.UserNo != this.User.UserId)
            {
                Response.Redirect(TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl("~/Kullanici/Token"), true);
            }
            return View(Model);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(TMS.View.Kullanici.Token Model, FormCollection Frm)
        {
            switch (Request.Form["BtnIslem"])
            {
                case "Kaydet":
                    {
                        if (ModelState.IsValid)
                        {
                            try
                            {
                                Model.IslemMesaj.Visible = true;
                                Model.IslemMesaj.Tip = IslemMesajTip.Basarili;
                                Model.IslemMesaj.Baslik = "Token İşlemleri";
                                Model.AktifKayit.Ad = Model.AktifKayit.Ad.Trim();
                                Model.AktifKayit.Durum =(short)Durum.Pasif;
                                if (Model.AktifKayit.No == 0)
                                {
                                    var Kontrol = TokenService.Get(x => x.Ad.ToLower()==Model.AktifKayit.Ad.ToLower());
                                    if (Kontrol == null)
                                    {
                                        Model.AktifKayit.Durum = (short)Durum.Aktif;
                                        Model.AktifKayit.UserNo = this.User.UserId;
                                        Model.AktifKayit.Tarih = DateTime.Now;
                                        TokenService.Create(Model.AktifKayit);
                                        Model.IslemMesaj.Mesaj = "Token Başarıyla Eklendi.";
                                        TokenService.Commit();
                                    }
                                    else
                                    {

                                        Model.IslemMesaj.Visible = true;
                                        Model.IslemMesaj.Baslik = "Token İşlemleri";
                                        Model.IslemMesaj.Mesaj = "Bu Token Adı ile bir kayıt daha önce kayıt eidlmiş lütfen başka bir ad giriniz.";
                                        Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                                        return View(Model);
                                    }
                                }
                                else
                                {
                                    var Nesne = TokenService.GetById(Model.AktifKayit.No);
                                    Nesne.Ad = Model.AktifKayit.Ad;
                                    Nesne.Aciklama = Model.AktifKayit.Ad;
                                    Nesne.Miktar = Model.AktifKayit.Miktar;
                                    TokenService.Update(Model.AktifKayit);
                                    Model.IslemMesaj.Mesaj = "Token Başarıyla Güncellendi.";
                                    TokenService.Commit();
                                }

                                Model.TokenListesi = TokenService.TokenListesiGetir(this.User.UserId);
                                return RedirectToAction("Index", "Token", Model);
                            }
                            catch (Exception ex)
                            {
                                Model.IslemMesaj.Visible = true;
                                Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                                Model.IslemMesaj.Baslik = "Token İşlemleri";
                                Model.IslemMesaj.Mesaj = "Hata! " + ex.Message;
                                return View(Model);
                            }
                        }
                        else
                        {
                            Model.IslemMesaj.Visible = true;
                            Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                            Model.IslemMesaj.Baslik = "Token İşlemleri";
                            Model.IslemMesaj.Mesaj = "Hatalar :<br>" + ModelState.HataListesi().ListToString("<br>");
                            return View(Model);
                        }
                    }
                case "İptal Et":
                    {
                        Model.TokenListesi = TokenService.TokenListesiGetir(this.User.UserId);
                        return RedirectToAction("Index", "Token", Model);
                    }
                default:
                    break;
            }
            return View(Model);
        }
        public ActionResult Create()
        {
            TMS.View.Kullanici.Token Model = new TMS.View.Kullanici.Token();
            Model.AktifKayit = new TMS.Core.Data.Token();
            Model.AktifKayit.Tarih = DateTime.Now;
            Model.AktifKayit.Durum = (short)Durum.Aktif;
            return View("Edit", Model);
        }
    }
}
