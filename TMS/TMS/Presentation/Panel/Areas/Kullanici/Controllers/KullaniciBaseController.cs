﻿using TMS.Panel.Web.Code.Security;
using System.Collections.Generic;
using System.Linq;

namespace TMS.Panel.Web.Areas.Kullanici.Controllers
{
    [CustomAuthorize(Roles = "Kullanici")]
    public class KullaniciBaseController : BaseController
    {
        private readonly Core.Service.KullaniciService KullaniciService = new Core.Service.KullaniciService();

        public Core.Data.Kullanici AktifKullaniciGetir(int UserId = 0)
        {
            if (UserId == 0)
            {
                UserId = User.UserId;
            }
            Core.Data.Kullanici Sonuc = new Core.Data.Kullanici();
            Sonuc = KullaniciService.Get(x => x.UserNo == UserId);
            return Sonuc;
        }
    }
}