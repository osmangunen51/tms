﻿using TMS.View.Genel;
using System.Web.Mvc;

namespace TMS.Panel.Web.Areas.Kullanici.Controllers
{
    public class FileManagerController : Controller
    {
        public virtual ActionResult Index()
        {
            TMS.Panel.Web.Code.Security.CustomPrincipal Usr = (User as TMS.Panel.Web.Code.Security.CustomPrincipal);
            FileViewModel model = new FileViewModel() { Folder = Usr.Folder, SubFolder = Usr.SubFolder };
            return View(model);
        }
    }
}