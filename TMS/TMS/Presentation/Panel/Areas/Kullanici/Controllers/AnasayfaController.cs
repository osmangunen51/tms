﻿using TMS.Core.Service;
using TMS.Panel.Web.Code;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;

namespace TMS.Panel.Web.Areas.Kullanici.Controllers
{
    public class AnasayfaController : KullaniciBaseController
    {
        private UsersService UsersService = new UsersService();

        private UserBildirimService UserBildirimService = new UserBildirimService();
        private UserOneriService UserOneriService = new UserOneriService();
        private TokenService TokenService = new TokenService();
        private SiparisService SiparisService = new SiparisService();

        [Compress]
        [PageGuide]
        public ActionResult Index()
        {
            SifreDegistirmeLayoutAyarla("~/Areas/Kullanici/Views/Shared/_Layout.cshtml");
            TMS.View.Kullanici.Anasayfa Model = new TMS.View.Kullanici.Anasayfa();
            Model.User = UsersService.Get(x => x.UserId == User.UserId);
            Model.UserBildirimListesi = UserBildirimService.UserGelenBildirimListesiGetir(User.UserId).Where(x => !x.UserBildirim.OkunmaDurum).ToList();
            Model.UserOneriListesi = UserOneriService.UserGelenOneriListesiGetir(User.UserId);
            Model.UserLogListesi = LogService.GetMany(x => x.UserNo == User.UserId && !((int)x.Tip == (int)TMS.View.Genel.LogTip.Giris || (int)x.Tip == (int)TMS.View.Genel.LogTip.Cikis)).OrderByDescending(x => x.Date).Take(5).ToList();
            var TokenListesi = TokenService.GetMany(x => x.UserNo == this.User.UserId);
            Model.TokenSayisi = TokenListesi.Count();
            List<int> TokenNoListesi = TokenListesi.Select(x => x.No).Distinct().ToList();
            Model.SiparisSayisi = SiparisService.GetMany(x => TokenNoListesi.Contains((int)x.KaynakTokenNo)).Count();
            return View(Model);
        }


        [Compress]
        public ActionResult YapimAsamasinda()
        {
            return View();
        }
    }
}