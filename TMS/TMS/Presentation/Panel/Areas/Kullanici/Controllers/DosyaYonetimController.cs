﻿using TMS.Core.Lib;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
namespace TMS.Panel.Web.Areas.Kullanici.Controllers
{
    public class DosyaYonetimController : KullaniciBaseController
    {
        [HttpPost]
        public ActionResult DosyaYukle()
        {
            TMS.Core.Lib.IslemDurum IslemSonuc = new Core.Lib.IslemDurum() { Durum = false, Deger = null, Tip = Core.Lib.IslemDurum.IslemMesajTip.Uyari, Hata = null, Mesaj = "" };
            string DizinUrlPath = "~/Content/Dosya/";
            string DizinPath = Server.MapPath(DizinUrlPath);
            DirectoryInfo DizinBilgi = new DirectoryInfo(DizinPath);
            if (!DizinBilgi.Exists) { DizinBilgi.Create(); }
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname;
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        //fname = string.Format("Dosya_{0}_{1}", DateTime.Now.ToString().Replace(" ", "___").Replace(".", "_").Replace(":", "__"), fname);
                        string ServerFileName = $"~/Content/Dosya/User/{User.UserId}/{Guid.NewGuid().ToString()}_{fname.TrkToEng().Replace(" ", "").ToLower()}";
                        string ServerFile = Server.MapPath(ServerFileName);
                        var FileBilgi = new System.IO.FileInfo(ServerFile);
                        if (!FileBilgi.Directory.Exists)
                        {
                            FileBilgi.Directory.Create();
                        }
                        file.SaveAs(ServerFile);
                        IslemSonuc.Durum = true;
                        IslemSonuc.Mesaj = "Dosya Başarıyla Yüklendi.";
                        IslemSonuc.Tip = Core.Lib.IslemDurum.IslemMesajTip.Basarili;
                        IslemSonuc.Deger = ServerFileName;
                    }
                }
                catch (Exception Hata)
                {
                    IslemSonuc.Durum = true;
                    IslemSonuc.Mesaj = "Dosya Yüklenemedi Hata Oluştu. Ayrıntılar : " + Hata.Message;
                    IslemSonuc.Tip = Core.Lib.IslemDurum.IslemMesajTip.Hata;
                    IslemSonuc.Deger = null;
                    IslemSonuc.Hata = Hata;
                }
            }
            else
            {
                IslemSonuc.Durum = true;
                IslemSonuc.Mesaj = "Dosya Yok.";
                IslemSonuc.Tip = Core.Lib.IslemDurum.IslemMesajTip.Hata;
                IslemSonuc.Deger = null;
                IslemSonuc.Hata = null;
            }
            return Json(IslemSonuc, JsonRequestBehavior.AllowGet);
        }
    }
}