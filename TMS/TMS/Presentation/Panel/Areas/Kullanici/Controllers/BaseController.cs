﻿using TMS.Core.Service;
using TMS.Panel.Web.Code.Security;
using TMS.View.Genel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace TMS.Panel.Web.Areas.Kullanici.Controllers
{
    [ProfilFilter]
    public class BaseController : TMS.Panel.Web.Controllers.BaseController
    {

        public Core.Service.IlService IlService { get; set; } = new Core.Service.IlService();

        private UserBildirimService UserBildirimService = new UserBildirimService();
        private UsersService UsersService = new UsersService();
        private KullaniciService KullaniciService = new KullaniciService();



        protected virtual new CustomPrincipal User
        {
            get { return HttpContext.User as CustomPrincipal; }
        }
        public string MesajBaslikText { get; set; } = "";
        public string MesajGuncellemeText { get; set; } = "";
        public string MesajEklemeText { get; set; } = "";

        public string MesajSilText { get; set; } = "";
        public string MesajBulunamadiText { get; set; } = "";
        public string MesajHataText { get; set; } = ".";

        public string MesajZatenKayitliText { get; set; } = "";

        public List<Select2Item> OgrenmeAlaniListesi { get; set; }

        public BaseController()
        {

        }

        public JsonResult IlEkle(string Ifade)
        {
            Select2Item Sonuc = new Select2Item();
            if (!string.IsNullOrEmpty(Ifade))
            {
                var Nesne = IlService.Get(x => x.Ad == Ifade);
                if (Nesne == null)
                {
                    Nesne = new Core.Data.Il()
                    {
                        Ad = Ifade
                    };
                    IlService.Create(Nesne);
                    IlService.Commit();
                }
                Sonuc.id = Nesne.No;
                Sonuc.text = Nesne.Ad;
            }
            return Json(new { Sonuc }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IlDegerGetir(string id)
        {
            Select2Item Sonuc = new Select2Item();
            if (!string.IsNullOrEmpty(id))
            {
                int No = System.Convert.ToInt32(id);
                var Nesne = IlService.Get(x => x.No == No);
                if (Nesne != null)
                {
                    Sonuc.id = Nesne.No;
                    Sonuc.text = Nesne.Ad;
                }
            }
            return Json(new { Sonuc }, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult IlListesiGetir(string term)
        {
            List<Select2Item> Sonuc = IlService.GetMany(m => (term == "" || m.Ad.ToLower().Contains(term.ToLower()))).Select(Snc =>
                    new Select2Item()
                    {
                        id = Snc.No,
                        text = Snc.Ad
                    }
                ).ToList();
            return Json(new { Sonuc }, JsonRequestBehavior.AllowGet);
        }
    }
}
