﻿using TMS.Core.Service;
using TMS.Panel.Web.Code;
using TMS.View.Genel;
using System;
using System.Web.Mvc;
namespace TMS.Panel.Web.Areas.Kullanici.Controllers
{
    public class UserBildirimController : KullaniciBaseController
    {
        private UserBildirimService UserBildirimService = new UserBildirimService();
        [Compress]
        public ActionResult Goruntule(int Id)
        {
            if (!string.IsNullOrEmpty(Id.ToString()))
            {
                TMS.View.Genel.UserBildirim Model = new TMS.View.Genel.UserBildirim();
                Model.AktifKayit = UserBildirimService.GetById(Id);
                if (Model.AktifKayit.HedefUsersNo != User.UserId)
                {
                    return RedirectToAction("Index", "Anasayfa");
                }
                Model.AktifKayit.OkunmaDurum = true;
                UserBildirimService.Update(Model.AktifKayit);
                UserBildirimService.Commit();
                return View(Model);
            }
            else
            {
                return View("Index");
            }
        }
        [Compress]
        public ActionResult Index()
        {
            TMS.View.Genel.UserBildirim Model = new TMS.View.Genel.UserBildirim();
            Model.UserBildirimListesi = UserBildirimService.UserGelenBildirimListesiGetir(User.UserId);
            return View(Model);
        }
        [HttpPost]
        public JsonResult OkunmaDurumDegistir(int UserBildirimNo)
        {
            TMS.Core.Lib.IslemDurum Sonuc = new TMS.Core.Lib.IslemDurum();
            var UserBildirim = UserBildirimService.GetById(UserBildirimNo);
            if (UserBildirim.HedefUsersNo == User.UserId)
            {
                try
                {
                    UserBildirim.OkunmaDurum = !UserBildirim.OkunmaDurum;
                    UserBildirimService.Update(UserBildirim);
                    UserBildirimService.Commit();
                    Sonuc.Durum = true;
                    Sonuc.Deger = UserBildirim.OkunmaDurum;
                    Sonuc.Mesaj = "Başarıyla Tamamlandı.";
                    Sonuc.Tip = TMS.Core.Lib.IslemDurum.IslemMesajTip.Basarili;
                }
                catch (Exception Hata)
                {
                    Sonuc.Hata = Hata;
                    Sonuc.Durum = false;
                    Sonuc.Deger = UserBildirim;
                    Sonuc.Mesaj = "Genel Bir Hata Oluştu.";
                    Sonuc.Tip = TMS.Core.Lib.IslemDurum.IslemMesajTip.Hata;
                }
            }
            else
            {
                Sonuc.Durum = false;
                Sonuc.Deger = UserBildirim;
                Sonuc.Mesaj = "Bu bildirim size ait değil.";
                Sonuc.Tip = TMS.Core.Lib.IslemDurum.IslemMesajTip.Uyari;
            }
            return Json(Sonuc, JsonRequestBehavior.AllowGet);
        }
    }
}
