﻿using Newtonsoft.Json;
using TMS.Core.Service;
using TMS.Panel.Web.Code;
using TMS.Panel.Web.Code.Security;
using TMS.View.Genel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace TMS.Panel.Web.Areas.Kullanici.Controllers
{
    public class UsersController : KullaniciBaseController
    {
        private readonly UsersService UsersService = new UsersService();
        private RolesService RolesService = new RolesService();
        private UserRolesService UserRolesService = new UserRolesService();
        public UsersController()
        {
        }

        [Compress]
        public ActionResult Profil(int Id)
        {
            TMS.View.Genel.Profil Model = new TMS.View.Genel.Profil();
            Model.AktifKayit = UsersService.GetById(Id);
            return View(Model);
        }


        [HttpPost]
        [ValidateInput(false)]
        [Compress]
        public ActionResult Profil(TMS.View.Genel.Profil Model)
        {
            switch (Request.Form["BtnIslem"])
            {
                case "Kaydet":
                    {
                        ModelState.Remove("AktifKayit.Password");
                        if (ModelState.IsValid)
                        {
                            try
                            {
                                Model.IslemMesaj.Visible = true;
                                Model.IslemMesaj.Tip = IslemMesajTip.Basarili;
                                Model.IslemMesaj.Baslik = "User Profil İşlemleri";
                                Core.Data.Users Nesne = UsersService.GetById(Model.AktifKayit.UserId);
                                if (Nesne != null)
                                {
                                    Nesne.FirstName = Model.AktifKayit.FirstName;
                                    Nesne.LastName = Model.AktifKayit.LastName;
                                    Nesne.Email = Model.AktifKayit.Email;
                                    UsersService.Update(Nesne);
                                    Model.IslemMesaj.Mesaj = "User Profili Başarıyla Güncellendi.";
                                    UsersService.Commit();
                                    Model.AktifKayit = Nesne;

                                    List<TMS.Core.Data.Roles> RolListesi = UsersService.RolListesiGetir(Model.AktifKayit.UserId);
                                    CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
                                    serializeModel.UserId = Model.AktifKayit.UserId;
                                    serializeModel.FirstName = Model.AktifKayit.FirstName;
                                    serializeModel.LastName = Model.AktifKayit.LastName;
                                    serializeModel.roles = RolListesi.Select(x => x.RoleName).ToArray();
                                    serializeModel.Image = Model.AktifKayit.Image;
                                    serializeModel.Telefon = Model.AktifKayit.Telefon;
                                    serializeModel.UserName = Model.AktifKayit.Username;
                                    string userData = JsonConvert.SerializeObject(serializeModel);
                                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                                             1,
                                             Model.AktifKayit.Email,
                                             DateTime.Now,
                                             DateTime.Now.AddMinutes(15),
                                             false,
                                             userData);
                                    string encTicket = FormsAuthentication.Encrypt(authTicket);
                                    HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                                    Response.Cookies.Add(faCookie);

                                }
                                Response.Redirect("~/Kullanici/Anasayfa");
                                return View("Index", Model);
                            }
                            catch (Exception ex)
                            {
                                Model.IslemMesaj.Visible = true;
                                Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                                Model.IslemMesaj.Baslik = "User Profil İşlemleri";
                                Model.IslemMesaj.Mesaj = "Hata! " + ex.Message;
                                return View(Model);
                            }
                        }
                        break;
                    }
                case "İptal Et":
                    {
                        Response.Redirect(TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl(string.Format("~/Kullanici/Anasayfa/Index")));
                        return View("Index", Model);
                    }
                default:
                    break;
            }
            return View(Model);
        }
    }
}
