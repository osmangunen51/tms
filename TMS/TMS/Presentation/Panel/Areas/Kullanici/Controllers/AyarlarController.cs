﻿using TMS.Core.Lib;
using TMS.Core.Service;
using TMS.View.Genel;
using System.Web.Mvc;

namespace TMS.Panel.Web.Areas.Kullanici.Controllers
{
    public class AyarlarController : KullaniciBaseController
    {
        private readonly UsersService UsersService = new UsersService();
        private readonly KullaniciService KullaniciService = new KullaniciService();
        public AyarlarController()
        {
            MesajBaslikText = "Ayarlar İşlemleri";
            MesajGuncellemeText = "Ayarlar Başarıyla Güncellendi";
            MesajEklemeText = "Ayarlar Başarıyla Eklendi.";
            MesajSilText = "Ayarlar Başarıyla Silindi.";
            MesajBulunamadiText = "Ayarlar Başarıyla Silindi.";
            MesajHataText = "Hata Oluştu.";
            MesajZatenKayitliText = "Bu Ayarlar Zaten Kayıtlı";
        }
    }
}