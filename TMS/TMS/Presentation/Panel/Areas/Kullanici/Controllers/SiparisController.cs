﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using TMS.Core.Lib;
using TMS.Core.Service;
using TMS.View.Genel;
namespace TMS.Panel.Web.Areas.Kullanici.Controllers
{
    public class SiparisController : KullaniciBaseController
    {
        private SiparisService SiparisService = new SiparisService();
        private TokenService TokenService = new TokenService();

        public SiparisController()
        {

        }
        public ActionResult Index()
        {
            TMS.View.Kullanici.Siparis Model = new TMS.View.Kullanici.Siparis();
            Model.AktifKayit = new TMS.Core.Data.Siparis();
            Model.SatisListesi = SiparisService.SatisListesiGetir(this.User.UserId);
            return View(Model);
        }

        [HttpPost]
        public ActionResult Index(TMS.View.Kullanici.Siparis Model, FormCollection Frm)
        {
            Model.SatisListesi = SiparisService.SatisListesiGetir(this.User.UserId);
            return View(Model);
        }

        public ActionResult Delete(int id)
        {
            TMS.View.Kullanici.Siparis Model = new TMS.View.Kullanici.Siparis();
            var Aktif = SiparisService.GetById(id);
            if (Aktif != null)
            {
                var Token = TokenService.GetById((int)Aktif.KaynakTokenNo);
                if (Token.UserNo != this.User.UserId)
                {
                    Response.Redirect(TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl("~/Kullanici/Siparis"), true);
                    return RedirectToAction("Index", "Siparis", Model);
                }
                try
                {
                    SiparisService.Delete(Aktif);
                    SiparisService.Commit();
                    Model.IslemMesaj.Visible = true;
                    Model.IslemMesaj.Tip = IslemMesajTip.Basarili;
                    Model.IslemMesaj.Baslik = "Silme İşlemleri";
                    Model.IslemMesaj.Mesaj = "Başarıyla Silindi.";

                }
                catch (Exception)
                {
                    Model.IslemMesaj.Visible = true;
                    Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                    Model.IslemMesaj.Baslik = "Silme İşlemleri";
                    Model.IslemMesaj.Mesaj = "Hata Oluştu.";
                }
            }
            else
            {
                Model.IslemMesaj.Visible = true;
                Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                Model.IslemMesaj.Baslik = "Silme İşlemleri";
                Model.IslemMesaj.Mesaj = "Bulunamadı";
            }
            return RedirectToAction("Index", "Siparis", Model);
        }

        public ActionResult Durdur(int id)
        {
            TMS.View.Kullanici.Siparis Model = new TMS.View.Kullanici.Siparis();
            var Aktif = SiparisService.GetById(id);
            if (Aktif != null)
            {
                var Token = TokenService.GetById((int)Aktif.KaynakTokenNo);
                if (Token.UserNo!=this.User.UserId)
                {
                    Response.Redirect(TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl("~/Kullanici/Siparis"), true);
                    return RedirectToAction("Index", "Siparis", Model);
                }
                try
                {

                    string BaglantiTxt = ConfigurationManager.ConnectionStrings["EntitiesMysql"].ToString();
                    List<TMS.View.Genel.SiparisListItem> SiparisListesi = SiparisService.TumSiparisSatisListesiGetir().ToList();
                    using (TMS.Core.Mysql.Data.Istemci Istemci = new TMS.Core.Mysql.Data.Istemci(BaglantiTxt))
                    {
                        Dictionary<string, object> PrmListesi = new Dictionary<string, object>();
                        PrmListesi.Add("@token", Aktif.HedefTokenNo);
                        PrmListesi.Add("@durum", "5");
                        TMS.Core.Mysql.Data.IslemDurum SorguIslem = Istemci.KomutCalistirWithNonQuery(@"
                        Update Siparis
                        Set Siparis.durum=@durum
                        where 
                        Siparis.token=@token",
                        PrmListesi);
                        if (SorguIslem.Durum)
                        {
                            Aktif.Durum = 5;
                            SiparisService.Update(Aktif);
                            SiparisService.Commit();

                            Model.IslemMesaj.Visible = true;
                            Model.IslemMesaj.Tip = IslemMesajTip.Basarili;
                            Model.IslemMesaj.Baslik = "Sipariş İşlemleri";
                            Model.IslemMesaj.Mesaj = "Başarıyla Durduruldu.";
                        }
                        else
                        {
                            Model.IslemMesaj.Visible = true;
                            Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                            Model.IslemMesaj.Baslik = "Sipariş İşlemleri";
                            Model.IslemMesaj.Mesaj = SorguIslem.Mesaj;
                        }

                    }

                }
                catch (Exception Hata)
                {
                    Model.IslemMesaj.Visible = true;
                    Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                    Model.IslemMesaj.Baslik = "Sipariş İşlemleri";
                    Model.IslemMesaj.Mesaj = "Hata Oluştu.";
                }
            }
            else
            {
                Model.IslemMesaj.Visible = true;
                Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                Model.IslemMesaj.Baslik = "Sipariş İşlemleri";
                Model.IslemMesaj.Mesaj = "Bulunamadı";
            }
            Model.SatisListesi = SiparisService.SatisListesiGetir(this.User.UserId);
            return View("Index",Model);
        }

        public ActionResult Edit(int id)
        {
            TMS.View.Kullanici.Siparis Model = new TMS.View.Kullanici.Siparis();
            Model.AktifKayit = SiparisService.GetById(id);
            var Token = TokenService.GetById((int)Model.AktifKayit.KaynakTokenNo);
            if (Token.UserNo != this.User.UserId)
            {
                Response.Redirect(TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl("~/Kullanici/Siparis"), true);
                return RedirectToAction("Index", "Siparis", Model);
            }

            List<TokenListItem> TokenListesi = TokenService.TokenListesiGetir();
            Model.KaynakTokenListesi = TokenListesi.Where(x => x.Users.UserId == this.User.UserId).ToList();
            return View(Model);
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(TMS.View.Kullanici.Siparis Model, FormCollection Frm)
        {
            switch (Request.Form["BtnIslem"])
            {
                case "Kaydet":
                    {
                        if (ModelState.IsValid)
                        {
                            try
                            {
                                decimal TokenKalanMiktar = 0;
                                var Token = TokenService.GetById((int)Model.AktifKayit.KaynakTokenNo);
                                if (Token != null)
                                {
                                    var SiparisListesi = SiparisService.GetMany(x => x.KaynakTokenNo == Model.AktifKayit.KaynakTokenNo);
                                    decimal TokenSiparisToplamTutar = SiparisListesi.Sum(x => (decimal)x.Miktar);
                                    TokenKalanMiktar =(decimal)(Token.Miktar - TokenSiparisToplamTutar);
                                    TokenKalanMiktar = (decimal)(TokenKalanMiktar - Model.AktifKayit.Miktar);
                                }

                                Model.AktifKayit.TokenKalanMiktar = TokenKalanMiktar;
                                Model.IslemMesaj.Visible = true;
                                Model.IslemMesaj.Tip = IslemMesajTip.Basarili;
                                Model.IslemMesaj.Baslik = "Siparis İşlemleri";
                                if (Model.AktifKayit.No == 0)
                                {
                                    Model.AktifKayit.Durum = (short)Durum.Pasif;
                                    Model.AktifKayit.Tarih = DateTime.Now;
                                    SiparisService.Create(Model.AktifKayit);
                                    Model.IslemMesaj.Mesaj = "Siparis Başarıyla Eklendi.";

                                }
                                else
                                {
                                    var Nesne = SiparisService.GetById(Model.AktifKayit.No);
                                    Nesne.KaynakTokenNo = Model.AktifKayit.KaynakTokenNo;
                                    Nesne.HedefTokenNo = Model.AktifKayit.HedefTokenNo;
                                    Nesne.Miktar = Model.AktifKayit.Miktar;
                                    Nesne.HedefTokenAliasName = Model.AktifKayit.HedefTokenAliasName;
                                    Nesne.Durum = (short)Durum.Pasif;
                                    SiparisService.Update(Nesne);
                                    Model.IslemMesaj.Mesaj = "Siparis Başarıyla Güncellendi.";
                                }
                                SiparisService.Commit();
                                Model.SatisListesi = SiparisService.SatisListesiGetir(this.User.UserId);
                                return RedirectToAction("Index", "Siparis", Model);

                            }
                            catch (Exception ex)
                            {
                                Model.IslemMesaj.Visible = true;
                                Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                                Model.IslemMesaj.Baslik = "Siparis İşlemleri";
                                Model.IslemMesaj.Mesaj = "Hata! " + ex.Message;
                                return View(Model);
                            }
                        }
                        else
                        {
                            Model.IslemMesaj.Visible = true;
                            Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                            Model.IslemMesaj.Baslik = "Siparis İşlemleri";
                            Model.IslemMesaj.Mesaj = "Hatalar :<br>" + ModelState.HataListesi().ListToString("<br>");
                            return View(Model);
                        }
                    }
                case "İptal Et":
                    {
                        Model.SatisListesi = SiparisService.SatisListesiGetir(this.User.UserId);
                        return RedirectToAction("Index", "Siparis", Model);
                    }
                default:
                    break;
            }
            List<TokenListItem> TokenListesi = TokenService.TokenListesiGetir();
            Model.KaynakTokenListesi = TokenListesi.Where(x => x.Users.UserId == this.User.UserId).ToList();
            return View(Model);
        }
        public ActionResult Create(int id=0)
        {
            if (id>0)
            {
                var Token = TokenService.GetById(id);
                if (Token == null)
                {
                    id = 0;
                }
                else
                {
                    if (Token.UserNo!=this.User.UserId)
                    {
                        Response.Redirect(TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl("~/Kullanici/Token"),true);
                    }
                }
            }

            TMS.View.Kullanici.Siparis Model = new TMS.View.Kullanici.Siparis();
            Model.AktifKayit = new TMS.Core.Data.Siparis();
            Model.AktifKayit.Tarih = DateTime.Now;
            Model.AktifKayit.Durum = (short)Durum.Aktif;
            List<TokenListItem> TokenListesi = TokenService.TokenListesiGetir(0);
            Model.KaynakTokenListesi=TokenListesi.Where(x => x.Users.UserId == this.User.UserId).ToList();
            Model.AktifKayit.KaynakTokenNo = id;
            Model.AktifKayit.HedefTokenNo ="";
            return View("Edit", Model);
        }

        [HttpGet]
        public ActionResult BilgiGuncelle(int Id)
        {
            TMS.Core.Lib.IslemDurum IslemSonuc = new Core.Lib.IslemDurum() { Durum = false, Deger = null, Tip = Core.Lib.IslemDurum.IslemMesajTip.Uyari, Hata = null, Mesaj = "" };
            try
            {
                var SiparisSatis = SiparisService.SatisListesiGetirWithSiparisNo(Id);
                IslemSonuc.Durum = true;
                IslemSonuc.Mesaj = "İşlem Başarılı";
                IslemSonuc.Tip = Core.Lib.IslemDurum.IslemMesajTip.Hata;
                IslemSonuc.Deger = SiparisSatis;
            }
            catch (Exception Hata)
            {
                IslemSonuc.Durum = false;
                IslemSonuc.Mesaj = "Dosya Yüklenemedi Hata Oluştu. Ayrıntılar : " + Hata.Message;
                IslemSonuc.Tip = Core.Lib.IslemDurum.IslemMesajTip.Hata;
                IslemSonuc.Deger = null;
                IslemSonuc.Hata = Hata;
            }
            return Json(IslemSonuc, JsonRequestBehavior.AllowGet);
        }


    }
}
