﻿using TMS.Core.Service;
using System;
using System.Web;
using System.Web.Mvc;
namespace TMS.Panel.Web.Areas.Kullanici
{
    public class ProfilFilter : ActionFilterAttribute
    {
        private UsersService UsersService = new UsersService();
        private KullaniciService KullaniciService = new KullaniciService();
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            base.OnActionExecuting(filterContext);
        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }
    }
}