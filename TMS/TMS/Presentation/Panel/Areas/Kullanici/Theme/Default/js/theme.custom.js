﻿(function (theme, $) {
    'use strict';

    if (typeof (window.localStorage) === "undefined") return false;

    var closed = localStorage.getItem('sidebar-left-closed'),
        $window = $(window);

    $window.on('sidebar-left-toggle', function (e, opt) {
        localStorage.setItem('sidebar-left-closed', opt.added);
    });


    if (closed === 'true') {
        $('html').addClass('sidebar-left-collapsed').removeClass('sidebar-left-closed');
    }
}).apply(this, [window.theme, jQuery]);

var ResimEkle = function (context) {
    var ui = $.summernote.ui;
    var button = ui.button({
        contents: '<i title="Resim Ekle" class="fa fa-picture-o"/>',
        click: function (e) {
            EditorAcResimEkle(context);
        }
    });
    return button.render();
}
var LinkEkle = function (context) {
    var ui = $.summernote.ui;
    var button = ui.button({
        contents: '<i title="Dosya Ekle" class="fa fa-file"/>',
        //tooltip: 'Dosya Ekle',
        click: function (e) {
            EditorAcLinkEkle(context);
        }
    });
    return button.render();
}
var VideoEkle = function (context) {
    var ui = $.summernote.ui;
    var button = ui.button({
        contents: '<i class="fa fa-youtube-play"/>',
        //tooltip: 'Video Ekle',
        click: function (e) {
            EditorAcVideoEkle(context);
        }
    });
    return button.render();
}
var AuidoEkle = function (context) {
    var ui = $.summernote.ui;
    var button = ui.button({
        contents: '<i class="fa fa-file-audio-o"/>',
        //tooltip: 'Ses Ekle',
        click: function (e) {
            EditorAcAudioEkle(context);
        }
    });
    return button.render();
}
function EditorAcAudioEkle(editor) {
    var Folder = editor.data("userelfinderfolder");
    var subFolder = editor.data("userelfindersubfolder");
    if (Folder === null) {
        Folder = "Content";
    }
    if (subFolder === null) {
        subFolder = "";
    }
    $('<div />').dialogelfinder({
        url: '/connector',
        customData: { folder: Folder, subFolder: subFolder },
        rememberLastDir: false,
        lang: 'tr',
        commandsOptions: {
            getfile: {
                oncomplete: 'destroy' // destroy elFinder after file selection
            }
        },
        getFileCallback: function (File) {
            var url = File.path.toString();
            url = url.replace('Files', File.baseUrl);
            var Html = '<p><audio controls><source src="' + url + '" type="audio/mpeg"></audio></p><p></p>'
            editor.summernote('pasteHTML', Html);
        }
    });
}
function EditorAcResimEkle(editor) {
    var Folder = editor.data("userelfinderfolder");
    var subFolder = editor.data("userelfindersubfolder");
    if (Folder === null) {
        Folder = "Content";
    }
    if (subFolder === null) {
        subFolder = "";
    }
    $('<div />').dialogelfinder({
        url: '/connector',
        customData: { folder: Folder, subFolder: subFolder },
        rememberLastDir: false,
        lang: 'tr',
        commandsOptions: {
            getfile: {
                oncomplete: 'destroy' // destroy elFinder after file selection
            }
        },
        getFileCallback: function (File) {
            var url = File.path.toString();
            url = url.replace('Files', File.baseUrl);
            console.log(url);
            editor.summernote('editor.insertImage', url, function ($image) {
                $image.css('width', $image.width());
                $image.attr('class', "img-responsive");
            });
        }
    });
}
function EditorAcVideoEkle(editor) {
    var Folder = editor.data("userelfinderfolder");
    var subFolder = editor.data("userelfindersubfolder");
    if (Folder === null) {
        Folder = "Content";
    }
    if (subFolder === null) {
        subFolder = "";
    }
    $('<div />').dialogelfinder({
        url: '/connector',
        customData: { folder: Folder, subFolder: subFolder },
        rememberLastDir: false,
        lang: 'tr',
        commandsOptions: {
            getfile: {
                oncomplete: 'destroy' // destroy elFinder after file selection
            }
        },
        getFileCallback: function (File) {
            var url = File.path.toString();
            url = url.replace('Files', File.baseUrl);
            var node = document.createElement('div');
            var html = '<video width="100%" height="400" controls="controls"><source src="' + url + '" type= "video/mp4" autostart= "true"></video>';
            editor.summernote.invoke('editor.insertText', html);
        }
    });
}
function EditorAcLinkEkle(editor) {
    var Folder = editor.data("userelfinderfolder");
    var subFolder = editor.data("userelfindersubfolder");
    if (Folder === null) {
        Folder = "Content";
    }
    if (subFolder === null) {
        subFolder = "";
    }

    $('<div />').dialogelfinder({
        url: '/connector',
        customData: { folder: Folder, subFolder: subFolder },
        rememberLastDir: false,
        lang: 'tr',
        commandsOptions: {
            getfile: {
                oncomplete: 'destroy' // destroy elFinder after file selection
            }
        },
        getFileCallback: function (File) {
            var url = File.path.toString();
            url = url.replace('Files', File.baseUrl);
            editor.summernote('createLink',
                {
                    text: File.name,
                    url: url,
                    newWindow: true
                }
            );
        }
    });
}
$(".summernote").each(function (Index) {
    var Editor = $(this);
    var yukseklik = $(this).data("yukseklik");
    if (yukseklik === "undefined" | yukseklik === "") {
        yukseklik = 200;
    };

    Editor.summernote({
        lang:'tr-TR',
        height: yukseklik,
        tableClassName: 'table table-striped',
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']],
            ['ResimEkle', ['ResimEkle']],
            ['LinkEkle', ['LinkEkle']],
            ['VideoEkle', ['VideoEkle']],
            ['AuidoEkle', ['AuidoEkle']]
        ],
        buttons: {
            ResimEkle: ResimEkle(Editor),
            LinkEkle: LinkEkle(Editor),
            VideoEkle: VideoEkle(Editor),
            AuidoEkle: AuidoEkle(Editor)
        }
    });
});
(function ($) {
    var GetUrlBilgi = function (href) {
        var l = document.createElement("a");
        l.href = href;
        return l;
    };
    if (typeof GeriSayimZaman === "undefined") {
        var Tmp = 1;
    }
    else {
        $('#GeriSayim').countdown(GeriSayimZaman)
            .on('update.countdown', function (event) {
                var $this = $(this);
                $(this).html(event.strftime(''
                    + '<span class="btn btn-info"><strong>%M</strong></span> Dk. '
                    + '<span class="btn btn-success btn-sm"><strong>%S</strong></span> Sn.'));
            }
            ).on('finish.countdown', function (event) {
                window.open(GeriSayimUrl, "_self")
            }
            );
    }
}).apply(this, [jQuery]);
$(window).load(function () {
    $(".se-pre-con").fadeOut("slow");
});
// Tooltip
(function ($) {
    $('.jstreeListe').jstree({
        'core': {
            'themes': {
                'name': 'proton',
                'responsive': true
            }
        }
    });
    $(".jstreeListe").bind(
        "select_node.jstree", function (evt, data) {
            var NesneId = data.node.li_attr["data-nesne"];
            var HedefNesne = $('#' + NesneId);
            console.log(HedefNesne);
            if (HedefNesne !== null) {
                var Deger = data.node.li_attr["data-deger"];
                HedefNesne.val(Deger);
            }
        }
    )
        .on('changed.jstree', function (e, data) {
            var path = data.instance.get_path(data.node, '/');
            console.log('Selected: ' + path);
        });
}).apply(this, [jQuery]);

$('.dropdown-menu ul li').click(function (e) {
});
$('.OpenDropDownMenu').on({
    "click": function (e) {
        e.stopPropagation();
    }
});
// Tooltip
(function ($) {
    $('.datatabkel').jstree({
        'core': {
            'themes': {
                'name': 'proton',
                'responsive': true
            }
        }
    });
    $(".jstreeListe").bind(
        "select_node.jstree", function (evt, data) {
            var NesneId = data.node.li_attr["data-nesne"];
            var HedefNesne = $('#' + NesneId);
            console.log(HedefNesne);
            if (HedefNesne !== null) {
                var Deger = data.node.li_attr["data-deger"];
                HedefNesne.val(Deger);
            }
        }
    )
        .on('changed.jstree', function (e, data) {
            var path = data.instance.get_path(data.node, '/');
            console.log('Selected: ' + path);
        });
}).apply(this, [jQuery]);

function OzelArama(filterVal, columnVal) {
    var found;
    if (columnVal === '') {
        return true;
    }
    switch (filterVal) {
        case 'happy':
            found = columnVal.search(/:-\]|:\)|Happy|JOY|:D/g);
            break;
        case 'sad':
            found = columnVal.search(/:\(|Sad|:'\(/g);
            break;
        case 'angry':
            found = columnVal.search(/!!!|Arr\.\.\./g);
            break;
        case 'lucky':
            found = columnVal.search(/777|Bingo/g);
            break;
        case 'january':
            found = columnVal.search(/01|Jan/g);
            break;
        default:
            found = 1;
            break;
    }

    if (found !== -1) {
        return true;
    }
    return false;
}

function TabloToDataTable(Tablo, DisplaySize, SortColumnIndex = 0, SortColumnDirection = "asc", isordering = true) {
  var tbl= $(Tablo).dataTable(
        {
          destroy: true,
          "ordering": isordering,
            "order": [[SortColumnIndex, SortColumnDirection]],
            "stateSave": true,
            "search": {
                "caseInsensitive": true
            },
            "iDisplayLength": DisplaySize,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Turkish.json",
                "sSearch": "Ara"
            },
            "lengthMenu": [[5,10,50, 100, 250, 500, -1], [5,10,50, 100, 250, 500, "Tümü"]]
        }
    );
}

function TabloToDataFiltreTable(Tablo, DisplaySize) {

    var table = $(Tablo).dataTable(
        {
            destroy: true,
            "stateSave": true,
            "search": {
                "caseInsensitive": true
            },
            "iDisplayLength": DisplaySize,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Turkish.json",
                "sSearch": "Ara"
            },
            "lengthMenu": [[5,10, 50, 100, 250, 500, -1], [5,10, 50, 100, 250, 500, "Tümü"]]
        }
    );

    $('#' + $(Tablo).attr("id") + ' .arm').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text"');
    });

    table.len(DisplaySize).draw();

}

function AreaTextSummerNote(Tablo) {
    var Editor = $(Tablo);
    Editor.summernote({
        height: 400,
        tableClassName: 'table table-striped ',
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            //['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']],
            ['ResimEkle', ['ResimEkle']],
            ['LinkEkle', ['LinkEkle']],
            ['VideoEkle', ['VideoEkle']],
            ['AuidoEkle', ['AuidoEkle']]
        ],
        buttons: {
            ResimEkle: ResimEkle(Editor),
            LinkEkle: LinkEkle(Editor),
            VideoEkle: VideoEkle(Editor),
            AuidoEkle: AuidoEkle(Editor)
        }
    });
}

(function ($) {
    var clipboardcopy = new Clipboard('.kopyalakes');
    clipboardcopy.on('success', function (e) {
        e.clearSelection();
    });
}).apply(this, [jQuery]);

// Tooltip
(function ($) {
    $(".DtTable").each(function (index) {
        var dipslaysize = 0;
        var hedef = "#" + $(this).attr("id");
        var displaysize = $(this).data("displaysize");
        if (typeof (displaysize) === 'undefined') displaysize = 5;
        var sortcolumnindex = $(this).data("sortcolumnindex");
        var sortdirection = $(this).data("sortdirection");
        if (typeof (sortcolumnindex) === 'undefined') sortcolumnindex = 0;
        if (typeof (sortdirection) === 'undefined') sortdirection = "asc";
        var ordering = $(this).data("ordering");
        if (typeof (ordering) === 'undefined') ordering = true;
        TabloToDataTable(hedef, displaysize, sortcolumnindex, sortdirection, ordering);
    });
}).apply(this, [jQuery]);

(function ($) {
    $(".DtTableFiltre").each(function (index) {
        var dipslaysize = 0;
        var hedef = "#" + $(this).attr("id");
        var displaysize =$(this).attr("data-displaysize");
        if (typeof (displaysize) === 'undefined') displaysize = 5;
        var sortcolumnindex = $(this).data("sortcolumnindex");
        var sortdirection = $(this).data("sortdirection");
        if (typeof (sortcolumnindex) === 'undefined') sortcolumnindex = 0;
        if (typeof (sortdirection) === 'undefined') sortdirection = "asc";
        var ordering = $(this).data("ordering");
        if (typeof (ordering) === 'undefined') ordering = true;
        TabloToDataTable(hedef, displaysize, sortcolumnindex, sortdirection, ordering);
    });
}).apply(this, [jQuery]);

(function ($) {
    $('.IlSecim').change(function () {
        var IlNo = $(this).val();
        var Hedef = $(this).data("hedef");
        var Url = $(this).data("url");
        console.log(IlNo);
        if (IlNo !== null && IlNo !== '') {
            $.ajax({
                type: "post",
                url: Url,
                data: { IlNo: IlNo },
                success: function (Sonuclar) {
                    console.log(Sonuclar);
                    $('#' + Hedef).empty();
                    $.each(Sonuclar, function (index, Ilce) {
                        $('#' + Hedef).append($('<option/>', {
                            value: Ilce.No,
                            text: Ilce.Ad
                        }));
                    });
                },
                error: function () {
                    alert("Hata");
                },
                beforeSend: function () {
                },
                complete: function () {
                }
            });
        }
    });
}).apply(this, [jQuery]);

function OnayKutusuOnayAl(title, text, type, confirmButtonText, cancelButtonText, Url, target = "", modal = false) {
    var sonuc = false;
    swal({
        title: title,
        text: text,
        type: type,
        html:true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: confirmButtonText,
        cancelButtonText: cancelButtonText
    },
        function (isConfirm) {
            if (isConfirm) {
                if (target === "") {
                    if (modal) {
                        e.preventDefault();
                        $('#PnlPencere').modal('show').find('.modal-body').load(Url);
                    }
                    else {
                        window.location.replace(Url);
                    }
                }
                else {
                    window.open(Url, target);
                }
            }
        });
    return sonuc;
}

$(function ($) {
    //----Sol menü - url değişimine göre selector işlemleri------------------------------------
    var url = window.location.href;

    if ((url.split("/")[3] || url.split("/")[4] || url.split("/")[5]) != undefined) {
        var seciliUrl = "/" + url.split("/")[3] + "/" + url.split("/")[4] + "/" + url.split("/")[5];
        var hedef = $('nav#menu a[href="' + seciliUrl + '"]');
        hedef.addClass("text-bold text-primary");
        hedef.parent("li").parent("ul").parent("li").addClass("nav-active nav-expanded");
    }
    else {
        $('nav#menu a[href="/"]').parent("li").addClass("nav-active");
    }
    //-----------------------------------------------------------------------------------------
});

(function ($) {
    $(".DosyaSec").change(function () {
        var Hedef = '#' + $(this).data('hedef');
        var hedefprocessbarkontrol = $('#' + $(this).data('hedefprocessbarkontrol'));
        var HedefOnIzleme = '#' + $(this).data('onizeme');

        if (window.FormData !== undefined) {
            var fileUpload = $(this).get(0);
            var files = fileUpload.files;
            var fileData = new FormData();
            for (var i = 0; i < files.length; i++) {
                fileData.append(files[i].name, files[i]);
            }
            $.ajax({
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (progress) {

                        var total = Math.round((progress.loaded / progress.total) * 100);
                        hedefprocessbarkontrol.css({ "width": total + "%" });
                        hedefprocessbarkontrol.text(total + "%");
                    }, false);
                    //Code to be executed if upload is aborted
                    xhr.addEventListener("abort", function () {
                        hedefprocessbarkontrol.closest(".progress").fadeOut(3000, function () {
                            $(this).hide();
                        });
                    }, false);
                    //Update progress and remove it after upload has finished
                    xhr.addEventListener("loadend", function () {
                        hedefprocessbarkontrol.closest(".progress").fadeOut(3000, function () {
                            $(this).hide();
                        });
                    }, false);

                    //Show an error on progress if an error has occured during upload
                    xhr.addEventListener("error", function () {
                        hedefprocessbarkontrol.closest(".progress")
                            .addClass("progress-error").find("status-count").text("Error");
                    }, false);
                    //Show timeout error on progress bar if upload request has timedout
                    xhr.addEventListener("timeout", function (progress) {
                        hedefprocessbarkontrol.closest(".progress")
                            .addClass("progress-timedout").find("status-count").text("Timed Out");
                    }, false);
                   return xhr;
                },
                url: '/Kullanici/DosyaYonetim/DosyaYukle',
                type: "POST",
                contentType: false,
                processData: false,
                data:fileData,
                beforeSend: function ()
                {
                    $(this).show();
                    $(".se-pre-con").fadeIn("slow");
                    hedefprocessbarkontrol.parent().show();
                },
                complete: function ()
                {

                    $(".se-pre-con").fadeOut("slow");
                },
                success: function (Sonuc)
                {
                    $(".se-pre-con").fadeIn("slow");
                    $(Hedef).val(Sonuc.Deger);
                    $(HedefOnIzleme).attr("src", Sonuc.Deger.replace("~/", "/"));
                    $(HedefOnIzleme).attr("href", Sonuc.Deger.replace("~/", "/"));
                    $(".se-pre-con").fadeOut("slow");
                },
                error: function (Hata) {
                    console.log(Hata);
                    swal({
                        title: "Dosya Yükleme",
                        text: "Hata Oluştu...",
                        html:true,
                        type: 'error',
                        showCancelButton: false
                    });
                }
            });
        }
        else
        {
            swal({
                html:true,
                title: "Dosya Yükleme",
                text: "Desteklenmeyen Format",
                type: 'error',
                showCancelButton: false
            });
        }
    });

}).apply(this, [jQuery]);

$(function ($) {
    $('.ModalPencere').click(
        function () {
            var height = $(this).attr('data-height');
            var width = $(this).attr('data-width');
            var Html = $(this).attr('data-html');
            var Tip = $(this).attr('data-tip');
            var Url = $(this).attr('data-url');
            var Baslik = $(this).attr('data-baslik');
            var ButtonText = $(this).attr('data-buttontext');
            if (Tip === "1") {
                $('#_ModalPencereBody').load(Url, function () {
                    $('#Baslik').html(Baslik);
                    $('#btnKapat').val(ButtonText);
                });
            }
            if (Tip === "2") {
                if (Url.includes('.jpg') | Url.includes('.png') | Url.includes('.jpeg')) {
                    Html = '<img style="width:100%;height:100%" src="' + Url + '" </img>';
                } else if (Url.includes('.pdf')) {
                    Html = '<object data="' + Url + '" type="application/pdf" width="100%" height="550px"><p>Alternative text - include a link < a href = "' + Url + '"> to the PDF!</a ></p></object>'
                }
                $('#_ModalPencereBody').html(Html);
                $('#Baslik').html(Baslik);
                $('#btnKapat').val(ButtonText);
            }
            $('#_ModalPencere').modal({
                show: true,
                backdrop: 'static',
                keyboard: false
            });
        }
    );
});

$(function ($) {
    $(document).on("click", ".btndelete", function () {
        var link = $(this).data("link");
        var baslik = $(this).data("baslik");
        var mesaj = $(this).data("mesaj");
        return OnayKutusuOnayAl(baslik, mesaj, 'warning', 'Evet', 'Hayır', link);
    });
    $(document).on("click", ".btnclone", function () {
        var link = $(this).data("link");
        var baslik = $(this).data("baslik");
        var mesaj = $(this).data("mesaj");
        return OnayKutusuOnayAl(baslik, mesaj, 'warning', 'Evet', 'Hayır', link);
    });

    $(document).on("click", ".btnOnay", function () {
        var link = $(this).data("link");
        var baslik = $(this).data("baslik");
        var mesaj = $(this).data("mesaj");
        var modaldurum = $(this).data("modal-durum");
        if (typeof modaldurum === "undefined") {
            modaldurum = false;
        }
        return OnayKutusuOnayAl(baslik, mesaj, 'warning', 'Evet', 'Hayır', link, modaldurum);
    });
});

$(function ($) {
    $(".remoteselect").each(function () {
        var data_createurl = $(this).data("createurl");
        var data_selecturl = $(this).data("selecturl");
        var data_initurl = $(this).data("initurl");
        var data_initvalue = $(this).data("initvalue");
        var minimumInputLength = 0;
        if ($(this).data("minimumInputLength") !== "undefined") {
            minimumInputLength = $(this).data("minimumInputLength");
        }
        var selectislem = $(this).select2({
            minimumInputLength: minimumInputLength,
            ajax: {
                url: data_selecturl,
                type: "POST",
                dataType: 'json',
                data: function (term) {
                    return {
                        term: term,
                    };
                },
                results: function (data, page) {
                    return { results: data.Sonuc };
                },
            },
            initSelection: function (element, callback) {
                var id = data_initvalue;
                if (id !== "") {
                    $.ajax(data_initurl, {
                        data: { id: id },
                        dataType: "json"
                    }).done(function (data) {
                        //console.log(data);
                        callback(data.Sonuc);
                    });
                }
            }
        });
        selectislem.on("change", function (e) {
            var data = e.added;
            if (data.id == -1)
            {
                window.open(data_createurl, '_blank').focus();
            }
        });
    });

    $(".remotemultiselect").each(function () {
        var data_selecturl = $(this).data("selecturl");
        var data_planno = $(this).data("planno");
        var data_initurl = $(this).data("initurl");
        var data_initvalue = $(this).data("initvalue");
        var minimumInputLength = 0;
        if ($(this).data("minimumInputLength") !== "undefined") {
            minimumInputLength = $(this).data("minimumInputLength");
        }
        var selectislem = $(this).select2({
            minimumInputLength: minimumInputLength,
            multiple:true,
            ajax: {
                url: data_selecturl,
                type: "POST",
                dataType: 'json',
                data: function (term) {
                    return {
                        term: term,
                        planno: data_planno
                    };
                },
                results: function (data, page) {
                    return { results: data.Sonuc };
                },
            },
            initSelection: function (element, callback) {
                var id = data_initvalue;
                $.ajax(data_initurl, {
                    data: { id: id },
                    type: "POST",
                    dataType: "json"
                }).done(function (data) {
                    callback(data.Sonuc);
                });
            }
        });
        selectislem.on("change", function (e) {
            var data = e.added;
            if (data.id == -1) {
                window.open(data_createurl, '_blank').focus();
            }
        });
    });
});


function OnayKutusuOnayAl(title, text, type, confirmButtonText, cancelButtonText, Url, target = "") {
    var sonuc = false;
    if (target == "")
    {
        target = false;
    }
    swal({
        title: title,
        text: text,
        type: type,
        html:true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: confirmButtonText,
        cancelButtonText: cancelButtonText
    },
        function (isConfirm) {
            if (isConfirm) {
                if (target === false)
                {
                    window.location.replace(Url);
                }
                else {
                    window.open(Url, target);
                }
            }
        });
    return sonuc;
}

$(".video-modal").click(function () {
    var theModal = $(this).data("target");
    videoSRC = $(this).attr("data-video");
    alert(videoSRC);
    videoSRCauto = videoSRC + "?modestbranding=1&rel=0&controls=0&showinfo=0&html5=1&autoplay=1";
    $(theModal + ' iframe').attr('src', videoSRCauto);
    $(theModal).on('hidden.bs.modal', function () {
        $(theModal + ' iframe').attr('src', videoSRC);
    });
});



function KontrolRender() {
    AreaTextSummerNote('.summernote');
    $(".DtTable").each(function (index) {
        var dipslaysize = 0;
        var hedef = "#" + $(this).attr("id");
        var displaysize = $(this).attr("data-displaysize");
        if (typeof (displaysize) === 'undefined') displaysize = 5;
        var sortcolumnindex = $(this).data("sortcolumnindex");
        var sortdirection = $(this).data("sortdirection");
        if (typeof (sortcolumnindex) === 'undefined') sortcolumnindex = 0;
        if (typeof (sortdirection) === 'undefined') sortdirection = "asc";
        var ordering = $(this).data("ordering");
        if (typeof (ordering) === 'undefined') ordering = true;
        TabloToDataTable(hedef, displaysize, sortcolumnindex, sortdirection, ordering);
    });

    // Tooltip
    (function ($) {
        $('.jstreeListe').jstree({
            'core': {
                'themes': {
                    'name': 'proton',
                    'responsive': true
                }
            }
        });
        $(".jstreeListe").bind(
            "select_node.jstree", function (evt, data) {
                var NesneId = data.node.li_attr["data-nesne"];
                var HedefNesne = $('#' + NesneId);
                console.log(HedefNesne);
                if (HedefNesne !== null) {
                    var Deger = data.node.li_attr["data-deger"];
                    HedefNesne.val(Deger);
                }
            }
        )
            .on('changed.jstree', function (e, data) {
                var path = data.instance.get_path(data.node, '/');
                console.log('Selected: ' + path);
            });
    }).apply(this, [jQuery]);
    $('[data-plugin-selectTwo]').each(function () {
        var $this = $(this),
            opts = {};
        var pluginOptions = $this.data('plugin-options');
        if (pluginOptions)
            opts = pluginOptions;
        $this.themePluginSelect2(opts);
    });
    $('[data-plugin-ios-switch]').each(function () {
        var $this = $(this);
        $this.themePluginIOS7Switch();
    });
    $('[data-plugin-ios-switch]').each(function () {
        var $this = $(this);
        $this.themePluginIOS7Switch();
    });
    $('.rating').each(function () {
        var $this = $(this);
        var tur = $this.attr("data-tur");
        var alantur = $this.attr("data-alantur");
        var alanNo = $this.attr("data-alanNo");
        var tur = $this.attr("data-tur");
        $this.barrating({
            theme: 'fontawesome-stars',
            onSelect: function (value, text, event) {
                if (value == "") {
                    value=0
                }
                if (typeof (event) !== 'undefined') {
                    var Url = '/Kullanici/Base/UserDerecelendirmeKaydet';
                    var deger = value;
                    $.ajax({
                        type: "post",
                        url: Url,
                        data: { tur: tur, alantur:alantur, alanno:alanNo, deger:deger},
                        success: function (Sonuc) {

                        },
                        error: function () {
                            alert("Hata");
                        },
                        beforeSend: function () {
                        },
                        complete: function () {
                        }
                    });
                }
                else
                {
                    // rating was selected programmatically
                    // by calling `set` method
                }
            }
        });

        var data_current_rating = $this.attr("data-current-rating");
        $this.barrating('set', data_current_rating);

    });
}



$(function () {
    $(document).on("click", ".btn-ajax-action", function (e) {
        e.preventDefault();
        var action = $(this).data("action-url");
        var itemId = $(this).data("item-id");
        var ajaxparams = $(this).data("ajax-params")[0];
        var mtitle = $(this).data("modal-title");
        var mclass = $(this).data("modal-class");
        var ttype = $(this).data("togglemodal");
        var cmessage = $(this).data("confirm-message");

        if (cmessage != undefined && cmessage.length > 0) {
            if (!confirm(cmessage))
                return false;
        }

        var target;
        if (ttype == "modal") {
            target = $($(this).data("target") + " #modal-body-content");
            $($(this).data("target") + " h5.modal-title").text(mtitle);
            if (mclass != undefined && mclass.length > 0) {
                $($(this).data("target") + " .modal-dialog").addClass(mclass);
            }
        } else {
            target = $($(this).data("target"));
        }
        target.html("");
        $.ajax({
            type: "get",
            url: action,
            data: ajaxparams,
            beforeSend: function () {
                $("#loadingAjax").show();
            },
            success: function (r) {

                if (ttype == "noty") {
                    new PNotify({
                        title: r.Baslik,
                        text: r.Mesaj,
                        type: 'custom',
                        addclass: 'notification-danger',
                        icon: 'fas fa-clipboard-check'
                    });

                    if (r.Durum) {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    return false;
                }
                if (r.Durum != undefined) {
                    target.html(r.message);
                } else {
                    target.html(r);
                    KontrolRender();
                }
            },
            error: function (r) {
                new PNotify({
                    title: 'Hata',
                    text: r.statusText,
                    type: 'custom',
                    addclass: 'notification-danger',
                    icon: 'fas fa-clipboard-check'
                });
            },
            complete: function (r) {
                $("#loadingAjax").hide();
            }
        });
    });
});

function fnCompleteAjax(result) {
    var r = result.responseJSON;
    var type = "success";
    var icon = "fas fa-clipboard-check";
    r.Tip == "Hata";
    if (r.Tip == 1) {
        type = "success";
        icon = "fas fa-check-circle";
    }
    if (r.Tip == 2) {
        type = "danger";
        icon = "fas fa-exclamation-circle";
    }
    if (r.Tip == 3) {
        type = "error";
        icon = "fas fa-exclamation-triangle";
    }
    if (r.Tip == 4) {
        icon = "fas fa-comment-exclamation";
        type = "info";
    }

    new PNotify({
        title: r.Baslik,
        text: r.Mesaj,
        type: type,
        icon: 'fas fa-clipboard-check'
    });

    if (r.SayfaYenidenYuklenme) {
        setTimeout(function () {
            window.location.reload();
        }, 1000);
    }
    else {
        if (r.Durum) {
            $("#PnlModalPenceresi").modal("hide");
        }
    }
}





String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1)
}




//if (window.jQuery) {
//    $(".BtnIslem").click(function () {
//        var top = $(document).scrollTop();
//        localStorage['scrollTop'] = top;
//    });
//    $(window).load(function () {
//        var top = parseInt(localStorage['scrollTop']);
//        $(document).scrollTop(top);
//    });
//}


$(function () {
    $('.rating').each(function () {
        var $this = $(this);
        var tur = $this.attr("data-tur");
        var alantur = $this.attr("data-alantur");
        var alanNo = $this.attr("data-alanNo");
        var tur = $this.attr("data-tur");
        $this.barrating({
            theme: 'fontawesome-stars',
            onSelect: function (value, text, event) {
                if (value == "") {
                    value = 0
                }
                if (typeof (event) !== 'undefined') {
                    var Url = '/Kullanici/Base/UserDerecelendirmeKaydet';
                    var deger = value;
                    $.ajax({
                        type: "post",
                        url: Url,
                        data: { tur: tur, alantur: alantur, alanno: alanNo, deger: deger },
                        success: function (Sonuc) {

                        },
                        error: function () {
                            alert("Hata");
                        },
                        beforeSend: function () {
                        },
                        complete: function () {
                        }
                    });
                }
                else {
                }
            }
        });
        var data_current_rating = $this.attr("data-current-rating");
        $this.barrating('set', data_current_rating);
    });
    $('.dosyasecprogressbar').hide();
});



$(function () {
    $('.datetimepicker').datetimepicker({
        locale: 'tr'
    });

    $('.datepicker').datepicker({
        language: 'tr',
        format: "dd.mm.yyyy",
    }).on('focus', function () {
        $(this).trigger('blur');
    });
});


String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1)
}


$(function () {
    $('.tokenfield').tokenfield({
        delimiter: '\r\n'
    });
});

(function ($) {
    $(".Para").maskMoney({ prefix: 'TL ', allowZero: true, allowNegative: true, thousands: '', decimal: ',', affixesStay: false });


    $('.MaskEdit').each(function () {
        var nesne = $(this);
        var prefix = nesne.data('prefix');
        var suffix = nesne.data('suffix');
        var affixesStay = nesne.data('affixesStay');
        var thousands = nesne.data('thousands');
        var precision = nesne.data('precision');
        var allowZero = nesne.data('allowZero');
        var allowEmpty = nesne.data('allowEmpty');
        var allowNegative = nesne.data('allowNegative');
        var doubleClickSelection = nesne.data('doubleClickSelection');
        var bringCaretAtEndOnFocus = nesne.data('bringCaretAtEndOnFocus');

        if (typeof (prefix) == "undefined") { prefix = ""; };
        if (typeof (suffix) == "undefined") { suffix = ""; };
        if (typeof (affixesStay) == "undefined") { affixesStay = true; };
        if (typeof (thousands) == "undefined") { thousands = ","; };
        if (typeof (precision) == "undefined") { precision = "2"; };
        if (typeof (allowZero) == "undefined") { allowZero = true; };
        if (typeof (allowEmpty) == "undefined") { allowEmpty = true; };
        if (typeof (allowNegative) == "undefined") { allowNegative = false; };
        if (typeof (doubleClickSelection) == "undefined") { doubleClickSelection = true; };
        if (typeof (bringCaretAtEndOnFocus) == "undefined") { bringCaretAtEndOnFocus = true; };
        nesne.maskMoney({
            prefix: prefix,
            sufix: suffix,
            affixesStay: affixesStay,
            thousands: thousands,
            precision: precision,
            allowEmpty: allowEmpty,
            allowNegative: allowNegative,
            doubleClickSelection: doubleClickSelection,
            bringCaretAtEndOnFocus: bringCaretAtEndOnFocus
        });
    });

}).apply(this, [jQuery]);

