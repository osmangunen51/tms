﻿using System.Web.Mvc;
namespace TMS.Panel.Web.Areas.Kullanici
{
    internal static class RouteConfig
    {
        internal static void RegisterRoutes(AreaRegistrationContext context)
        {
            context.MapRoute(
                             "Kullanici_default",
                             "Kullanici/{controller}/{action}/{id}",
                            defaults: new { controller = "Anasayfa", action = "Index", id = UrlParameter.Optional },
                            namespaces: new[] { "TMS.Panel.Web.Areas.Kullanici.Controllers" }
                         );
        }
    }
}
