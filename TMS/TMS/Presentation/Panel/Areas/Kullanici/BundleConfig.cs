﻿using System.Web.Optimization;

namespace TMS.Panel.Web.Areas.Kullanici
{
    internal static class BundleConfig
    {
        internal static void RegisterBundles(BundleCollection bundles)
        {
            #region Css Bundle



            bundles.Add(new StyleBundle("~/Kullanici.Vendor/css").
                        Include
                        (
                            "~/Areas/Kullanici/Theme/Default/vendor/bootstrap/css/bootstrap.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/font-awesome/css/font-awesome.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/font-awesome5/css/all.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/magnific-popup/magnific-popup.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/bootstrap-datepicker/css/datepicker3.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/sweetalert/sweetalert.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/pnotify/pnotify.custom.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/jstree/themes/default/style.css"
                        )
                );
            bundles.Add(new StyleBundle("~/Kullanici.CustomVendor/css").
                        Include
                        (
                            "~/Areas/Kullanici/Theme/Default/vendor/select2/select2.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/bootstrap-multiselect/bootstrap-multiselect.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/jquery-datatables-bs3/assets/css/datatables.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/morris/morris.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/summernote/summernote.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/summernote/summernote-bs4.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/hover-css/hover.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/jquery-bar-rating/themes/fontawesome-stars.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/tokenfield/css/tokenfield-typeahead.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/tokenfield/css/bootstrap-tokenfield.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/bootstrap-tour/css/bootstrap-tour.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/bootstrap-tour/css/theme-flaty.css"
                        )
                );

            bundles.Add(new StyleBundle("~/Kullanici.Theme/css").
                     Include
                     (
                         "~/Areas/Kullanici/Theme/Default/css/theme.css"
                     )
            );
            bundles.Add(new StyleBundle("~/Kullanici.CustomTheme/css").
                                    Include
                                    (
                                        "~/Areas/Kullanici/Theme/Default/css/theme-custom.css"
                                    )
                            );

            #endregion Css Bundle

            #region Javascript Bundle

            bundles.Add(
                         new ScriptBundle("~/Kullanici.modernizr/js").
                                Include(
                                        "~/Areas/Kullanici/Theme/Default/vendor/modernizr/modernizr.js"
                                        )
                        );

            bundles.Add(
                           new ScriptBundle("~/Kullanici.jquery/js").
                               Include(
                               "~/Areas/Kullanici/Theme/Default/vendor/jquery/v1.12.0/jquery.js",
                               "~/Areas/Kullanici/Theme/Default/vendor/jquery-ui/js/v1.11.4/jquery-ui.js",
                               "~/Areas/Kullanici/Theme/Default/vendor/jquery-browser-mobile/jquery.browser.mobile.js",
                               "~/Areas/Kullanici/Theme/Default/vendor/jquery-cookie/jquery.cookie.js"
                               ));

            bundles.Add(
                new ScriptBundle("~/Kullanici.Vendor/js").
                    Include(
                    "~/Areas/Kullanici/Theme/Default/vendor/jquery-ui/v1.10.4/js/jquery-ui-1.10.4.js",
                    //"~/Areas/Kullanici/Theme/Default/vendor/style-switcher/style.switcher.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/bootstrap/js/bootstrap.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/nanoscroller/nanoscroller.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/magnific-popup/magnific-popup.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/jquery-datatables/media/js/jquery.dataTables.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/jquery-datatables-bs3/assets/js/datatables.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/jquery-placeholder/jquery.placeholder.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/summernote/summernote.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/TableExport/tableExport.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/TableExport/jquery.base64.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/TableExport/html2canvas.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/TableExport/jspdf/libs/sprintf.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/TableExport/jspdf/jspdf.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/TableExport/jspdf/libs/base64.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/rowsorter/rowsorter.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/sweetalert/sweetalert.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/jquery-countdown/jquery.countdown.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/jstree/jstree.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/clipboard/clipboard.min.js",
                    "~/Areas/Kullanici/Theme/Default/vendor/store-js/store.js"
                    )
            );
            bundles.Add(
                new ScriptBundle("~/Kullanici.CustomVendor/js").
                    Include(
                                "~/Areas/Kullanici/Theme/Default/vendor/jquery-ui/v1.10.4/js/jquery-ui-1.10.4.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jquery-appear/jquery.appear.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/select2/select2.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/bootstrap-multiselect/bootstrap-multiselect.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jquery-easypiechart/jquery.easypiechart.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/flot/jquery.flot.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/flot-tooltip/jquery.flot.tooltip.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/flot/jquery.flot.pie.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/flot/jquery.flot.categories.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/flot/jquery.flot.resize.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jquery-sparkline/jquery.sparkline.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/raphael/raphael.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/morris/morris.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/gauge/gauge.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/snap-svg/snap.svg.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/liquid-meter/liquid.meter.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jqvmap/jquery.vmap.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jqvmap/data/jquery.vmap.sampledata.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jqvmap/maps/jquery.vmap.world.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.africa.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.asia.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.australia.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.europe.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/bootstrap-datetimepicker/js/moment.min.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jquery-validation/jquery.validate.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/pnotify/pnotify.custom.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/ios7-switch/ios7-switch.js",
                                "~/Areas/Kullanici/Theme/Default/js/ui-elements/examples.modals.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/bootstrap-filestyle-1.3.0/bootstrap-filestyle.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/validate/jquery.validate.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/validate/jquery.validate.unobtrusive.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/validate/jquery.unobtrusive-ajax.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jquery-bar-rating/jquery.barrating.min.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/tokenfield/js/bootstrap-tokenfield.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/bootstrap-tour/js/bootstrap-tour.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/bootstrap-tour/js/cdstorage.js",
                                "~/Areas/Kullanici/Theme/Default/vendor/jquery.maskMoney.js"
                            )
            );
            bundles.Add(
                new ScriptBundle("~/Kullanici.theme/js").
                    Include(
                            "~/Areas/Kullanici/Theme/Default/js/theme.js"
                            )
            );
            bundles.Add(
               new ScriptBundle("~/Kullanici.customtheme/js").
                   Include(
                           "~/Areas/Kullanici/Theme/Default/js/theme.custom.js"
                           )
           );
            bundles.Add(
               new ScriptBundle("~/Kullanici.theme_init/js").
                   Include(
                           "~/Areas/Kullanici/Theme/Default/js/theme.init.js"
                           )
            );
            bundles.Add(
                new ScriptBundle("~/Kullanici.SpecificPage/js").
                    Include(

                            )
            );

            bundles.Add(
                new ScriptBundle("~/Kullanici.GoogleMap/js", "https://maps.googleapis.com/maps/api/js?key=AIzaSyDDFkrvHfVe77pUg0E-xpUc3zrXk7J-oks").
                    Include(
                    "~/Areas/Kullanici/Theme/Default/js/gmaps.js"
                    )
            );
            #endregion Javascript Bundle
            bundles.Add(new ScriptBundle("~/Kullanici.elfinder/js").Include(
                 //"~/Areas/Kullanici/Theme/Default/vendor/elfinder/jquery-1.9.1.js",
                 //"~/Areas/Kullanici/Theme/Default/vendor/elfinder/jquery-ui-1.10.2.js",
                 "~/Areas/Kullanici/Theme/Default/vendor/elfinder/js/elfinder.full.js"
                 ));
            bundles.Add(new StyleBundle("~/Kullanici.elfinder/css").Include(
                            "~/Areas/Kullanici/Theme/Default/vendor/elfinder/css/elfinder.full.css",
                            "~/Areas/Kullanici/Theme/Default/vendor/elfinder/css/theme.css"));
            BundleTable.EnableOptimizations = false;
        }
    }
}