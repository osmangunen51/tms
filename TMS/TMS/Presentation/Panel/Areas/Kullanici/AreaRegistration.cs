﻿using System.Web.Mvc;
using System.Web.Optimization;

namespace TMS.Panel.Web.Areas.Kullanici
{
    public class KullaniciAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Kullanici";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            RegisterRoutes(context);
            RegisterBundles();
        }

        private void RegisterRoutes(AreaRegistrationContext context)
        {
            Kullanici.RouteConfig.RegisterRoutes(context);
        }

        private void RegisterBundles()
        {
            Kullanici.BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}