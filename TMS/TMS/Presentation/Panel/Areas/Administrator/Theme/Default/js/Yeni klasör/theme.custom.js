var ResimEkle = function (context) {
    var ui = $.summernote.ui;
    var button = ui.button({
        contents: '<i class="fa fa-picture-o"/> Resim Ekle',
        //tooltip: 'Resim Ekle',
        click: function (e) {
            EditorAcResimEkle(context);
        }
    });
    return button.render();
}
var LinkEkle = function (context) {
    var ui = $.summernote.ui;
    var button = ui.button({
        contents: '<i class="fa fa-link"/> Link Ekle',
        //tooltip: 'Link Ekle',
        click: function (e) {
            EditorAcLinkEkle(context);
        }
    });
    return button.render();
}
var VideoEkle = function (context) {
    var ui = $.summernote.ui;
    var button = ui.button({
        contents: '<i class="fa fa-youtube-play"/> Video Ekle',
        //tooltip: 'Video Ekle',
        click: function (e) {
            EditorAcVideoEkle(context);
        }
    });
    return button.render();
}
var AuidoEkle = function (context) {
    var ui = $.summernote.ui;
    var button = ui.button({
        contents: '<i class="fa fa-file-audio-o"/> Ses Ekle',
        //tooltip: 'Ses Ekle',
        click: function (e) {
            EditorAcAudioEkle(context);
        }
    });
    return button.render();
}
function EditorAcAudioEkle(editor) {
    var Folder = editor.data("userelfinderfolder");
    var subFolder = editor.data("userelfindersubfolder");
    if (Folder == null) {
        Folder = "Content";
    }
    if (subFolder == null) {
        subFolder = "Bos";
    }
    $('<div />').dialogelfinder({
        url: '/connector',
        customData: { folder: Folder, subFolder: subFolder },
        rememberLastDir: false,
        lang: 'tr',
        commandsOptions: {
            getfile: {
                oncomplete: 'destroy' // destroy elFinder after file selection
            }
        },
        getFileCallback: function (File) {
            var url = File.path.toString();
            url = url.replace('Files', File.baseUrl);
            var Html = '<p><audio controls><source src="' + url + '" type="audio/mpeg"></audio></p><p></p>'
            editor.summernote('pasteHTML', Html);
        }
    });
}
function EditorAcResimEkle(editor) {
    var Folder = editor.data("userelfinderfolder");
    var subFolder = editor.data("userelfindersubfolder");
    console.log(Folder);
    console.log(subFolder);
    if (Folder == null) {
        Folder = "Content";
    }
    if (subFolder == null) {
        subFolder = "Bos";
    }
    $('<div />').dialogelfinder({
        url: '/connector',
        customData: { folder: Folder, subFolder: subFolder },
        rememberLastDir: false,
        lang: 'tr',
        commandsOptions: {
            getfile: {
                oncomplete: 'destroy' // destroy elFinder after file selection
            }
        },
        getFileCallback: function (File) {
            var url = File.path.toString();
            url = url.replace('Files', File.baseUrl);
            console.log(url);
            editor.summernote('editor.insertImage', url, function ($image) {
                $image.css('width', $image.width());
                $image.attr('class', "img-responsive");
            });
        }
    });
}
function EditorAcVideoEkle(editor) {
    var Folder = editor.data("userelfinderfolder");
    var subFolder = editor.data("userelfindersubfolder");
    if (Folder == null) {
        Folder = "Content";
    }
    if (subFolder == null) {
        subFolder = "Bos";
    }
    $('<div />').dialogelfinder({
        url: '/connector',
        customData: { folder: Folder, subFolder: subFolder },
        rememberLastDir: false,
        lang: 'tr',
        commandsOptions: {
            getfile: {
                oncomplete: 'destroy' // destroy elFinder after file selection
            }
        },
        getFileCallback: function (File) {
            var url = File.path.toString();
            url = url.replace('Files', File.baseUrl);
            var node = document.createElement('div');
            var html = '<video width="100%" height="400" controls="controls"><source src="' + url + '" type= "video/mp4" autostart= "true"></video>';
            editor.summernote.invoke('editor.insertText', html);
        }
    });
}
function EditorAcLinkEkle(editor) {
    var Folder = editor.data("userelfinderfolder");
    var subFolder = editor.data("userelfindersubfolder");
    if (Folder == null) {
        Folder = "Content";
    }
    if (subFolder == null) {
        subFolder = "Bos";
    }
    $('<div />').dialogelfinder({
        url: '/connector',
        customData: { folder: Folder, subFolder: subFolder },
        rememberLastDir: false,
        lang: 'tr',
        commandsOptions: {
            getfile: {
                oncomplete: 'destroy' // destroy elFinder after file selection
            }
        },
        getFileCallback: function (File) {
            var url = File.path.toString();
            url = url.replace('Files', File.baseUrl);
            editor.summernote('createLink',
                {
                    text: File.name,
                    url: url,
                    newWindow: true
                }
            );
        }
    });
}
$(".summernote").each(function (Index) {
    var Editor = $(this);
    Editor.summernote({
        height: 400,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            //['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']],
            ['ResimEkle', ['ResimEkle']],
            ['LinkEkle', ['LinkEkle']],
            ['VideoEkle', ['VideoEkle']],
            ['AuidoEkle', ['AuidoEkle']]
        ],
        buttons: {
            ResimEkle: ResimEkle(Editor),
            LinkEkle: LinkEkle(Editor),
            VideoEkle: VideoEkle(Editor),
            AuidoEkle: AuidoEkle(Editor)
        }
    });
});
(function ($) {
    var GetUrlBilgi = function (href) {
        var l = document.createElement("a");
        l.href = href;
        return l;
    };
    $(".BtnResimYukle").click(function () {
        var NesneId = $(this).attr("data-hedef");
        var Folder = $(this).attr("data-userelfinderfolder");
        var subFolder = $(this).attr("data-userelfindersubfolder");
        $('<div />').dialogelfinder({
            url: '/connector',
            customData: { folder: Folder, subFolder: subFolder },
            rememberLastDir: false,
            lang: 'tr',
            commandsOptions: { getfile: { oncomplete: 'destroy' } },
            getFileCallback: function (File) {
                console.log(File);
                var url = File.path.toString();
                url = url.replace('Files', File.baseUrl);
                var urlRsm = url;
                var Lnk = GetUrlBilgi(url);
                url = '~' + Lnk.pathname;
                var HedefNesne = $('#' + NesneId);
                console.log(HedefNesne);
                if (HedefNesne != null) {
                    url = url.replace("~C", "~/C");
                    url = url.replace("~c", "~/c");
                    HedefNesne.val(url);
                    console.log(url);
                    $('#SpnRsm' + NesneId).attr("style", 'background-image:url(' + urlRsm + '?h=70&amp;w=150&amp;mode=max&amp;scale=both);background-repeat:no-repeat;background-size:100%;width:150px;height:70px;');
                }
            }
        });
    });
    $(".BtnResimTemizle").click(function () {
        var NesneId = $(this).attr("data-hedef");
        var HedefNesne = $('#' + NesneId);
        if (HedefNesne != null) {
            HedefNesne.val('');
            $('#SpnRsm' + NesneId).attr("style", "width:150px;height:70px;");
        }
    });

    if (typeof GeriSayimZaman === "undefined") {
    }
    else {
        $('#GeriSayim').countdown(GeriSayimZaman)
            .on('update.countdown', function (event) {
                var $this = $(this);
                $(this).html(event.strftime(''
                    + '<span class="btn btn-info"><strong>%M</strong></span> Dk. '
                    + '<span class="btn btn-success btn-sm"><strong>%S</strong></span> Sn.'));
            }
            ).on('finish.countdown', function (event) {
                window.open(GeriSayimUrl, "_self")
            }
            );
    }


}).apply(this, [jQuery]);
$(window).load(function () {
    $(".se-pre-con").fadeOut("slow");
});
// Tooltip
(function ($) {
    $('.jstreeListe').jstree({
        'core': {
            'themes': {
                'name': 'proton',
                'responsive': true
            }
        }
    });
    $(".jstreeListe").bind(
        "select_node.jstree", function (evt, data) {
            var NesneId = data.node.li_attr["data-nesne"];
            var HedefNesne = $('#' + NesneId);
            console.log(HedefNesne);
            if (HedefNesne != null) {
                var Deger = data.node.li_attr["data-deger"];
                HedefNesne.val(Deger);
            }
        }
    )
        .on('changed.jstree', function (e, data) {
            var path = data.instance.get_path(data.node, '/');
            console.log('Selected: ' + path);
        });
}).apply(this, [jQuery]);
$(function ($) {
    //----Sol men� - url de�i�imine g�re selector i�lemleri------------------------------------
    var url = window.location.href;
    if ((url.split("/")[3] || url.split("/")[4]) != undefined) {
        var seciliUrl = "/" + url.split("/")[3] + "/" + url.split("/")[4];
        var hedef = $('nav#menu a[href="' + seciliUrl + '"]');
        hedef.addClass("text-bold text-primary");
        hedef.parent("li").parent("ul").parent("li").addClass("nav-active nav-expanded");
    }
    else {
        $('nav#menu a[href="/"]').parent("li").addClass("nav-active");
    }
    //-----------------------------------------------------------------------------------------
});
$('.dropdown-menu ul li').click(function (e) {
});
$('.OpenDropDownMenu').on({
    "click": function (e) {
        e.stopPropagation();
    }
});
// Tooltip
(function ($) {
    $('.datatabkel').jstree({
        'core': {
            'themes': {
                'name': 'proton',
                'responsive': true
            }
        }
    });
    $(".jstreeListe").bind(
        "select_node.jstree", function (evt, data) {
            var NesneId = data.node.li_attr["data-nesne"];
            var HedefNesne = $('#' + NesneId);
            console.log(HedefNesne);
            if (HedefNesne != null) {
                var Deger = data.node.li_attr["data-deger"];
                HedefNesne.val(Deger);
            }
        }
    )
        .on('changed.jstree', function (e, data) {
            var path = data.instance.get_path(data.node, '/');
            console.log('Selected: ' + path);
        });
}).apply(this, [jQuery]);

function TabloToDataTable(Tablo, DisplaySize) {
    if (typeof (DisplaySize) === 'undefined') a = 50;
    $(Tablo).dataTable(
        {
            "iDisplayLength": DisplaySize,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Turkish.json",
                "sSearch":"asasa"
            },
            "lengthMenu": [[50, 100, 250, 500, -1], [50, 100, 250, 500, "T�m�"]]
        }
    );
}
function AreaTextSummerNote(Tablo) {
    var Editor = $(Tablo);
    Editor.summernote({
        height: 400,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            //['insert', ['link', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview', 'help']],
            ['ResimEkle', ['ResimEkle']],
            ['LinkEkle', ['LinkEkle']],
            ['VideoEkle', ['VideoEkle']],
            ['AuidoEkle', ['AuidoEkle']]
        ],
        buttons: {
            ResimEkle: ResimEkle(Editor),
            LinkEkle: LinkEkle(Editor),
            VideoEkle: VideoEkle(Editor),
            AuidoEkle: AuidoEkle(Editor)
        }
    });
}
function KontrolRender() {
    AreaTextSummerNote('.summernote');
    // Tooltip
    (function ($) {
        $('.jstreeListe').jstree({
            'core': {
                'themes': {
                    'name': 'proton',
                    'responsive': true
                }
            }
        });
        $(".jstreeListe").bind(
            "select_node.jstree", function (evt, data) {
                var NesneId = data.node.li_attr["data-nesne"];
                var HedefNesne = $('#' + NesneId);
                console.log(HedefNesne);
                if (HedefNesne != null) {
                    var Deger = data.node.li_attr["data-deger"];
                    HedefNesne.val(Deger);
                }
            }
        )
            .on('changed.jstree', function (e, data) {
                var path = data.instance.get_path(data.node, '/');
                console.log('Selected: ' + path);
            });
    }).apply(this, [jQuery]);
    $('[data-plugin-selectTwo]').each(function () {
        var $this = $(this),
            opts = {};
        var pluginOptions = $this.data('plugin-options');
        if (pluginOptions)
            opts = pluginOptions;
        $this.themePluginSelect2(opts);
    });
    $('[data-plugin-ios-switch]').each(function () {
        var $this = $(this);
        $this.themePluginIOS7Switch();
    });
}
(function ($) {
    var clipboardcopy = new Clipboard('.kopyalakes');
    clipboardcopy.on('success', function (e) {
        e.clearSelection();
    });
}).apply(this, [jQuery]);
// Tooltip
(function ($) {
    TabloToDataTable('.DtTable', 50);
}).apply(this, [jQuery]);
(function ($) {
    $('.IlSecim').change(function () {
        var IlNo = $(this).val();
        var Hedef = $(this).data("hedef");
        var Url = $(this).data("url");
        console.log(IlNo);
        if (IlNo != null && IlNo != '') {
            $.ajax({
                type: "post",
                url: Url,
                data: { IlNo: IlNo },
                success: function (Sonuclar) {
                    console.log(Sonuclar);
                    $('#' + Hedef).empty();
                    $.each(Sonuclar, function (index, Ilce) {
                        $('#' + Hedef).append($('<option/>', {
                            value: Ilce.No,
                            text: Ilce.Ad
                        }));
                    });
                },
                error: function () {
                    alert("Hata");
                },
                beforeSend: function () {
                },
                complete: function () {
                }
            });
        }
    });
}).apply(this, [jQuery]);



function OnayKutusuOnayAl(title, text, type, confirmButtonText, cancelButtonText, Url) {
    var sonuc = false;
    swal({
        title: title,
        text: text,
        type: type,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: confirmButtonText,
        cancelButtonText: cancelButtonText
    },
        function (isConfirm)
        {
            if (isConfirm) {
                window.location.replace(Url);
            }
        });
    return sonuc;
}


$(function ($) {
    //----Sol men� - url de�i�imine g�re selector i�lemleri------------------------------------
    var url = window.location.href;
    if ((url.split("/")[3] || url.split("/")[4]) != undefined) {
        var seciliUrl = "/" + url.split("/")[3] + "/" + url.split("/")[4];
        var hedef = $('nav#menu a[href="' + seciliUrl + '"]');
        hedef.addClass("text-bold text-primary");
        hedef.parent("li").parent("ul").parent("li").addClass("nav-active nav-expanded");
    }
    else {
        $('nav#menu a[href="/"]').parent("li").addClass("nav-active");
    }
    //-----------------------------------------------------------------------------------------
});


