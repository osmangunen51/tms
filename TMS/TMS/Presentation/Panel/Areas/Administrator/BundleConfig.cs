﻿using System.Web.Optimization;

namespace TMS.Panel.Web.Areas.Administrator
{
    internal static class BundleConfig
    {
        internal static void RegisterBundles(BundleCollection bundles)
        {
            #region Css Bundle

            bundles.Add(new StyleBundle("~/Administrator.Vendor/css").
                        Include
                        (
                            "~/Areas/Administrator/Theme/Default/vendor/bootstrap/css/bootstrap.css",
                            "~/Areas/Administrator/Theme/Default/vendor/font-awesome/css/font-awesome.css",
                            "~/Areas/Administrator/Theme/Default/vendor/font-awesome5/css/all.css",
                            "~/Areas/Administrator/Theme/Default/vendor/magnific-popup/magnific-popup.css",
                            "~/Areas/Administrator/Theme/Default/vendor/bootstrap-datepicker/css/datepicker3.css",
                            "~/Areas/Administrator/Theme/Default/vendor/sweetalert/sweetalert.css",
                            "~/Areas/Administrator/Theme/Default/vendor/jstree/themes/default/style.css"
                        )
                );
            bundles.Add(new StyleBundle("~/Administrator.CustomVendor/css").
                        Include
                        (
                            "~/Areas/Administrator/Theme/Default/vendor/select2/select2.css",
                            "~/Areas/Administrator/Theme/Default/vendor/bootstrap-multiselect/bootstrap-multiselect.css",
                            "~/Areas/Administrator/Theme/Default/vendor/jquery-datatables-bs3/assets/css/datatables.css",
                            "~/Areas/Administrator/Theme/Default/vendor/morris/morris.css",
                            "~/Areas/Administrator/Theme/Default/vendor/summernote/summernote.css",
                            "~/Areas/Administrator/Theme/Default/vendor/summernote/summernote-bs4.css",
                            "~/Areas/Administrator/Theme/Default/vendor/hover-css/hover.css",
                            "~/Areas/Administrator/Theme/Default/vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css",
                            "~/Areas/Administrator/Theme/Default/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css",
                            "~/Areas/Administrator/Theme/Default/vendor/tokenfield/css/tokenfield-typeahead.css",
                            "~/Areas/Administrator/Theme/Default/vendor/tokenfield/css/bootstrap-tokenfield.css"
                        )
                );
            bundles.Add(new StyleBundle("~/Administrator.Theme/css").
                        Include
                        (
                            "~/Areas/Administrator/Theme/Default/css/theme.css"
                        )
            );
            bundles.Add(new StyleBundle("~/Administrator.CustomTheme/css").
                                    Include
                                    (
                                        "~/Areas/Administrator/Theme/Default/css/theme-custom.css",
                                        "~/Areas/Administrator/Theme/Default/css/theme-custom.css"
                                    )
                            );

            #endregion Css Bundle

            #region Javascript Bundle

            bundles.Add(
                            new ScriptBundle("~/Administrator.modernizr/js").
                                Include(
                                        "~/Areas/Administrator/Theme/Default/vendor/modernizr/modernizr.js"
                                        )
                        );

            bundles.Add(
                           new ScriptBundle("~/Administrator.jquery/js").
                               Include(
                               "~/Areas/Administrator/Theme/Default/vendor/jquery/v1.12.0/jquery.js",
                               "~/Areas/Administrator/Theme/Default/vendor/jquery-ui/js/v1.11.4/jquery-ui.js",
                               "~/Areas/Administrator/Theme/Default/vendor/jquery-browser-mobile/jquery.browser.mobile.js",
                               "~/Areas/Administrator/Theme/Default/vendor/jquery-cookie/jquery.cookie.js"
                               ));

            bundles.Add(
                new ScriptBundle("~/Administrator.Vendor/js").
                    Include(
                    "~/Areas/Administrator/Theme/Default/vendor/jquery-ui/v1.10.4/js/jquery-ui-1.10.4.js",
                    //"~/Areas/Administrator/Theme/Default/vendor/style-switcher/style.switcher.js",
                    "~/Areas/Administrator/Theme/Default/vendor/bootstrap/js/bootstrap.js",
                    "~/Areas/Administrator/Theme/Default/vendor/nanoscroller/nanoscroller.js",
                    "~/Areas/Administrator/Theme/Default/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js",
                    "~/Areas/Administrator/Theme/Default/vendor/magnific-popup/magnific-popup.js",
                    "~/Areas/Administrator/Theme/Default/vendor/jquery-datatables/media/js/jquery.dataTables.js",
                    "~/Areas/Administrator/Theme/Default/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js",
                    "~/Areas/Administrator/Theme/Default/vendor/jquery-datatables-bs3/assets/js/datatables.js",
                    "~/Areas/Administrator/Theme/Default/vendor/jquery-placeholder/jquery.placeholder.js",
                    "~/Areas/Administrator/Theme/Default/vendor/summernote/summernote.js",
                    "~/Areas/Administrator/Theme/Default/vendor/TableExport/tableExport.js",
                    "~/Areas/Administrator/Theme/Default/vendor/TableExport/jquery.base64.js",
                    "~/Areas/Administrator/Theme/Default/vendor/TableExport/html2canvas.js",
                    "~/Areas/Administrator/Theme/Default/vendor/TableExport/jspdf/libs/sprintf.js",
                    "~/Areas/Administrator/Theme/Default/vendor/TableExport/jspdf/jspdf.js",
                    "~/Areas/Administrator/Theme/Default/vendor/TableExport/jspdf/libs/base64.js",
                    "~/Areas/Administrator/Theme/Default/vendor/rowsorter/rowsorter.js",
                    "~/Areas/Administrator/Theme/Default/vendor/sweetalert/sweetalert.js",
                    "~/Areas/Administrator/Theme/Default/vendor/jquery-countdown/jquery.countdown.js",
                    "~/Areas/Administrator/Theme/Default/vendor/jstree/jstree.js",
                    "~/Areas/Administrator/Theme/Default/vendor/clipboard/clipboard.min.js",
                    "~/Areas/Administrator/Theme/Default/vendor/store-js/store.js"
                    )
            );
            bundles.Add(
                new ScriptBundle("~/Administrator.CustomVendor/js").
                    Include(
                                "~/Areas/Administrator/Theme/Default/vendor/jquery-ui/v1.10.4/js/jquery-ui-1.10.4.js",
                                "~/Areas/Administrator/Theme/Default/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js",
                                "~/Areas/Administrator/Theme/Default/vendor/jquery-appear/jquery.appear.js",
                                "~/Areas/Administrator/Theme/Default/vendor/select2/select2.js",
                                "~/Areas/Administrator/Theme/Default/vendor/bootstrap-multiselect/bootstrap-multiselect.js",
                                "~/Areas/Administrator/Theme/Default/vendor/jquery-easypiechart/jquery.easypiechart.js",
                                "~/Areas/Administrator/Theme/Default/vendor/flot/jquery.flot.js",
                                "~/Areas/Administrator/Theme/Default/vendor/flot-tooltip/jquery.flot.tooltip.js",
                                "~/Areas/Administrator/Theme/Default/vendor/flot/jquery.flot.pie.js",
                                "~/Areas/Administrator/Theme/Default/vendor/flot/jquery.flot.categories.js",
                                "~/Areas/Administrator/Theme/Default/vendor/flot/jquery.flot.resize.js",
                                "~/Areas/Administrator/Theme/Default/vendor/jquery-sparkline/jquery.sparkline.js",
                                "~/Areas/Administrator/Theme/Default/vendor/raphael/raphael.js",
                                "~/Areas/Administrator/Theme/Default/vendor/morris/morris.js",
                                "~/Areas/Administrator/Theme/Default/vendor/gauge/gauge.js",
                                "~/Areas/Administrator/Theme/Default/vendor/snap-svg/snap.svg.js",
                                "~/Areas/Administrator/Theme/Default/vendor/liquid-meter/liquid.meter.js",
                                "~/Areas/Administrator/Theme/Default/vendor/jqvmap/jquery.vmap.js",
                                "~/Areas/Administrator/Theme/Default/vendor/jqvmap/data/jquery.vmap.sampledata.js",
                                "~/Areas/Administrator/Theme/Default/vendor/jqvmap/maps/jquery.vmap.world.js",
                                "~/Areas/Administrator/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.africa.js",
                                "~/Areas/Administrator/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.asia.js",
                                "~/Areas/Administrator/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.australia.js",
                                "~/Areas/Administrator/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.europe.js",
                                "~/Areas/Administrator/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js",
                                "~/Areas/Administrator/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js",
                                "~/Areas/Administrator/Theme/Default/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js",
                                "~/Areas/Administrator/Theme/Default/vendor/bootstrap-datetimepicker/js/moment.min.js",
                                "~/Areas/Administrator/Theme/Default/vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js",
                                "~/Areas/Administrator/Theme/Default/vendor/ios7-switch/ios7-switch.js",
                                "~/Areas/Administrator/Theme/Default/js/ui-elements/examples.modals.js",
                                "~/Areas/Administrator/Theme/Default/vendor/bootstrap-filestyle-1.3.0/bootstrap-filestyle.js",
                                "~/Areas/Administrator/Theme/Default/vendor/tokenfield/js/bootstrap-tokenfield.js"
                            )
            );
            bundles.Add(
                new ScriptBundle("~/Administrator.theme/js").
                    Include(
                            "~/Areas/Administrator/Theme/Default/js/theme.js"
                            )
            );
            bundles.Add(
               new ScriptBundle("~/Administrator.customtheme/js").
                   Include(
                           "~/Areas/Administrator/Theme/Default/js/theme.custom.js"
                           )
           );
            bundles.Add(
               new ScriptBundle("~/Administrator.theme_init/js").
                   Include(
                           "~/Areas/Administrator/Theme/Default/js/theme.init.js"
                           )
            );
            bundles.Add(
                new ScriptBundle("~/Administrator.SpecificPage/js").
                    Include(

                            )
            );

            bundles.Add(
                new ScriptBundle("~/Administrator.GoogleMap/js", "https://maps.googleapis.com/maps/api/js?key=AIzaSyDDFkrvHfVe77pUg0E-xpUc3zrXk7J-oks").
                    Include(
                    "~/Areas/Administrator/Theme/Default/js/gmaps.js"
                    )
            );

            #endregion Javascript Bundle

            bundles.Add(new ScriptBundle("~/Administrator.elfinder/js").Include(
                 "~/Areas/Administrator/Theme/Default/vendor/elfinder/jquery-1.9.1.js",
                 "~/Areas/Administrator/Theme/Default/vendor/elfinder/jquery-ui-1.10.2.js",
                 "~/Areas/Administrator/Theme/Default/vendor/elfinder/js/elfinder.full.js"
                 ));
            bundles.Add(new StyleBundle("~/Administrator.elfinder/css").Include(
                            "~/Areas/Administrator/Theme/Default/vendor/elfinder/css/elfinder.full.css",
                            "~/Areas/Administrator/Theme/Default/vendor/elfinder/css/theme.css"));
            BundleTable.EnableOptimizations = false;
        }
    }
}