﻿using System.Web.Mvc;
namespace TMS.Panel.Web.Areas.Administrator
{
    internal static class RouteConfig
    {
        internal static void RegisterRoutes(AreaRegistrationContext context)
        {
            context.MapRoute(
                             "Administrator_default",
                             "Administrator/{controller}/{action}/{id}",
                            defaults: new { controller = "Anasayfa", action = "Index", id = UrlParameter.Optional },
                            namespaces: new[] { "TMS.Panel.Web.Areas.Administrator.Controllers" }
                         );
        }
    }
}
