﻿using System.Web.Mvc;
using System.Web.Optimization;

namespace TMS.Panel.Web.Areas.Administrator
{
    public class AdministratorAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Administrator";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            RegisterRoutes(context);
            RegisterBundles();
        }

        private void RegisterRoutes(AreaRegistrationContext context)
        {
            Administrator.RouteConfig.RegisterRoutes(context);
        }

        private void RegisterBundles()
        {
            Administrator.BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}