﻿using System.Linq;
using System.Web.Mvc;
using TMS.Core.Service;
using TMS.Panel.Web.Code;

namespace TMS.Panel.Web.Areas.Administrator.Controllers
{
    public class AnasayfaController : AdministratorBaseController
    {
        private UsersService UsersService = new UsersService();
        private UserBildirimService UserBildirimService = new UserBildirimService();
        [Compress]
        public ActionResult Index()
        {
            SifreDegistirmeLayoutAyarla("~/Areas/Administrator/Views/Shared/_Layout.cshtml");
            TMS.View.Administrator.Anasayfa Model = new TMS.View.Administrator.Anasayfa();
            Model.User = UsersService.Get(x => x.UserId == User.UserId);
            Model.SurucuSayisi = UsersService.GetAll().Count();
            Model.UserBildirimListesi = UserBildirimService.UserGelenBildirimListesiGetir(User.UserId);
            Model.UserLogListesi = LogService.GetMany(x => x.UserNo == User.UserId).OrderByDescending(x => x.Date).Take(20).ToList();
            return View(Model);
        }

        [Compress]
        public ActionResult YapimAsamasinda()
        {
            return View();
        }
    }
}