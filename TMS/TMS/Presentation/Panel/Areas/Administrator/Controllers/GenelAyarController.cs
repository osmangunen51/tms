﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TMS.Core.Data;
using TMS.Core.Service;

namespace TMS.Panel.Web.Areas.Administrator.Controllers
{
    public class GenelAyarController : AdministratorBaseController
    {
        #region --# Servis Tanımları

        private readonly GenelAyarService GenelAyarService = new GenelAyarService();

        #endregion --# Servis Tanımları

        // GET: Administrator/Ayarlar
        public ActionResult Index()
        {
            TMS.View.Administrator.GenelAyar Model = new TMS.View.Administrator.GenelAyar();
            Model.AyarListesi = GenelAyarService.GetAll().ToList();
            return View(Model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(TMS.View.Administrator.GenelAyar Model, FormCollection Form)
        {
            switch (Request.Form["BtnIslem"])
            {
                case "Kaydet":
                    {
                        List<string> KeyListesi = new List<string>();
                        foreach (var Item in Form)
                        {
                            if (Item.ToString() != "BtnIslem")
                            {
                                string SystemAd = Item.ToString();
                                string Deger = Form[SystemAd].ToString();
                                Core.Data.GenelAyar GenelAyar = GenelAyarService.Get(x => x.SystemAd == SystemAd);
                                if (GenelAyar == null)
                                {
                                    GenelAyar = new Core.Data.GenelAyar()
                                    {
                                        SystemAd = SystemAd,
                                        Deger = Deger
                                    };
                                    GenelAyarService.Add(GenelAyar);
                                }
                                else
                                {
                                    GenelAyar.Deger = Deger;
                                    GenelAyarService.Update(GenelAyar);
                                }
                                GenelAyarService.Commit();
                                KeyListesi.Add(SystemAd);
                            }
                        }

                        List<Core.Data.GenelAyar> SilinecekAyarlar = GenelAyarService.GetMany(x => !KeyListesi.Contains(x.SystemAd)).ToList();
                        foreach (var SilGenelAyar in SilinecekAyarlar)
                        {
                            GenelAyarService.Delete(SilGenelAyar);
                        }
                        GenelAyarService.Commit();
                        Model.AyarListesi = GenelAyarService.GetAll().ToList();
                        break;
                    }
                default:
                    break;
            }
            return View(Model);
        }
    }
}