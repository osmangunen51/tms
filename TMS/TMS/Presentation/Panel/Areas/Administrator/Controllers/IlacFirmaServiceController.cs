using EEYS.View.Administrator;
using EEYS.Core.Service;
using System;
using System.Linq;
using System.Web.Mvc;
using EEYS.View.Genel;
using EEYS.Core.Lib;
using AutoMapper;

namespace EEYS.Panel.Web.Areas.Administrator.Controllers
{
    public class IlacFirmaServiceController : AdministratorBaseController
    {
        private readonly UsersService UsersService = new UsersService();
        private readonly RolesService RolesService = new RolesService();
        private readonly IlacFirmaServiceService IlacFirmaServiceService = new IlacFirmaServiceService();
        private readonly UserRolesService UserRolesService = new UserRolesService();
        public string MesajBaslikText{ get; set; } = "Mesaj Başlık";
        public string MesajGuncellemeText { get; set; } = "IlacFirmaService Başarıyla Güncellendi";
        public string MesajEklemeText { get; set; } = "IlacFirmaService Başarıyla Eklendi.";

        public string MesajSilText { get; set; } = "IlacFirmaService Başarıyla Slindi.";
        public string MesajBulunamadiText { get; set; } = "IlacFirmaService Başarıyla Slindi.";
        public string MesajHataText { get; set; } = "Hata Oluştu.";

        public IlacFirmaServiceController()
        {
        }

        public ActionResult Index()
        {
            EEYS.View.Administrator.IlacFirmaService Model = new EEYS.View.Administrator.IlacFirmaService();
            Model.AktifKayit = new EEYS.Core.Data.IlacFirmaService();
            Model.IlacFirmaServiceListesi = IlacFirmaServiceService.IlacFirmaServiceListesiGetir();
            return View(Model);
        }

        [HttpPost]
        public ActionResult Index(EEYS.View.Administrator.IlacFirmaService Model, FormCollection Frm)
        {
            Model.IlacFirmaServiceListesi =IlacFirmaServiceService.IlacFirmaServiceListesiGetir();
            return View(Model);
        }

        public ActionResult Delete(int id)
        {
            EEYS.View.Administrator.IlacFirmaService Model = new EEYS.View.Administrator.IlacFirmaService();
            var Aktif = IlacFirmaServiceService.GetById(id);
            if (Aktif != null)
            {
                try
                {
                    IlacFirmaServiceService.Delete(Aktif);
                    IlacFirmaServiceService.Commit();
                    Model.IslemMesaj.Visible = true;
                    Model.IslemMesaj.Tip = IslemMesajTip.Basarili;
                    Model.IslemMesaj.Baslik = MesajBaslikText;
                    Model.IslemMesaj.Mesaj = this.MesajSilText;
                }
                catch (Exception)
                {
                    Model.IslemMesaj.Visible = true;
                    Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                    Model.IslemMesaj.Baslik = this.MesajSilText;
                    Model.IslemMesaj.Mesaj = this.MesajHataText;
                }
            }
            else
            {
                Model.IslemMesaj.Visible = true;
                Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                Model.IslemMesaj.Baslik = this.MesajBaslikText;
                Model.IslemMesaj.Mesaj = this.MesajBulunamadiText;
            }
            return RedirectToAction("Index", "IlacFirmaService", Model);
        }

        public ActionResult Edit(int id)
        {
            EEYS.View.Administrator.IlacFirmaService Model = new EEYS.View.Administrator.IlacFirmaService();
            Model.AktifKayit = IlacFirmaServiceService.GetById(id);
            Model.IlacFirmaServiceListesi = IlacFirmaServiceService.IlacFirmaServiceListesiGetir();
            return View(Model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(EEYS.View.Administrator.IlacFirmaService Model, FormCollection Form)
        {
            switch (Request.Form["BtnIslem"])
            {
                case "Kaydet":
                    {
                        if (ModelState.IsValid)
                        {
                            try
                            {
                                Model.IslemMesaj.Visible = true;
                                Model.IslemMesaj.Tip = IslemMesajTip.Basarili;
                                Model.IslemMesaj.Baslik= this.MesajBaslikText;
                                if (Model.AktifKayit.No == 0)
                                {
                                    IlacFirmaServiceService.Create(Model.AktifKayit);
                                    Model.IslemMesaj.Mesaj = this.MesajEklemeText;
                                }
                                else
                                {
                                    Core.Data.IlacFirmaService Nesne = IlacFirmaServiceService.GetById(Model.AktifKayit.No);
                                    Nesne.Ad = Model.AktifKayit.Ad;
                                    Nesne.IlacNo = Model.AktifKayit.IlacNo;
                                    Nesne.Durum = Model.AktifKayit.Durum;
                                    IlacFirmaServiceService.Update(Nesne);
                                    Model.IslemMesaj.Mesaj = this.MesajGuncellemeText;
                                }
                                IlacFirmaServiceService.Commit();
                                Model.IlacFirmaServiceListesi = IlacFirmaServiceService.IlacFirmaServiceListesiGetir();
                                return View("Index", Model);
                            }
                            catch (Exception ex)
                            {
                                Model.IslemMesaj.Visible = true;
                                Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                                Model.IslemMesaj.Baslik = this.MesajBaslikText;
                                Model.IslemMesaj.Mesaj = "Hata! " + ex.Message.Replace("'"," ");
                                return View(Model);
                            }
                        }
                        else
                        {
                            Model.IslemMesaj.Visible = true;
                            Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                            Model.IslemMesaj.Baslik = this.MesajBaslikText;
                            Model.IslemMesaj.Mesaj = "Hatalar :  <br>" + ModelState.HataListesi().ListToString("<br>");
                            return View(Model);
                        }
                    }
                case "İptal Et":
                    {
                        return View("Index", Model);
                    }
                default:
                    break;
            }
            return View(Model);
        }

        public ActionResult Create()
        {
            EEYS.View.Administrator.IlacFirmaService Model = new EEYS.View.Administrator.IlacFirmaService();
            Model.AktifKayit = new EEYS.Core.Data.IlacFirmaService();
            Model.AktifKayit.Durum=(int)EEYS.View.Genel.Durum.Aktif;
            Model.IlacFirmaServiceListesi = IlacFirmaServiceService.IlacFirmaServiceListesiGetir();
            return View("Edit", Model);
        }
    }
}