﻿using System.Web.Mvc;
using TMS.Panel.Web.Areas.Administrator.Code;
using TMS.Panel.Web.Code;

namespace TMS.Panel.Web.Areas.Administrator.Controllers
{
    public class AdministratorController : AdministratorBaseController
    {
        [Compress]
        public ActionResult Index()
        {
            GenelView Model = new GenelView();
            return View("Index", Model);
        }
    }
}