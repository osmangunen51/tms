﻿using System;
using System.Web.Mvc;
using TMS.Core.Lib;
using TMS.Core.Service;
using TMS.View.Genel;
namespace TMS.Panel.Web.Areas.Administrator.Controllers
{
    public class KullaniciController : AdministratorBaseController
    {
        private KullaniciService KullaniciService = new KullaniciService();
        public KullaniciController()
        {
        }
        public ActionResult Index()
        {
            TMS.View.Administrator.Kullanici Model = new TMS.View.Administrator.Kullanici();
            Model.AktifKayit = new TMS.Core.Data.Kullanici();
            Model.KullaniciListesi = KullaniciService.KullaniciListesiGetir();
            return View(Model);
        }

        [HttpPost]
        public ActionResult Index(TMS.View.Administrator.Kullanici Model, FormCollection Frm)
        {
            Model.KullaniciListesi = KullaniciService.KullaniciListesiGetir();
            return View(Model);
        }

        public ActionResult Delete(int id)
        {
            TMS.View.Administrator.Kullanici Model = new TMS.View.Administrator.Kullanici();
            var Aktif = KullaniciService.GetById(id);
            if (Aktif != null)
            {
                try
                {
                    KullaniciService.Delete(Aktif);
                    KullaniciService.Commit();
                    Model.IslemMesaj.Visible = true;
                    Model.IslemMesaj.Tip = IslemMesajTip.Basarili;
                    Model.IslemMesaj.Baslik = "Silme İşlemleri";
                    Model.IslemMesaj.Mesaj = "Başarıyla Silindi.";

                }
                catch (Exception)
                {
                    Model.IslemMesaj.Visible = true;
                    Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                    Model.IslemMesaj.Baslik = "Silme İşlemleri";
                    Model.IslemMesaj.Mesaj = "Hata Oluştu.";
                }
            }
            else
            {
                Model.IslemMesaj.Visible = true;
                Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                Model.IslemMesaj.Baslik = "Silme İşlemleri";
                Model.IslemMesaj.Mesaj = "Bulunamadı";
            }
            return RedirectToAction("Index", "Kullanici", Model);
        }
        public ActionResult Edit(int id)
        {
            TMS.View.Administrator.Kullanici Model = new TMS.View.Administrator.Kullanici();
            Model.AktifKayit = KullaniciService.GetById(id);
            return View(Model);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(TMS.View.Administrator.Kullanici Model, FormCollection Frm)
        {
            switch (Request.Form["BtnIslem"])
            {
                case "Kaydet":
                    {
                        if (ModelState.IsValid)
                        {
                            try
                            {
                                Model.IslemMesaj.Visible = true;
                                Model.IslemMesaj.Tip = IslemMesajTip.Basarili;
                                Model.IslemMesaj.Baslik = "Kullanıcı İşlemleri";

                                if (Model.AktifKayit.No == 0)
                                {
                                    KullaniciService.Create(Model.AktifKayit);
                                    Model.IslemMesaj.Mesaj = "Kullanıcı Başarıyla Eklendi.";
                                }
                                else
                                {
                                    var Nesne = KullaniciService.GetById(Model.AktifKayit.No);
                                    Nesne.Ad = Model.AktifKayit.Ad;
                                    Nesne.Soyad = Model.AktifKayit.Ad;
                                    Nesne.Mail = Model.AktifKayit.Mail;
                                    KullaniciService.Update(Model.AktifKayit);
                                    Model.IslemMesaj.Mesaj = "Kullanıcı Başarıyla Güncellendi.";
                                }
                                KullaniciService.Commit();
                                Model.KullaniciListesi = KullaniciService.KullaniciListesiGetir();
                                return View("Index", Model);
                            }
                            catch (Exception ex)
                            {
                                Model.IslemMesaj.Visible = true;
                                Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                                Model.IslemMesaj.Baslik = "Kullanıcı İşlemleri";
                                Model.IslemMesaj.Mesaj = "Hata! " + ex.Message;
                                return View(Model);
                            }
                        }
                        else
                        {
                            Model.IslemMesaj.Visible = true;
                            Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                            Model.IslemMesaj.Baslik = "Kullanıcı İşlemleri";
                            Model.IslemMesaj.Mesaj = "Hatalar :<br>" + ModelState.HataListesi().ListToString("<br>");
                            return View(Model);
                        }
                    }
                case "İptal Et":
                    {
                        Model.KullaniciListesi = KullaniciService.KullaniciListesiGetir();
                        return View("Index", Model);
                    }
                default:
                    break;
            }
            return View(Model);
        }
        public ActionResult Create()
        {
            TMS.View.Administrator.Kullanici Model = new TMS.View.Administrator.Kullanici();
            Model.AktifKayit = new TMS.Core.Data.Kullanici();
            Model.AktifKayit.Tarih = DateTime.Now;
            Model.AktifKayit.Durum = (short)Durum.Aktif;
            return View("Edit", Model);
        }
    }
}
