﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TMS.Core.Service;
using TMS.Panel.Web.Code;
using TMS.Panel.Web.Code.Security;
using TMS.View.Genel;

namespace TMS.Panel.Web.Areas.Administrator.Controllers
{
    public class UsersController : AdministratorBaseController
    {
        private readonly UsersService UsersService = new UsersService();
        private RolesService RolesService = new RolesService();
        private UserRolesService UserRolesService = new UserRolesService();
        public UsersController()
        {
        }

        [Compress]
        public ActionResult Profil(int Id)
        {
            TMS.View.Genel.Profil Model = new TMS.View.Genel.Profil();
            Model.AktifKayit = UsersService.GetById(Id);
            return View(Model);
        }


        [HttpPost]
        [ValidateInput(false)]
        [Compress]
        public ActionResult Profil(TMS.View.Genel.Profil Model)
        {
            switch (Request.Form["BtnIslem"])
            {
                case "Kaydet":
                    {
                        ModelState.Remove("AktifKayit.Password");
                        if (ModelState.IsValid)
                        {
                            try
                            {
                                Model.IslemMesaj.Visible = true;
                                Model.IslemMesaj.Tip = IslemMesajTip.Basarili;
                                Model.IslemMesaj.Baslik = "User Profil İşlemleri";
                                Core.Data.Users Nesne = UsersService.GetById(Model.AktifKayit.UserId);
                                if (Nesne != null)
                                {
                                    Nesne.FirstName = Model.AktifKayit.FirstName;
                                    Nesne.LastName = Model.AktifKayit.LastName;
                                    Nesne.Email = Model.AktifKayit.Email;
                                    UsersService.Update(Nesne);
                                    Model.IslemMesaj.Mesaj = "User Profili Başarıyla Güncellendi.";
                                    UsersService.Commit();
                                    Model.AktifKayit = Nesne;

                                    List<TMS.Core.Data.Roles> RolListesi = UsersService.RolListesiGetir(Model.AktifKayit.UserId);
                                    CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
                                    serializeModel.UserId = Model.AktifKayit.UserId;
                                    serializeModel.FirstName = Model.AktifKayit.FirstName;
                                    serializeModel.LastName = Model.AktifKayit.LastName;
                                    serializeModel.roles = RolListesi.Select(x => x.RoleName).ToArray();
                                    serializeModel.Image = Model.AktifKayit.Image;
                                    serializeModel.Telefon = Model.AktifKayit.Telefon;
                                    serializeModel.UserName = Model.AktifKayit.Username;
                                    string userData = JsonConvert.SerializeObject(serializeModel);
                                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                                             1,
                                             Model.AktifKayit.Email,
                                             DateTime.Now,
                                             DateTime.Now.AddMinutes(15),
                                             false,
                                             userData);
                                    string encTicket = FormsAuthentication.Encrypt(authTicket);
                                    HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                                    Response.Cookies.Add(faCookie);

                                }
                                Response.Redirect("~/Administrator/Anasayfa");
                                return View("Index", Model);
                            }
                            catch (Exception ex)
                            {
                                Model.IslemMesaj.Visible = true;
                                Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                                Model.IslemMesaj.Baslik = "User Profil İşlemleri";
                                Model.IslemMesaj.Mesaj = "Hata! " + ex.Message;
                                return View(Model);
                            }
                        }
                        break;
                    }
                case "İptal Et":
                    {
                        Response.Redirect(TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl(string.Format("~/Administrator/Anasayfa/Index")));
                        return View("Index", Model);
                    }
                default:
                    break;
            }
            return View(Model);
        }



        public ActionResult Index()
        {
            TMS.View.Administrator.Users Model = new TMS.View.Administrator.Users();
            Model.AktifKayit = new TMS.Core.Data.Users();
            Model.UsersListesi = UsersService.UsersListesiGetir("").ToList();
            return View(Model);
        }

        [HttpPost]
        public ActionResult Index(TMS.View.Administrator.Users Model, FormCollection Frm)
        {
            Model.UsersListesi = UsersService.UsersListesiGetir("").ToList();
            return View(Model);
        }
        public ActionResult Delete(int id)
        {
            TMS.View.Administrator.Users Model = new TMS.View.Administrator.Users();
            var Aktif = UsersService.GetById(id);
            if (Aktif != null)
            {
                try
                {
                    UsersService.Delete(Aktif);
                    UsersService.Commit();
                    Model.IslemMesaj.Visible = true;
                    Model.IslemMesaj.Tip = IslemMesajTip.Basarili;
                    Model.IslemMesaj.Baslik = "Silme İşlemleri";
                    Model.IslemMesaj.Mesaj = "Başarıyla Silindi.";
                }
                catch (Exception)
                {
                    Model.IslemMesaj.Visible = true;
                    Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                    Model.IslemMesaj.Baslik = "Silme İşlemleri";
                    Model.IslemMesaj.Mesaj = "Hata Oluştu.";
                }
            }
            else
            {
                Model.IslemMesaj.Visible = true;
                Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                Model.IslemMesaj.Baslik = "Silme İşlemleri";
                Model.IslemMesaj.Mesaj = "Bulunamadı";
            }
            return RedirectToAction("Index", "Users", Model);
        }
        public ActionResult Edit(int id)
        {
            TMS.View.Administrator.Users Model = new TMS.View.Administrator.Users();
            Model.AktifKayit = UsersService.GetById(id);
            Model.RolNoListesi = UsersService.RolListesiGetir(Model.AktifKayit.UserId).Select(x => x.RoleId).ToArray();
            Model.RolListesi = RolesService.GetAll().ToList();
            return View(Model);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(TMS.View.Administrator.Users Model, FormCollection Frm)
        {
            switch (Request.Form["BtnIslem"])
            {
                case "Kaydet":
                    {
                        if (ModelState.IsValid)
                        {
                            try
                            {
                                Model.IslemMesaj.Visible = true;
                                Model.IslemMesaj.Tip = IslemMesajTip.Basarili;
                                Model.IslemMesaj.Baslik = "Users İşlemleri";
                                int DurumKontrol = UsersService.UsersListesiGetir("").Where(x => x.Email == Model.AktifKayit.Email).Count();
                                bool IslemDurum = true;

                                if (Model.AktifKayit.UserId == 0)
                                {
                                    if (DurumKontrol == 0)
                                    {
                                        Model.AktifKayit.Username = Model.AktifKayit.Email;
                                        Model.AktifKayit.CreateDate = DateTime.Now;
                                        UsersService.Create(Model.AktifKayit);
                                        UsersService.Commit();
                                        UserRolesService.Create(new Core.Data.UserRoles() { User_UserId = Model.AktifKayit.UserId, Role_RoleId = RolesService.Get(x => x.RoleName == "Avm").RoleId });
                                        UserRolesService.Commit();
                                        Model.IslemMesaj.Mesaj = "Users Başarıyla Eklendi.";
                                    }
                                    else
                                    {
                                        IslemDurum = false;
                                    }
                                }
                                else
                                {
                                    if (DurumKontrol < 2)
                                    {
                                        Core.Data.Users Nesne = UsersService.GetById(Model.AktifKayit.UserId);
                                        if (Nesne != null)
                                        {
                                            Nesne.FirstName = Model.AktifKayit.FirstName;
                                            Nesne.LastName = Model.AktifKayit.LastName;
                                            Nesne.Email = Model.AktifKayit.Email;
                                            Nesne.Telefon = Model.AktifKayit.Telefon;
                                            Nesne.IsActive = Model.AktifKayit.IsActive;
                                            Nesne.Password = Model.AktifKayit.Password;
                                            Nesne.Image = Model.AktifKayit.Image;
                                            UsersService.Update(Nesne);
                                            UsersService.Commit();
                                            Model.IslemMesaj.Mesaj = "Users Başarıyla Güncellendi.";
                                        }
                                    }
                                    else
                                    {
                                        IslemDurum = false;
                                    }
                                }
                                if (IslemDurum)
                                {
                                    Model.UsersListesi = UsersService.UsersListesiGetir("").ToList();
                                    Model.RolListesi = RolesService.GetAll().ToList();
                                    foreach (var RolNo in Model.RolNoListesi)
                                    {
                                        Core.Data.UserRoles UserRole = UserRolesService.Get(x => x.Role_RoleId == RolNo && x.User_UserId == Model.AktifKayit.UserId);
                                        if (UserRole == null)
                                        {
                                            UserRole = new Core.Data.UserRoles();
                                            UserRole.Role_RoleId = RolNo;
                                            UserRole.User_UserId = Model.AktifKayit.UserId;
                                            UserRolesService.Add(UserRole);
                                            UserRolesService.Commit();
                                        }
                                    }
                                    return View("Index", Model);
                                }
                                else
                                {
                                    Model.IslemMesaj.Visible = true;
                                    Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                                    Model.IslemMesaj.Baslik = "Users İşlemleri";
                                    Model.IslemMesaj.Mesaj = "Bu Kayıt Zaten Var.";
                                    return View(Model);
                                }
                            }
                            catch (Exception ex)
                            {
                                Model.IslemMesaj.Visible = true;
                                Model.IslemMesaj.Tip = IslemMesajTip.Hata;
                                Model.IslemMesaj.Baslik = "Users İşlemleri";
                                Model.IslemMesaj.Mesaj = "Hata! " + ex.Message;
                                return View(Model);
                            }
                        }
                        break;
                    }
                case "İptal Et":
                    {
                        Model.UsersListesi = UsersService.UsersListesiGetir("").ToList();
                        Model.RolListesi = RolesService.GetAll().ToList();
                        return View("Index", Model);
                    }
                default:
                    break;
            }
            return View(Model);
        }
        public ActionResult Create()
        {
            TMS.View.Administrator.Users Model = new TMS.View.Administrator.Users();
            Model.RolNoListesi = UsersService.RolListesiGetir(Model.AktifKayit.UserId).Select(x => x.RoleId).ToArray();
            Model.RolListesi = RolesService.GetAll().ToList();
            Model.AktifKayit = new TMS.Core.Data.Users();
            Model.AktifKayit.IsActive = true;
            return View("Edit", Model);
        }
    }
}
