﻿using TMS.Panel.Web.Code.Security;

namespace TMS.Panel.Web.Areas.Administrator.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class AdministratorBaseController : BaseController
    {
    }
}