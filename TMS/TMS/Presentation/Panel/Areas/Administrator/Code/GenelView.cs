﻿using TMS.View.Genel;

namespace TMS.Panel.Web.Areas.Administrator.Code
{
    public class GenelView : BaseView
    {
        public GenelView()
        {
            IslemMesaj = new View.Genel.IslemMesaj();
        }
    }
}