﻿namespace TMS.Panel.Web
{
    using Newtonsoft.Json;
    using System;
    using System.Configuration;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using TMS.Panel.Web.App_Start;
    using TMS.Panel.Web.Code;
    using TMS.Panel.Web.Code.Security;
    using TMS.Panel.Web.Code.ZamanlanmisGorevler.Zamanlayicilar;
    using TMS.Panel.Web.Mappings;
    using TMS.View.Genel;

    /// <summary>
    /// Defines the <see cref="MvcApplication" />
    /// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {

        public void DefaultLocale()
        {
            HttpCookie cookie = Request.Cookies.Get("CacheLang");
            if (cookie == null)
            {
                HttpCookie newCookie = new HttpCookie("CacheLang");
                newCookie.Value = "tr_TR";
                Response.Cookies.Add(newCookie);
            }
        }


        /// <summary>
        /// Defines the LogService
        /// </summary>
        private Core.Service.LogsService LogService = new Core.Service.LogsService();

        /// <summary>
        /// The Application_Start
        /// </summary>
        protected void Application_Start()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
            DevExpress.XtraReports.Web.WebDocumentViewer.Native.WebDocumentViewerBootstrapper.SessionState = System.Web.SessionState.SessionStateBehavior.Default;
            AreaRegistration.RegisterAllAreas();
            if (ConfigurationManager.AppSettings["PerformansKayitDurum"] == "true")
            {
                GlobalFilters.Filters.Add(new PerformanceFilter());
            }
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfiguration.Configure();
            //ZamanlayiciVeriEsitleme.Start();
            ZamanlayiciBilgilendirme.Start();
        }

        /// <summary>
        /// The Application_BeginRequest
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="EventArgs"/></param>
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (!HttpContext.Current.Request.Url.ToString().ToLower().Contains("/uygulama") && !HttpContext.Current.Request.Url.ToString().ToLower().Contains("browserlink"))
            {
                if (ConfigurationManager.AppSettings["BakimModu"] == "true")
                {
                    if (!Request.IsLocal)
                    {
                        HttpContext.Current.RewritePath("~/Bakim/index.html");
                    }
                    else
                    {
                        HttpContext.Current.RewritePath("~/Bakim/index.html");
                    }
                }
            }
        }

        /// <summary>
        /// The Application_PostAuthenticateRequest
        /// </summary>
        /// <param name="sender">The <see cref="Object"/></param>
        /// <param name="e">The <see cref="EventArgs"/></param>
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            try
            {
                HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                if (authCookie != null)
                {
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    CustomPrincipalSerializeModel serializeModel = JsonConvert.DeserializeObject<CustomPrincipalSerializeModel>(authTicket.UserData);
                    CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
                    newUser.UserId = serializeModel.UserId;
                    newUser.FirstName = serializeModel.FirstName;
                    newUser.LastName = serializeModel.LastName;
                    newUser.UserName = serializeModel.UserName;
                    newUser.Image = serializeModel.Image;
                    newUser.BayiListesi = serializeModel.BayiListesi;
                    newUser.roles = serializeModel.roles;
                    newUser.Folder = serializeModel.Folder;
                    newUser.SubFolder = serializeModel.SubFolder;
                    newUser.Telefon = serializeModel.Telefon;
                    HttpContext.Current.User = newUser;
                }
            }
            catch (Exception)
            {
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            DefaultLocale();

        }
        /// <summary>
        /// The Session_End
        /// </summary>
        /// <param name="sender">The <see cref="object"/></param>
        /// <param name="e">The <see cref="EventArgs"/></param>
        protected void Session_End(object sender, EventArgs e)
        {
            if (HttpContext.Current != null)
            {
                // Code that runs when a new session is started
                if (HttpContext.Current.User != null)
                {
                    CustomPrincipal user = (CustomPrincipal)HttpContext.Current.User;
                    LogService.Ekle(user.UserId, HttpContext.Current.Request.UserHostAddress, LogTip.Cikis, string.Format("{0} ({1} {1}) Kullanıcısı Çıkış Yaptı", user.UserName, user.FirstName, user.LastName));
                }
            }
        }
    }
}