﻿using System.Web.Mvc;
namespace TMS.Panel.Web.Controllers
{
    public class YetkiController : Controller
    {
        // GET: Kullanici/Yetki
        public ActionResult ErisimYok()
        {
            return View();
        }

        public ActionResult Kisitlama(string Url)
        {
            return View(Url);
        }
    }
}
