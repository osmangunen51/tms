﻿using System.Web.Mvc;

namespace TMS.Panel.Web.Controllers
{
    public class AnasayfaController : BaseController
    {
        public ActionResult Index()
        {
            return RedirectPermanent(TMS.Panel.Web.Araclar.HtmlHelpers.ResolveUrl("~/Platform/Account/Index"));
            //return View();
        }
    }
}
