﻿using System;
using System.Web.Mvc;

namespace TMS.Panel.Web.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            Exception exception = Server.GetLastError();
            Server.ClearError();
            return View(exception);
        }
    }
}