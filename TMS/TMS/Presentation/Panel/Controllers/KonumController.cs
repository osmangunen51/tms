﻿using System.Linq;
using System.Web.Mvc;
namespace TMS.Panel.Web.Areas.Administrator.Controllers
{
    public class KonumController : Controller
    {
        private TMS.Core.Service.IlceService IlceService = new TMS.Core.Service.IlceService();
        // GET: Konum
        [HttpPost]
        public JsonResult IlceListesiGetir(int IlNo)
        {
            var data = IlceService.GetMany(x => x.IlNo == IlNo).OrderBy(x => x.Ad).Select(
                x => new { No = x.No, Ad = x.Ad }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
