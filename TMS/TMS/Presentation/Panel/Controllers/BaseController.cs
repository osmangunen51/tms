﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using TMS.Panel.Web.Code.Security;

namespace TMS.Panel.Web.Controllers
{
    public class BaseController : Controller
    {
        public Core.Service.LogsService LogService = new Core.Service.LogsService();
        private string _Ip = "";

        public string Ip
        {
            get
            {
                _Ip = HttpContext.Request.UserHostAddress;
                return _Ip;
            }
            set
            {
                _Ip = value;
            }
        }

        protected virtual new CustomPrincipal User
        {
            get { return HttpContext.User as CustomPrincipal; }
        }

        public void SifreDegistirmeLayoutAyarla(string Layout = "")
        {
            Session["ChangePasswordLayout"] = Layout;
        }
        //protected virtual new DataTable SonYuklenenExcelListesi
        //{
        //    get { return SonYuklenenExcelTablosunuGetir(); }
        //}

        public TMS.Core.Lib.IslemDurum SistemMesajGonder(string Baslik, string Mesaj)
        {
            TMS.Core.Lib.IslemDurum Sonuc = new Core.Lib.IslemDurum();
            try
            {
                bool GonderimDurum = Convert.ToBoolean(ConfigurationManager.AppSettings["SistemBilgilendirme:Durum"].ToString());
                if (GonderimDurum)
                {
                    using (TMS.Core.Lib.EmailIstemci Istemci = new Core.Lib.EmailIstemci()
                    {
                        ServerAd = ConfigurationManager.AppSettings["Smtp:ServerAd"],
                        KullaniciAd = ConfigurationManager.AppSettings["Smtp:KullaniciAd"],
                        Sifre = ConfigurationManager.AppSettings["Smtp:Sifre"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Smtp:Port"].ToString()),
                        SslDurum = Convert.ToBoolean(ConfigurationManager.AppSettings["Smtp:Ssl"].ToString())
                    })
                    {
                        Istemci.Mail.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["Smtp:KullaniciAd"], "TMS.com");
                        Istemci.Mail.Bcc.Add(ConfigurationManager.AppSettings["SistemBilgilendirme:MailListesi"]);
                        Istemci.Mail.Subject = string.Format("Sistem:{0} ", Baslik);
                        Istemci.Mail.BodyEncoding = System.Text.Encoding.UTF8;
                        Istemci.Mail.SubjectEncoding = System.Text.Encoding.UTF8;
                        Istemci.Mail.Body = Mesaj;
                        TMS.Core.Lib.IslemDurum IslemSonuc = Istemci.MailGonder();
                    }
                }
                else
                {
                    Sonuc.Durum = true;
                    Sonuc.Tip = Core.Lib.IslemDurum.IslemMesajTip.Basarili;
                    Sonuc.Mesaj = "Mesaj Gönderilme Ayarı Kapalı";
                }
            }
            catch (Exception Hata)
            {
                Sonuc.Durum = false;
                Sonuc.Hata = Hata;
                Sonuc.Tip = Core.Lib.IslemDurum.IslemMesajTip.Hata;
                Sonuc.Mesaj = string.Format("Hata Oluştu : {0}", Hata.Message);
            }
            return Sonuc;
        }

        public TMS.Core.Lib.IslemDurum SistemSmsGonder(string No, string Mesaj)
        {
            TMS.Core.Lib.IslemDurum Sonuc = new TMS.Core.Lib.IslemDurum();
            try
            {
                using (TMS.Core.Service.SmsService SmsService = new TMS.Core.Service.SmsService())
                {
                    var Snc = SmsService.Gonder(No, Mesaj);
                    if (Snc.Durum)
                    {
                        Sonuc.Durum = true;
                        Sonuc.Baslik = "Sms Gönderimi";
                        Sonuc.Tip = Core.Lib.IslemDurum.IslemMesajTip.Basarili;
                        Sonuc.Mesaj = "Başarıyla Gönderildi.";
                    }
                    else
                    {
                        Sonuc.Durum = false;
                        Sonuc.Baslik = "Sms Gönderimi";
                        Sonuc.Tip = Core.Lib.IslemDurum.IslemMesajTip.Hata;
                        Sonuc.Mesaj = "Başarıyla Gönderildi.";
                    }
                }
            }
            catch (Exception Hata)
            {
                Sonuc.Durum = false;
                Sonuc.Baslik = "Sms Gönderimi";
                Sonuc.Tip = Core.Lib.IslemDurum.IslemMesajTip.Hata;
                Sonuc.Mesaj = "Genel Hata Oluştu.";
                Sonuc.Deger = Hata;
            }
            return Sonuc;
        }



        public TMS.Core.Lib.IslemDurum SistemMailGonder(List<string> AdresListesi, string Baslik, string Mesaj)
        {
            TMS.Core.Lib.IslemDurum Sonuc = new Core.Lib.IslemDurum();
            try
            {
                bool GonderimDurum = Convert.ToBoolean(ConfigurationManager.AppSettings["SistemBilgilendirme:Durum"].ToString());
                if (GonderimDurum)
                {
                    using (TMS.Core.Lib.EmailIstemci Istemci = new Core.Lib.EmailIstemci()
                    {
                        ServerAd = ConfigurationManager.AppSettings["Smtp:ServerAd"],
                        KullaniciAd = ConfigurationManager.AppSettings["Smtp:KullaniciAd"],
                        Sifre = ConfigurationManager.AppSettings["Smtp:Sifre"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Smtp:Port"].ToString()),
                        SslDurum = Convert.ToBoolean(ConfigurationManager.AppSettings["Smtp:Ssl"].ToString())
                    })
                    {
                        Istemci.Mail.Bcc.Clear();
                        Istemci.Mail.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["Smtp:KullaniciAd"], "öğretmenlikuygulamalari.com");
                        Istemci.Mail.Bcc.Add(string.Join(",", AdresListesi));
                        Istemci.Mail.Subject = Baslik;
                        Istemci.Mail.BodyEncoding = System.Text.Encoding.UTF8;
                        Istemci.Mail.SubjectEncoding = System.Text.Encoding.UTF8;
                        Istemci.Mail.Body = Mesaj;
                        Istemci.Mail.IsBodyHtml = true;
                        Sonuc = Istemci.MailGonder();

                    }
                }
                else
                {
                    Sonuc.Durum = true;
                    Sonuc.Tip = Core.Lib.IslemDurum.IslemMesajTip.Basarili;
                    Sonuc.Mesaj = "Mesaj Gönderilme Ayarı Kapalı";
                }
            }
            catch (Exception Hata)
            {
                Sonuc.Durum = false;
                Sonuc.Hata = Hata;
                Sonuc.Tip = Core.Lib.IslemDurum.IslemMesajTip.Hata;
                Sonuc.Mesaj = string.Format("Hata Oluştu : {0}", Hata.Message);
            }
            return Sonuc;
        }


    }
}