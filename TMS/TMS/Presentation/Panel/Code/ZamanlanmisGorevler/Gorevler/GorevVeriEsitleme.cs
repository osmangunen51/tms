﻿using Quartz;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

namespace TMS.Panel.Web.Code.ZamanlanmisGorevler.Gorevler
{
    public class GorevVeriEsitleme : IJob
    {

        public Core.Service.SiparisService SiparisService = new Core.Service.SiparisService();
        public Core.Service.TokenService TokenService = new Core.Service.TokenService();
        public Core.Service.LogsService LogService = new Core.Service.LogsService();
        public string BaglantiTxt { get; set; } = "";
        public void Execute(IJobExecutionContext context)
        {
            BaglantiTxt = ConfigurationManager.ConnectionStrings["EntitiesMysql"].ToString();
            List<TMS.View.Genel.SiparisListItem> SiparisListesi = SiparisService.TumSiparisSatisListesiGetir().ToList();
            using (TMS.Core.Mysql.Data.Istemci Istemci = new TMS.Core.Mysql.Data.Istemci(BaglantiTxt))
            {
                var SorguIslem = Istemci.KomutCalistirWithExecuteReader(@"SELECT
                Siparis.token,
                Siparis.girischip,
                Siparis.sonchip,
                Siparis.aciklama
                FROM
                Siparis
                ", new System.Collections.Generic.Dictionary<string, object>());
                DataTable SncTablo = (DataTable)SorguIslem.Deger;
                foreach (DataRow Satir in SncTablo.Rows)
                {
                    var token = Satir["token"].ToString();
                    var girischip = Satir["girischip"].ToString();
                    var sonchip = Satir["sonchip"].ToString();
                    var aciklama = Satir["aciklama"].ToString();
                    var kaynaktoken = Satir["sifre"].ToString();
                    var kaynaktokenkalanmiktar = Satir["kaynaktokenkalanmiktar"].ToString();
                    var Nesne = SiparisListesi.Where(x => x.Siparis.HedefTokenNo== token).FirstOrDefault();
                    if (Nesne!=null)
                    {
                        var AktifSiparis = Nesne.Siparis;
                        if (!string.IsNullOrEmpty(girischip))
                        {
                            AktifSiparis.GirisMiktar = System.Convert.ToDecimal(girischip);
                        }
                        else
                        {
                            AktifSiparis.GirisMiktar = 0;
                        }
                        if (!string.IsNullOrEmpty(sonchip))
                        {
                            AktifSiparis.GuncelToplam = System.Convert.ToDecimal(sonchip);
                        }
                        else {
                            AktifSiparis.GuncelToplam = 0;
                        }
                        AktifSiparis.Aciklama = aciklama;
                        SiparisService.Update(AktifSiparis);
                        SiparisService.Commit();
                    }

                    var TokenNesne = TokenService.Get(x => x.Ad == kaynaktoken);
                    if (TokenNesne!=null)
                    {
                        decimal SonKalanMiktar = 0;
                        if (!string.IsNullOrEmpty(kaynaktokenkalanmiktar))
                        {
                            TokenNesne.SonKalanMiktar = System.Convert.ToDecimal(kaynaktokenkalanmiktar);
                        }
                        else
                        {
                            TokenNesne.SonKalanMiktar = 0;
                        }
                        TokenService.Update(TokenNesne);
                        TokenService.Commit();
                    }
                }

                Dictionary<string, object> PrmListesi = new Dictionary<string, object>();
                foreach (var item in SiparisListesi)
                {
                    var Nesne = SncTablo.Select($"token='{item.Siparis.HedefTokenNo}'");
                    if (Nesne.Count()==0)
                    {
                        PrmListesi = new Dictionary<string, object>();
                        PrmListesi.Add("@token", item.Siparis.HedefTokenNo);
                        PrmListesi.Add("@miktar", item.Siparis.Miktar);
                        PrmListesi.Add("@durum", "0");
                        PrmListesi.Add("@name", item.User.Username);
                        PrmListesi.Add("@mail", item.User.Email);
                        PrmListesi.Add("@sifre", item.KaynakToken.Ad);
                        PrmListesi.Add("@kaynaktokenkalanmiktar", item.Siparis.TokenKalanMiktar);

                        SorguIslem = Istemci.KomutCalistirWithNonQuery(@"
                        Insert INTO Siparis
                        (
                            Siparis.token,
                            Siparis.siparis,
                            Siparis.durum,
                            Siparis.name,
                            Siparis.mail,
                            Siparis.sifre,
                            Siparis.kaynaktokenkalanmiktar
                        )
                        VALUES(
                            @token,
                            @miktar,
                            @durum,
                            @name,
                            @mail,
                            @sifre,
                            @kaynaktokenkalanmiktar
                        )", PrmListesi);
                        if (SorguIslem.Durum)
                        {

                        }
                    }
                }
            }
        }
    }
}