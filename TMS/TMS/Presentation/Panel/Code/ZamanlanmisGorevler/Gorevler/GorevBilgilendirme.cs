﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using TMS.View.Genel;

namespace TMS.Web.Code.ZamanlanmisGorevler.Gorevler
{
    public class GorevBilgilendirme : IJob
    {
        public TMS.Core.Service.LogsService LogService = new TMS.Core.Service.LogsService();
        public Core.Service.BilgilendirmeService BilgilendirmeService = new Core.Service.BilgilendirmeService();

        public void Execute(IJobExecutionContext context)
        {
            int DenemeSayisi = 3;
            if (ConfigurationManager.AppSettings["Bilgilendirme:DenemeSayisi"] != null)
            {
                DenemeSayisi = Convert.ToInt32(ConfigurationManager.AppSettings["Bilgilendirme:DenemeSayisi"].ToString());
            }

            bool IslemYapilamDurumu = false;
            if (ConfigurationManager.AppSettings["Bilgilendirme:Durum"] != null)
            {
                IslemYapilamDurumu = Convert.ToBoolean(ConfigurationManager.AppSettings["Bilgilendirme:Durum"].ToString());
            }
            if (IslemYapilamDurumu)
            {
               
            }
        }
    }
}