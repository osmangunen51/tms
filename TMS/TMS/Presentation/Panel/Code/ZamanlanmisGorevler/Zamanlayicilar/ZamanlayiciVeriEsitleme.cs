﻿using Quartz;
using Quartz.Impl;
using System.Web;
namespace TMS.Panel.Web.Code.ZamanlanmisGorevler.Zamanlayicilar
{
    public class ZamanlayiciVeriEsitleme
    {
        public static void Start()
        {
            int Zaman = 1;
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();
            IJobDetail job = JobBuilder.Create<Gorevler.GorevVeriEsitleme>().UsingJobData("HttpContext", HttpContext.Current.Server.MapPath("")).Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule
                  (s =>
                      s.WithIntervalInMinutes(Zaman)
                  )
                .Build();
            scheduler.ScheduleJob(job, trigger);
        }
    }
}
