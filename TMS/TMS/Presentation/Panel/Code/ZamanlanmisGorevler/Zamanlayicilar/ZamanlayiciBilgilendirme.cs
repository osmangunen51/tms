﻿using Quartz;
using Quartz.Impl;
using System;
using System.Configuration;
using System.Web;
using TMS.Web.Code.ZamanlanmisGorevler.Gorevler;

namespace TMS.Panel.Web.Code.ZamanlanmisGorevler.Zamanlayicilar
{
    public class ZamanlayiciBilgilendirme
    {
        public static void Start()
        {
            int Zaman = 10;
            if (ConfigurationManager.AppSettings["Bilgilendirme:Zaman"] != null)
            {
                Zaman = Convert.ToInt32(ConfigurationManager.AppSettings["Bilgilendirme:Zaman"].ToString());
            }
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();
            IJobDetail job = JobBuilder.Create<GorevBilgilendirme>().UsingJobData("HttpContext", HttpContext.Current.Server.MapPath("")).Build();
            ITrigger trigger = TriggerBuilder.Create()
            .WithDailyTimeIntervalSchedule
              (s =>
                s.WithIntervalInMinutes(Zaman)
              )
            .Build();
            scheduler.ScheduleJob(job, trigger);
        }
    }
}
