﻿namespace TMS.Panel.Web.Code.Cache
{
    public class CacheKey
    {
        private readonly string _format = "{0}-{1}";
        private readonly string _generatedKey;

        public CacheKey(string root, string Prm)
        {
            _generatedKey = string.Format(_format, root, Prm);
        }

        public static CacheKey New(string KeyPrt, string Prm)
        {
            return new CacheKey(KeyPrt, Prm);
        }

        public override string ToString()
        {
            return _generatedKey;
        }
    }
}
