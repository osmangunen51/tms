﻿using System.IO;
using System.Web.Mvc;
namespace TMS.Panel.Web.Code
{
    public class PageGuideAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var file = $"~/Content/PageGuide/{filterContext.HttpContext.Request.RawUrl}.js";
            if (filterContext.ActionDescriptor.ActionName == "Index" && !file.Contains("/Index"))
            {
                file = $"~/Content/PageGuide/{filterContext.HttpContext.Request.RawUrl}{filterContext.ActionDescriptor.ActionName}.js";
            }
            System.IO.FileInfo Dosya = new FileInfo(filterContext.HttpContext.Server.MapPath(file));
            if (Dosya.Exists)
            {
                var controller = filterContext.Controller as Controller;
                controller.ViewBag.PageGuide = file.Replace("~/", "/");
            }
            else
            {

            }
            base.OnActionExecuting(filterContext);
        }
    }
}
