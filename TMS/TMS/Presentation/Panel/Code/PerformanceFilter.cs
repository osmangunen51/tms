﻿using System;
using System.Diagnostics;
using System.Web;
using System.Web.Mvc;
namespace TMS.Panel.Web.Code
{
    public class PerformanceFilter : IActionFilter
    {
        public Core.Service.PerformansService PerformansService = new Core.Service.PerformansService();
        private Stopwatch stopWatch = new Stopwatch();
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            stopWatch.Reset();
            stopWatch.Start();
        }
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            stopWatch.Stop();
            var executionTime = stopWatch.ElapsedMilliseconds;
            int KullaniciNo = -1;
            if (HttpContext.Current.Session["Kullanici"] != null)
            {
                KullaniciNo = Convert.ToInt32(HttpContext.Current.Session["Kullanici"].ToString());
            }
            PerformansService.Ekle(KullaniciNo, 1000, string.Format("{0}", filterContext.RequestContext.HttpContext.Request.Url.ToString()), (float)stopWatch.Elapsed.TotalSeconds);
        }
    }
}
