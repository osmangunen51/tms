﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Principal;
namespace TMS.Panel.Web.Code.Security
{
    public class CustomPrincipal : IPrincipal
    {
        public IIdentity Identity { get; private set; }
        public bool IsInRole(string role)
        {
            if (roles.Any(r => role.Contains(r)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public CustomPrincipal(string Username)
        {
            Identity = new GenericIdentity(Username);
        }
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Image { get; set; }
        public string Telefon { get; set; }
        public string[] roles { get; set; }


        [NotMapped]
        public string Folder { get; set; }
        [NotMapped]
        public string SubFolder { get; set; }

        [NotMapped]
        public List<int> BayiListesi { get; set; } = new List<int>();

    }

    public class CustomPrincipalSerializeModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Image { get; set; }
        public string Telefon { get; set; }
        public string Folder { get; set; }
        public string SubFolder { get; set; }
        public string UserName { get; set; }
        public string[] roles { get; set; }
        public List<int> BayiListesi { get; set; } = new List<int>();
    }
}
