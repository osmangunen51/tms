﻿using System.Web.Optimization;

namespace TMS.Panel.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Css Bundle

            bundles.Add(new StyleBundle("~/Anasayfa.Site/css").
                                    Include
                                    (
                                        "~/Theme/Default/css/site.css"
                                    )
                            );

            #endregion Css Bundle

            #region Javascript Bundle

            bundles.Add(
                new ScriptBundle("~/Anasayfa.Site/js").
                    Include(
                                "~/Theme/Default/js/jquery.js",
                                "~/Theme/Default/js/jRespond.min.js",
                                "~/Theme/Default/js/jquery.easing.min.js",
                                "~/Theme/Default/js/jquery.waitforimages.min.js",
                                "~/Theme/Default/js/jquery.transit.min.js",
                                "~/Theme/Default/js/jquery.fitvids.js",
                                "~/Theme/Default/js/jquery.waypoints.min.js",
                                "~/Theme/Default/js/jquery.stellar.js",
                                "~/Theme/Default/js/owl.carousel.min.js",
                                "~/Theme/Default/js/jquery.mb.YTPlayer.min.js",
                                "~/Theme/Default/js/hoverIntent.js",
                                "~/Theme/Default/js/simple-scrollbar.min.js",
                                "~/Theme/Default/js/superfish.js",
                                "~/Theme/Default/js/scrollIt.min.js",
                                "~/Theme/Default/js/jquery.magnific-popup.min.js",
                                "~/Theme/Default/js/jquery.validate.min.js",
                                "~/Theme/Default/js/jquery.ajaxchimp.min.js",
                                "~/Theme/Default/js/functions.js"
                          )
                    );

            #endregion Javascript Bundle

#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
                    BundleTable.EnableOptimizations = false;
#endif
        }
    }
}