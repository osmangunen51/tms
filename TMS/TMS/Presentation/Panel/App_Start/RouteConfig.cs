﻿using System.Web.Mvc;
using System.Web.Routing;

namespace TMS.Panel.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(null, "connector",
             defaults: new { controller = "File", action = "Index" },
             namespaces: new[] { "TMS.Panel.Web.Areas.Administrator.Controllers" }
              );
            routes.MapRoute(null,
                "Kisitlama",
                defaults: new { controller = "Yetki", action = "Kisitlama", Url = UrlParameter.Optional }
            );

            routes.MapRoute(
                null, "GenelHata",
                defaults: new { controller = "Error", action = "Index" }
              );

            routes.MapRoute
                (
                    name: "Default",
                    url: "{controller}/{action}/{id}",
                    defaults: new { controller = "Anasayfa", action = "Index", id = UrlParameter.Optional },
                    namespaces: new[] { "TMS.Panel.Web.Controllers" }
                );
        }
    }
}